﻿using System.Collections.Generic;

namespace BioLab.GCiatto.Utils
{
    public interface ICacheGroup
    {
        void Add(ICache<dynamic> cache);

        IEnumerable<ICache<dynamic>> Caches { get; }

        void ValidateAll();

        void InvalidateAll();
    }
}
