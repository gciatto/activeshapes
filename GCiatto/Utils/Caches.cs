﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Utils
{
    public static class Caches
    {
        public static ICache<U> Of<U>(this Func<U> f) where U : class
        {
            return new Cache<U>(f, false);
        }

        public static ICache<U> Lazy<U>(this Func<U> f) where U : class
        {
            return new Cache<U>(f, true); 
        }

        public static ICacheGroup Group(params ICache<dynamic>[] caches)
        {
            return new CacheGroup(caches);
        }
    }
}
