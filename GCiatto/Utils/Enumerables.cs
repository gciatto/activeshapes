﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Utils
{
    public static class Enumerables
    {
        public static IEnumerable<T> Peek<T>(this IEnumerable<T> e, Action<T> f)
        {
            return e.Select(it =>
            {
                f(it);
                return it;
            });
        }

        public static void ForEach<T>(this IEnumerable<T> e, Action<T> f)
        {
            e.Peek(f).Now();
        }

        public static void Now<T>(this IEnumerable<T> e)
        {
            var i = e.GetEnumerator();
            while (i.MoveNext()) ;
        }
    }
}
