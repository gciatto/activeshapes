﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Utils
{
    public static class Randoms
    {
        private static readonly Random R = new Random();

        public static int RandomInt()
        {
            return R.Next();
        }

        public static int RandomInt(int max)
        {
            return R.Next(max);
        }

        public static bool RandomBool
        {
            get
            {
                return R.Next(10) % 2 == 0;
            }
        }

        public static bool Probability(double p)
        {
            return R.NextDouble() < p;
        }

        public static int RandomSign
        {
            get
            {
                return RandomBool ? 1 : -1;
            }
        }

        public static double RandomDoubleSign
        {
            get
            {
                return RandomBool ? 1d : -1d;
            }
        }

        public static double RandomDouble()
        {
            return R.NextDouble();
        }

        public static double RandomDouble(double max)
        {
            return RandomDouble() * max;
        }

        public static double RandomDouble(double min, double max)
        {
            return RandomDouble(max - min) + min;
        }
    }
}
