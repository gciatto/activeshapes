﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Utils
{
    public interface IFuzzyMatchable<in T>
    {
        double FuzzyMatches(T other);
    }
}
