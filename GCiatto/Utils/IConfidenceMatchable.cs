﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Utils
{
    public interface IConfidenceMatchable<in T>
    {
        double ConfidenceMatches(T other);
    }
}
