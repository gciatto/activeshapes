﻿using System;

namespace BioLab.GCiatto.Utils
{
    sealed class Cache<T> : ICache<T> where T : class
    {
        private Func<T> generator;
        private bool invalid = true;
        private T cache;

        public Cache(Func<T> generator, bool lazy)
        {
            if (generator == null)
                throw new ArgumentNullException();
            this.generator = generator;
            if (lazy) { }
            else Validate();
        } 

        public void Validate()
        {
            if (invalid)
            {
                cache = generator();
                invalid = false;
            }
        }

        public void Invalidate()
        {
            invalid = true;
            cache = null;
        }

        public T Value
        {
            get
            {
                Validate();
                return cache;
            }
        }

        public override string ToString()
        {
            return String.Format("Cache[value={0}]", Value);
        }
        
    }
}
