﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Utils
{
    sealed class CacheGroup : ICacheGroup
    {
        private LinkedList<WeakReference<ICache<object>>> caches = new LinkedList<WeakReference<ICache<object>>>();

        public CacheGroup()
        {

        }
        public CacheGroup (IEnumerable<ICache<dynamic>> caches)
        {
            foreach (var c in caches)
            {
                Add(c);
            }
        }
        public void Add(ICache<dynamic> cache)
        {
            caches.AddLast(new WeakReference<ICache<object>>(cache));
        }

        public IEnumerable<ICache<dynamic>> Caches
        {
            get
            {
                return caches.Select(it =>
                {
                    ICache<dynamic> e;
                    if (it.TryGetTarget(out e))
                        return e;
                    else
                        return null;
                }).Where(it => it != null);
            }
        }

        public void ValidateAll()
        {
            foreach (var c in Caches)
            {
                c.Validate();
            }
        }

        public void InvalidateAll()
        {
            foreach (var c in Caches)
            {
                c.Invalidate();
            }
        }
    }
}
