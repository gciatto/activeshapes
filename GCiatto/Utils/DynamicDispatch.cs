﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Utils
{
    public class DynamicDispatch<T>
    {
        private IDictionary<Type, Action<T>> actions = new Dictionary<Type, Action<T>>();
        private Action<T> defaultAction;

        public DynamicDispatch() : this(null)
        {

        }
        public DynamicDispatch(Action<T> defaultAction)
        {
            Default = defaultAction;
        }
        public void Register<U>(Action<U> f) where U : T
        {
            actions.Add(typeof(U), it => f((U)it));
        }

        public Action<T> Default
        {
            get
            {
                return defaultAction;
            }
            set
            {
                defaultAction = value;
            }
        }

        public void Invoke<U>(U arg) where U : T
        {
            if (arg != null && actions.ContainsKey(arg.GetType()))
            {
                actions[arg.GetType()].Invoke(arg);
            }
            else
            {
                if (defaultAction != null)
                    defaultAction(arg);
                else
                    throw new ArgumentException();
            }
        }
    }
}
