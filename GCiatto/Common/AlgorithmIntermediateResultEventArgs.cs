﻿using System;

namespace BioLab.GCiatto.Common
{
    public class AlgorithmIntermediateResultEventArgs<T> : BioLab.Common.AlgorithmIntermediateResultEventArgs where T : class
    {
        public AlgorithmIntermediateResultEventArgs(T intRes)
            : this(intRes, "")
        {

        }

        public AlgorithmIntermediateResultEventArgs(T intRes, String desc)
            : base(intRes, desc)
        {

        }

        public AlgorithmIntermediateResultEventArgs(T intRes, int step)
            : base(intRes, "", step)
        {

        }

        public AlgorithmIntermediateResultEventArgs(T intRes, String desc, int step)
            : base(intRes, desc, step)
        {

        }

        public virtual T TypedIntermediateResult
        {
            get
            {
                return IntermediateResult as T;
            }
        }
    }
}
