﻿using System;
using BioLab.Common;
using BioLab.GCiatto.Utils;

namespace BioLab.GCiatto.Common
{
    public abstract class AbstractAlgorithm<Tin, Tint, Tout> : IAlgorithm<Tin, Tint, Tout> where Tin : class where Tint : class where Tout : class
    {
        private Tin input;
        private ICache<Tout> output;

        public AbstractAlgorithm()
        {
            output = Caches.Lazy(() => AlgorithmImplementation(input));
        }

        public virtual Tin Input
        {
            get
            {
                return input;
            }
            set
            {
                if (input != null && input.Equals(value))
                    output.Invalidate();
                input = value;
            }
        }

        public virtual Tout Output
        {
            get
            {
                return output.Value;
            }
        }

        public event EventHandler<AlgorithmIntermediateResultEventArgs> IntermediateResult;
        public event EventHandler<AlgorithmProgressChangedEventArgs> ProgressChanged;

        public Tout Exectute()
        {
            return Output;
        }

        public Tout Exectute(Tin input)
        {
            Input = input;
            return Output;
        }

        public void RaiseIntermediateResultEvent(Tint result)
        {
            if (IntermediateResult != null)
                IntermediateResult(this, new AlgorithmIntermediateResultEventArgs<Tint>(result));
        }

        public void RaiseIntermediateResultEvent(Tint result, int step)
        {
            if (IntermediateResult != null)
                IntermediateResult(this, new AlgorithmIntermediateResultEventArgs<Tint>(result, step));
        }

        public void RaiseIntermediateResultEvent(Tint result, string description)
        {
            if (IntermediateResult != null)
                IntermediateResult(this, new AlgorithmIntermediateResultEventArgs<Tint>(result, description));
        }

        public void RaiseIntermediateResultEvent(Tint result, string description, int step)
        {
            if (IntermediateResult != null)
                IntermediateResult(this, new AlgorithmIntermediateResultEventArgs<Tint>(result, description, step));
        }

        public void RaiseProgressChangedEvent(double percentage)
        {
            RaiseProgressChangedEvent(percentage, "");
        }

        public void RaiseProgressChangedEvent(double percentage, string description)
        {
            if (ProgressChanged != null)
            {
                ProgressChanged(this, new AlgorithmProgressChangedEventArgs(percentage, description));
            }
        }

        public void Run()
        {
            output.Validate();
        }

        public void Run(Tin input)
        {
            Input = input;
            output.Validate();
        }

        protected abstract Tout AlgorithmImplementation(Tin input);
    }
}
