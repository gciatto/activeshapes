﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Common
{
    public interface IAlgorithm<in Tin, in Tint, out Tout> : BioLab.Common.IAlgorithm where Tin : class where Tint : class where Tout : class
    {
        Tin Input { set; }
        Tout Output { get; }
        void Run(Tin input);
        Tout Exectute();
        Tout Exectute(Tin input);
        void RaiseIntermediateResultEvent(Tint result);
        void RaiseIntermediateResultEvent(Tint result, String description);
        void RaiseIntermediateResultEvent(Tint result, String description, int step);
        void RaiseIntermediateResultEvent(Tint result, int step);
        void RaiseProgressChangedEvent(double percentage);
        void RaiseProgressChangedEvent(double percentage, String description);
    }
}
