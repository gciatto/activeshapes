﻿using BioLab.Math.Geometry;
using BioLab.Math.LinearAlgebra;

namespace BioLab.GCiatto.Math.Geometry
{
    public static class Geometry
    {
        public static Vector ToVector(this IntPoint2D it)
        {
            return new Vector(new double[] { it.X, it.Y });
        }

        public static Vector ToVector(this Point2D it)
        {
            return new Vector(new double[] { it.X, it.Y });
        }

        public static IntPoint2D ToIntPoint2D(this Vector it)
        {
            return new IntPoint2D((int)System.Math.Round(it[0]), (int)System.Math.Round(it[1]));
        }

        public static IntPoint2D ToIntPoint2D(this Point2D it)
        {
            return (IntPoint2D)it;
        }

        public static Point2D ToPoint2D(this Vector it)
        {
            return new Point2D(it[0], it[1]);
        }
    }
}
