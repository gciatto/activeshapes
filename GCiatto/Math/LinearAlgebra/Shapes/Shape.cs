﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BioLab.GCiatto.ImageProcessing;
using BioLab.ImageProcessing;
using BioLab.Math.LinearAlgebra;

namespace BioLab.GCiatto.Math.LinearAlgebra.Shapes
{
    public class Shape
    {

        public Shape(IEnumerable<Vector> vectors)
        {
            this.Points = vectors;
        }

        public static Shape Of(params Vector[] vs)
        {
            return new Shape(vs);
        }

        public IEnumerable<Vector> Points
        {
            get;
        }

        public Vector HighDimensionalVector
        {
            get
            {
                return Points.ToHighDimensionalVector();
            }
        }

        public override string ToString()
        {
            return String.Format(
                    "Shape[Vectors={0}]",
                    LinearAlgebra.ToString(Points)
                );
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;
            else if (obj == null)
                return false;
            else if (obj.GetType().IsAssignableFrom(typeof(Shape)))
            {
                var other = obj as Shape;
                return Points.Equals(other.Points);
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            var result = 17;
            result += 31 * Points.GetHashCode();
            return result;
        }

        public MarkedRegionList ToMarkedRegionList(MarkedRegionListPattern regionsPattern)
        {
            return Points.ToMarkedRegionList(regionsPattern);
        }
    }
}
