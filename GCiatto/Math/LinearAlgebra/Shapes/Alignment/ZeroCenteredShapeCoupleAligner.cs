﻿using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Math.LinearAlgebra.Shapes.Alignment
{
    class ZeroCenteredShapeCoupleAligner : IShapeCoupleAligner
    {
        public ZeroCenteredShapeCoupleAligner()
        {

        }
        public ZeroCenteredShapeCoupleAligner(Shape reference)
        {
            Reference = reference;
        }

        public virtual Shape Reference
        {
            get;
            set;
        }

        public virtual Affinity2D Affinity(Shape shape)
        {
            return Affinity(Reference, shape);
        }

        protected static Affinity2D Affinity(Shape reference, Shape shape)
        {
            double a = 0.0, b = 0.0, n = 0.0;

            for (IEnumerator<Vector> e1 = shape.Points.GetEnumerator(), e2 = reference.Points.GetEnumerator(); e1.MoveNext() | e2.MoveNext();)
            {
                var c = new { X1 = e1.Current[0], Y1 = e1.Current[1], X2 = e2.Current[0], Y2 = e2.Current[1] };
                a += c.X1 * c.X2 + c.Y1 * c.Y2;
                b += c.X1 * c.Y2 - c.Y1 * c.X2;
                n += c.X1 * c.X1 + c.Y1 * c.Y1;
            }

            a /= n;
            b /= n;

            return Affinity2D.NewSimilarity(a, b);
        }

        public virtual Shape AlignToReference(Shape shape)
        {
            var at = Affinity(shape);
            return shape.Points.Select(p => at.Apply(p)).ToShape();
        }
    }
}
