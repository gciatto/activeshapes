﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Math.LinearAlgebra.Shapes.Alignment
{
    public delegate IEnumerable<Shape> ShapesAligner(IEnumerable<Shape> shapes);

    public static class Aligners
    {

        public static IEnumerable<Shape> DummyAlign(this IEnumerable<Shape> shapes)
        {
            var sca = CoupleAligners.ZeroCentered(shapes.First());
            return shapes.Select((it, i) => i > 0 ? sca.AlignToReference(it) : it);
        }

        public static IEnumerable<Shape> ZeroCenteredAlign(this IEnumerable<Shape> shapes)
        {
            return new ZeroCenteredShapesAligner().AlignAll(shapes);
        }
    }
}
