﻿using BioLab.GCiatto.Math.Statistic;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Math.LinearAlgebra.Shapes.Alignment
{
    class ZeroCenteredShapesAligner
    {
        private ZeroCenteredShapeCoupleAligner coupleAligner = new ZeroCenteredShapeCoupleAligner();
        private int limit = 10;
        public IEnumerable<Shape> AlignAll(IEnumerable<Shape> shapes)
        {
            var continueCond = true;
            var meanShapeHD = Normalize(shapes.First().HighDimensionalVector);
            var meanShapeLD = meanShapeHD.ToLowDimensionalVectors(2);

            for (int i = 0; i < limit && continueCond; i++)
            {
                coupleAligner.Reference = meanShapeLD.ToShape();
                shapes = shapes
                    .Select(s => coupleAligner.AlignToReference(s))
                    .Select(s => ProjectToTangentSpace(s, meanShapeHD));

                var temp = shapes
                    .Select(s => s.HighDimensionalVector)
                    .Mean()
                    .ToLowDimensionalVectors(2)
                    .ToShape();
                temp = coupleAligner.AlignToReference(temp);

                meanShapeHD = Normalize(temp.HighDimensionalVector);
                meanShapeLD = meanShapeHD.ToLowDimensionalVectors(2);
            }

            MeanShape = meanShapeLD.ToShape();
            return shapes;
        }

        public Shape MeanShape
        {
            get;
            protected set;
        }

        protected Vector Normalize(Vector x)
        {
            var norm = x.ComputeL2Norm();
            x.ApplyToEachComponent(x_i => x_i / norm);
            return x;
        }

        protected Shape ProjectToTangentSpace(Shape shape, Vector meanShapeHD)
        {
            var shapeHD = shape.HighDimensionalVector;
            var factor = shapeHD.ComputeDotProduct(meanShapeHD);
            shapeHD.ApplyToEachComponent(x_i => x_i / factor);
            return shapeHD.ToLowDimensionalVectors(2).ToShape();
        }
    }
}
