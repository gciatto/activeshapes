﻿using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Math.LinearAlgebra.Shapes.Alignment
{
    public static class CoupleAligners
    {
        public static IShapeCoupleAligner ZeroCentered(this Shape reference)
        {
            return new ZeroCenteredShapeCoupleAligner(reference);
        }

        public static IShapeCoupleAligner Aligner(this Shape reference)
        {
            return Aligner(reference, null);
        }

        public static IShapeCoupleAligner Aligner(this Shape reference, Vector refMean)
        {
            return new ShapeCoupleAligner(reference, refMean);
        }

        public static IShapeCoupleAligner Aligner()
        {
            return Aligner(null, null);
        }
    }
}
