﻿using BioLab.GCiatto.Math.Statistic;
using BioLab.GCiatto.Utils;
using BioLab.Math.LinearAlgebra;
using System;
using System.Linq;

namespace BioLab.GCiatto.Math.LinearAlgebra.Shapes.Alignment
{
    class ShapeCoupleAligner : ZeroCenteredShapeCoupleAligner, IShapeCoupleAligner
    {
        private ICache<Vector> refMean;
        
        public ShapeCoupleAligner(Shape reference, Vector refMean) : base(reference)
        {
            if (refMean != null)
                this.refMean = Caches.Of(() => refMean);
            else
                this.refMean = Caches.Lazy(GetRefMean);
        }

        public override Shape Reference
        {
            get
            {
                return base.Reference;
            }

            set
            {
                base.Reference = value;
                this.refMean = Caches.Lazy(GetRefMean);
            }
        }

        protected Vector GetRefMean()
        {
            return Reference.Points.Mean();
        }

        public Vector ReferenceMean
        {
            get { return refMean.Value; }
        }

        public Shape ZeroCenteredReference
        {
            get
            {
                return ZeroCenterd(Reference, ReferenceMean);
            }
        }

        protected Shape ZeroCenterd(Shape s, Vector m)
        {
            return s.Points.Select(p => p - m).ToShape();
        }

        public override Affinity2D Affinity(Shape input)
        {
            var inputMean = input.Points.Mean();
            var zAffinity = Affinity(ZeroCenteredReference, ZeroCenterd(input, inputMean));
            var ds = ReferenceMean - inputMean;
            return zAffinity.Translate(ds);
        }

    }
}
