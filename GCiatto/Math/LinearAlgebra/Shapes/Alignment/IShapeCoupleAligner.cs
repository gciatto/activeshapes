﻿using BioLab.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Math.LinearAlgebra.Shapes.Alignment
{
    public interface IShapeCoupleAligner
    {
        Shape Reference
        {
            get;
        }

        Shape AlignToReference(Shape shape);

        Affinity2D Affinity(Shape shape);
    }
}
