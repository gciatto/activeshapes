﻿using BioLab.GCiatto.Math.Statistic;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Math.LinearAlgebra.Shapes.Normalization
{

    public delegate Shape ShapeNormalizer(Shape shape);

    public static class ShapeNormalizers
    {
        public static Shape MeanNormalize(this Shape shape)
        {
            var mean = shape.Points.Mean();
            return shape.Points.Select(p => p - mean).ToShape();
        }

        public static Shape Identity(this Shape shape)
        {
            return shape;
        }
    }
}
