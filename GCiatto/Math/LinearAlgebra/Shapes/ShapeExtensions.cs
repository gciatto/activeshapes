﻿using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Math.LinearAlgebra.Shapes
{
    public static class ShapeExtensions
    {
        public static Shape ToShape(this IEnumerable<Vector> vs)
        {
            return new Shape(vs);
        }

    }
}
