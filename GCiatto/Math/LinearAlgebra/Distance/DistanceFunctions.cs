﻿using BioLab.Math.LinearAlgebra;

namespace BioLab.GCiatto.Math.LinearAlgebra.Distance
{
    public delegate double DistanceFunction(Vector x, Vector y);
    public delegate double DistanceFunctionWithContext<in T>(Vector x, Vector y, T ctx);

    public static class DistanceFunctions
    {
        public static double MahalanobisDistanceCovariance(Vector x, Vector y, Matrix covariance)
        {
            return MahalanobisDistanceInverseCovariance(x, y, covariance.ComputeInverse());
        }

        public static double MahalanobisDistanceInverseCovariance(Vector x, Vector y, Matrix inverse)
        {
            var p = (x - y);
            var res = (inverse * p).ComputeDotProduct(p);
            if (res < 0)
            {
                //throw new InvalidOperationException();
            }
            return System.Math.Sqrt(res);
        }

        public static double EuclideanDistance(Vector x, Vector y)
        {
            return (x - y).ComputeL2Norm();
        }
    }
}
