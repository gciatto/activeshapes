﻿using BioLab.GCiatto.Math.Geometry;
using BioLab.GCiatto.Utils;
using BioLab.Math.Geometry;
using BioLab.Math.LinearAlgebra;
using System;

namespace BioLab.GCiatto.Math.LinearAlgebra
{
    public abstract class Affinity2D
    {
        private Lazy<Affinity2D> inverse;
        private Lazy<Matrix> transformation;
        private Lazy<Vector> translation;

        public Affinity2D()
        {
            translation = new Lazy<Vector>(ComputeTranslation);
            transformation = new Lazy<Matrix>(ComputeTransformation);
            inverse = new Lazy<Affinity2D>(ComputeInverse);
        }

        public virtual Matrix Transformation
        {
            get
            {
                return transformation.Value;
            }
        }
        public virtual Vector Translation
        {
            get
            {
                return translation.Value;
            }
        }

        public virtual Affinity2D Inverse
        {
            get
            {
                return inverse.Value;
            }
        }

        protected abstract Affinity2D ComputeInverse();

        protected virtual Matrix ComputeTransformation()
        {
            return LinearAlgebra.ByDiagonal(1, 1);
        }

        protected virtual Vector ComputeTranslation()
        {
            return LinearAlgebra.Vector(0, 0);
        }

        public virtual Vector Apply(Vector x)
        {
            return Transformation * x + Translation;
        }

        public virtual Vector Apply(double x, double y)
        {
            return Apply(LinearAlgebra.Vector(x, y));
        }

        public virtual Point2D Apply(Point2D x)
        {
            return Apply(x.ToVector()).ToPoint2D();
        }

        public abstract Affinity2D Combine(Affinity2D other);

        public static Affinity2D Identity = new IdentityTransformation();
        public static Affinity2D Nullity = new NullityTransformation();

        public virtual Affinity2D Rotate(double angle)
        {
            return Combine(new RotationTransformation(angle));
        }

        public virtual Affinity2D Scale(double factor)
        {
            return Combine(new SymmetricScaleTransformation(factor));
        }

        public virtual Affinity2D Translate(Vector t)
        {
            return Combine(new TranslationTransformation(t));
        }

        public virtual Affinity2D Translate(double x, double y)
        {
            return Translate(LinearAlgebra.Vector(x, y));
        }
        public virtual Affinity2D OnlyTranslation()
        {
            return Identity;
        }

        public virtual Affinity2D OnlyTransformation()
        {
            return this;
        }

        public static Affinity2D NewRotation(double angle)
        {
            return new RotationTransformation(angle);
        }

        public static Affinity2D NewScale(double factor)
        {
            return new SymmetricScaleTransformation(factor);
        }

        public static Affinity2D NewTranslation(Vector t)
        {
            return new TranslationTransformation(t);
        }

        public static Affinity2D NewTranslation(double x, double y)
        {
            return new TranslationTransformation(LinearAlgebra.Vector(x, y));
        }

        public static Affinity2D NewSimilarity(double a, double b)
        {
            return new SimilarityTransformation(a, b);
        }
    }

    class GeneralTransformation : Affinity2D
    {
        private Matrix transformation;
        private Vector translation;
        //private DynamicDispatch<Affinity2D> dd;

        public GeneralTransformation(Matrix transformation)
        : this(transformation, LinearAlgebra.Zero(2))
        {
        }

        public GeneralTransformation(Matrix transformation, Vector translation) 
            : base()
        {
            this.transformation = transformation;
            this.translation = translation;

            //dd = new DynamicDispatch<Affinity2D>();
        }

        public GeneralTransformation(double t00, double t01, double t10, double t11, double tx, double ty)
            : this(LinearAlgebra.ByRows(2, t00, t01, t10, t11), LinearAlgebra.Vector(tx, ty))
        {

        }

        protected override Matrix ComputeTransformation()
        {
            return transformation;
        }

        protected override Vector ComputeTranslation()
        {
            return translation;
        }

        protected override Affinity2D ComputeInverse()
        {
            return new GeneralTransformation(transformation.ComputeInverse(), -translation);
        }

        public override Affinity2D Combine(Affinity2D other)
        {
            if (typeof(IdentityTransformation).IsAssignableFrom(other.GetType()))
            {
                return this;
            }
            else if (typeof(NullityTransformation).IsAssignableFrom(other.GetType()))
            {
                return other;
            }
            else if (typeof(TranslationTransformation).IsAssignableFrom(other.GetType()))
            {
                return new GeneralTransformation(transformation, translation + other.Translation);
            }
            else
            {
                return new GeneralTransformation(transformation * other.Transformation, translation + other.Translation);
            }
        }

        public override Affinity2D OnlyTranslation()
        {
            return new TranslationTransformation(translation);
        }

        public override Affinity2D OnlyTransformation()
        {
            return new GeneralTransformation(transformation);
        }
    }

    class IdentityTransformation : Affinity2D
    {
        public IdentityTransformation() : base() { }

        public override Affinity2D Inverse
        {
            get
            {
                return this;
            }
        }

        public override Affinity2D Combine(Affinity2D other)
        {
            return other;
        }

        public override Affinity2D Rotate(double angle)
        {
            return new RotationTransformation(angle);
        }

        public override Affinity2D Scale(double factor)
        {
            return new SymmetricScaleTransformation(factor);
        }

        public override Affinity2D Translate(Vector t)
        {
            return new TranslationTransformation(t);
        }

        protected override Affinity2D ComputeInverse()
        {
            return this;
        }
    }

    class NullityTransformation : Affinity2D
    {
        public NullityTransformation() : base() { }
        public override Affinity2D Inverse
        {
            get
            {
                throw new InvalidOperationException("not invertible");
            }
        }

        protected override Matrix ComputeTransformation()
        {
            return LinearAlgebra.ByDiagonal(0, 0);
        }

        public override Affinity2D Combine(Affinity2D other)
        {
            return this;
        }

        protected override Affinity2D ComputeInverse()
        {
            throw new InvalidOperationException("not invertible");
        }
    }

    class SimilarityTransformation : Affinity2D
    {
        private double a, b;
        public SimilarityTransformation(double a, double b) : base()
        {
            this.a = a;
            this.b = b;
        }

        protected override Matrix ComputeTransformation()
        {
            return LinearAlgebra.ByRows(2, a, -b, b, a);
        }

        public override Affinity2D Combine(Affinity2D other)
        {
            if (typeof(IdentityTransformation).IsAssignableFrom(other.GetType()))
            {
                return this;
            }
            else if (typeof(NullityTransformation).IsAssignableFrom(other.GetType()))
            {
                return other;
            }
            else if (typeof(SimilarityTransformation).IsAssignableFrom(other.GetType()))
            {
                var otherSimilarity = other as SimilarityTransformation;
                var newA = a * otherSimilarity.a - b * otherSimilarity.b;
                var newB = otherSimilarity.a * b + a * otherSimilarity.b;
                return new SimilarityTransformation(newA, newB);
            }
            else if (typeof(TranslationTransformation).IsAssignableFrom(other.GetType()))
            {
                return new GeneralTransformation(Transformation, other.Translation);
            }
            else
            {
                return new GeneralTransformation(Transformation * other.Transformation, Translation + other.Translation);
            }
        }

        protected override Affinity2D ComputeInverse()
        {
            var det = a * a + b * b;
            if (det == 0)
            {
                throw new InvalidOperationException("not invertible");
            }
            else
            {
                var newA = a / det;
                var newB = -b / det;
                return new SimilarityTransformation(newA, newB);
            }
        }
    }

    class RotationTransformation : SimilarityTransformation
    {
        private double angle;
        public RotationTransformation(double angle)
            : base(System.Math.Cos(angle), System.Math.Sin(angle))
        {
            this.angle = angle;
        }

        protected override Affinity2D ComputeInverse()
        {
            return new RotationTransformation(-angle);
        }
    }

    class SymmetricScaleTransformation : SimilarityTransformation
    {
        private double factor;
        public SymmetricScaleTransformation(double factor)
            : base(factor, 0)
        {
            this.factor = factor;
        }

        protected override Affinity2D ComputeInverse()
        {
            return new SymmetricScaleTransformation(1 / factor);
        }
    }

    class TranslationTransformation : Affinity2D
    {
        private Vector translation;

        public TranslationTransformation(Vector translation) 
            : base()
        {
            this.translation = translation;
        }
        public override Affinity2D Combine(Affinity2D other)
        {
            if (typeof(IdentityTransformation).IsAssignableFrom(other.GetType()))
            {
                return this;
            }
            else if (typeof(NullityTransformation).IsAssignableFrom(other.GetType()))
            {
                return other;
            }
            else if (typeof(TranslationTransformation).IsAssignableFrom(other.GetType()))
            {
                return new TranslationTransformation(translation + other.Translation);
            }
            else
            {
                return new GeneralTransformation(other.Transformation, Translation + other.Translation);
            }
        }

        protected override Vector ComputeTranslation()
        {
            return translation;
        }

        protected override Affinity2D ComputeInverse()
        {
            return new TranslationTransformation(-translation);
        }

        public override Affinity2D OnlyTranslation()
        {
            return this;
        }

        public override Affinity2D OnlyTransformation()
        {
            return Identity;
        }
    }
}
