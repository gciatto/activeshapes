﻿using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Math.LinearAlgebra.Similarity
{

    public delegate double SimilarityFunction(Vector x, Vector y);

    public static class SimilarityFunctions
    {
        public static double CosineSimilarity(this Vector x, Vector y)
        {
            return x.ComputeDotProduct(y) / (x.ComputeL2Norm() * y.ComputeL2Norm());
        }

        public static double SimilarityFromNonNegativeDistance(this double distance)
        {
            if (distance < 0 )
                throw new ArgumentException("negative distance");
            return 1d / (1d + distance);
        }

        public static double SimilarityFromLimitedDistanceLBUB(this double distance, double lb, double ub)
        {
            if (distance < lb || distance > ub)
                throw new ArgumentException("distance out of range");
            return (distance - lb) / (ub - lb);
        }
    }
}
