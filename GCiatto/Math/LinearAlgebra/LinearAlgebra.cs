﻿using BioLab.GCiatto.Utils;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BioLab.GCiatto.Math.LinearAlgebra
{
    public static class LinearAlgebra
    {
        public static Vector AppliedVector(this Vector from, Vector to)
        {
            return to - from;
        }
        public static Vector Versor(this Vector from, Vector to)
        {
            var v = to - from;
            var magnitude = v.ComputeL2Norm();
            v.ApplyToEachComponent(it => it / magnitude);
            return v;
        }

        public static Vector MapElements(this Vector v, Func<double, double> f)
        {
            var clone = v.Clone();
            v.ApplyToEachComponent(f);
            return clone;
        }

        public static void ApplyToEachComponent(this Vector v, Func<double, double> f)
        {
            for (int i = 0; i < v.Count; i++)
            {
                v[i] = f(v[i]);
            }
        }

        public static Vector Vector(params double[] components)
        {
            return new Vector(components);
        }

        public static Vector Zero(int dim)
        {
            return new Vector(dim);
        }

        public static Matrix ByRows(int cols, params double[] components)
        {
            if (components.Length % cols != 0)
                throw new ArgumentException();
            return new Matrix(components.Length / cols, cols, components);
        }

        public static Matrix ByDiagonal(params double[] components)
        {
            if (components.Length == 0)
                throw new ArgumentException();
            return new Matrix(components);
        }

        public static Vector ToHighDimensionalVector(this IEnumerable<Vector> points)
        {
            var lowDim = points.First().Count;
            var v = new Vector(points.Count() * lowDim);

            var i = 0;
            foreach (var p in points)
            { 
                if (p.Count != lowDim) throw new ArgumentException("unextected vector size");
                for (int j = 0; j < lowDim; j++)
                {
                    v[i++] = p[j];
                }
            }

            return v;
        }

        public static IList<Vector> ToLowDimensionalVectors(this Vector hdVector, int lowDim)
        {
            var highDim = hdVector.Count;
            if (highDim % lowDim != 0) throw new ArgumentException("unextected vector size or lowdim");
            var count = highDim / lowDim;
            var l = new List<Vector>(count);

            for (var i = 0; i < count; i++)
            {
                var lowV = new double[lowDim];
                for (var j = 0; j < lowDim; j++)
                {
                    lowV[j] = hdVector[i * lowDim + j];
                }
                l.Add(Vector(lowV));
            }

            return l;
        }

        public static String ToString(this Vector v)
        {
            if (v.Count == 0) return "()";
            else return String.Format("({0})",
                v.ToArray()
                    .Select(it => String.Format("{0:0.##}", it))
                    .Aggregate((a, b) => String.Format("{0}, {1}", a, b))
                  );
        }

        public static String ToString(this IEnumerable<Vector> v)
        {
            if (v.Count() == 0) return "()";
            else return String.Format("({0})",
                v.ToArray()
                    .Select(it => ToString(it))
                    .Aggregate((a, b) => String.Format("{0}, {1}", a, b))
                  );
        }
    }
}
