﻿using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Math.Statistic
{
    public static class Kernels
    {
        public static double StandardNormalFunction(Vector x)
        {
            return System.Math.Exp(-0.5 * (x.ComputeDotProduct(x))) / System.Math.Sqrt(2 * System.Math.PI);
        }

        public static double NormalFunction(Vector x, Vector mu)
        {
            var p = x - mu;
            return System.Math.Exp(-0.5 * (p.ComputeDotProduct(p))) / System.Math.Sqrt(2 * System.Math.PI);
        }

        public static double NormalFunction(Vector x, Vector mu, double sigma)
        {
            var p = x - mu;
            return System.Math.Exp(-0.5 * (p.ComputeDotProduct(p)) / sigma) / (sigma * System.Math.Sqrt(2 * System.Math.PI));
        }
    }
}
