﻿using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Math.Statistic
{
    public static class ParzenWindow
    {
        public static double EstimateProbability(this Vector x, IEnumerable<Vector> vectors, double h, Func<Vector, double> kernel)
        {
            int d = x.Count;
            int count = 0;
            double accumulator = 0;
            foreach (var x_i in vectors)
            {
                accumulator += kernel((x - x_i) / h);
                count++;
            }
            return accumulator / (count * System.Math.Pow(h, d));
        }

        public static double EstimateProbability(this Vector x, IEnumerable<Vector> vectors, double h)
        {
            return x.EstimateProbability(vectors, h, Kernels.StandardNormalFunction);
        }
    }
}
