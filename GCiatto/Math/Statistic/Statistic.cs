﻿using BioLab.GCiatto.Math.LinearAlgebra;
using BioLab.Math.Geometry;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BioLab.GCiatto.Math.Statistic
{
    static class Statistic
    {
        public static Vector Mean(this IEnumerable<Vector> vectors)
        {
            return vectors.Aggregate((a, b) => a + b) / vectors.Count();
        }

        public static double Variance(this IEnumerable<Vector> vectors, Vector mean)
        {
            return vectors.Select(it => it - mean)
                .Select(it => it.ComputeDotProduct())
                .Aggregate((a, b) => a + b) / vectors.Count();
        }

        public static Vector Variances(this IEnumerable<Vector> vectors, Vector mean)
        {
            return vectors.Select(it => it - mean)
                .Select(it => it.MapElements(x => x * x))
                .Aggregate((it, other) => it + other) / vectors.Count();
        }

        public static double Variance(this IEnumerable<Vector> vectors)
        {
            return vectors.Variance(vectors.Mean());
        }

        public static Vector Variances(this IEnumerable<Vector> vectors)
        {
            return vectors.Variances(vectors.Mean());
        }

        public static Affinity2D Normalization(this IEnumerable<Vector> vectors, out double variance)
        {
            var mean = vectors.Mean();
            variance = vectors.Variance(mean);
            //variance = -1;
            var scale = 1 / System.Math.Sqrt(variance);
            return Affinity2D.NewScale(scale).Translate(-mean);
        }

        public static Matrix CovarianceMatrix(this IEnumerable<Vector> vectors)
        {
            return vectors.CovarianceMatrix(vectors.Mean());
        }

        public static Matrix CovarianceMatrix(this IEnumerable<Vector> vectors, Vector mean)
        {
            int dim = mean.Count;
            Matrix m = new Matrix(dim, dim);
            int count = 0;

            foreach (var x in vectors)
            {
                //for (int i = 0; i < dim; i++)
                //{
                //    for (int j = 0; j < dim; j++)
                //    {
                //        m[i, j] += (x[i] - mean[i]) * (x[j] - mean[j]);
                //    }
                //}
                var p = x - mean;
                m += p * p;
                count++;
            }

            return m / count;
        }

    }
}
