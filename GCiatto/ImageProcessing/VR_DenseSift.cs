﻿using BioLab.Math.Geometry;
using BioLab.GUI.Forms;
using BioLab.Common;
using System.ComponentModel;
using System;
using BioLab.ImageProcessing;
using BioLab.Math.LinearAlgebra;
using System.Collections.Generic;
using BioLab.GCiatto.Utils;

namespace BioLab.GCiatto.ImageProcessing
{
    public class VR_DenseSift : ImageOperation<Image<byte>, VR_SiftKeyPoint>
    {
        private const double defaultGaussianMagnitudeWeightFactor = 3.0;
        private const double defaultKeypointDescriptorScaleFactor = 2.0;
        private const int defaultDescriptorDimension = 4;
        private const int defaultDescriptorDirectionsCount = 8;
        private const int defaultFilterSize = 3;
        private const double defaultScale = 2;
        private const int defaultBinCount = 36;
        private const double defaultGradientMagnitudeThreshold = 0.2;

        private IntPoint2D point;
        private double scale;
        private int filterSize;

        public Point2D Point
        {
            get
            {
                return point;
            }
            set
            {
                point = (IntPoint2D)value;
            }
        }

        [AlgorithmParameter]
        [DefaultValue(defaultScale)]
        public double Scale
        {
            get
            {
                return scale;
            }
            set
            {
                if (value != scale)
                    cachedData.Invalidate();
                scale = value;
            }
        }

        [AlgorithmParameter]
        [DefaultValue(defaultBinCount)]
        public int BinCount { get; set; }

        [AlgorithmParameter]
        [DefaultValue(defaultGaussianMagnitudeWeightFactor)]
        public double GaussianMagnitudeWeightFactor { get; set; }

        [AlgorithmParameter]
        [DefaultValue(defaultKeypointDescriptorScaleFactor)]
        public double KeypointDescriptorScaleFactor { get; set; }

        [AlgorithmParameter]
        [DefaultValue(defaultFilterSize)]
        public int FilterSize {
            get
            {
                return filterSize;
            }
            set
            {
                if (value != filterSize)
                    cachedData.Invalidate();
                filterSize = value;
            }
        }

        // dimensione del lato del descrittore (4 ad esempio indica un descrittore formato da 4x4 istogrammi)
        [AlgorithmParameter]
        [DefaultValue(defaultDescriptorDimension)]
        public int DescriptorDimension { get; set; }

        // dimensionalità degli istogrammi legati al descrittore
        [AlgorithmParameter]
        [DefaultValue(defaultDescriptorDirectionsCount)]
        public int DescriptorDirectionsCount { get; set; }

        // soglia massima a cui ridurre le magnitudini dei vettori di orientazione che la superano
        public double GradientsMagnitudeThreshold { get; set; }
        public int Radius { get; set; }

        private ICache<Tuple<Image<double>, Image<double>, Image<double>>> cachedData;

        private Image<double> GaussianMap
        {
            get
            {
                return cachedData.Value.Item1;
            }
        }
        private Image<double> Magnitude
        {
            get
            {
                return cachedData.Value.Item2;
            }
        }
        private Image<double> Direction
        {
            get
            {
                return cachedData.Value.Item3;
            }
        }


        public VR_DenseSift()
        {
            cachedData = Caches.Lazy(GenerateImageAndGaussianData);
            FilterSize = defaultFilterSize;
            GaussianMagnitudeWeightFactor = defaultGaussianMagnitudeWeightFactor;
            DescriptorDimension = defaultDescriptorDimension;
            DescriptorDirectionsCount = defaultDescriptorDirectionsCount;
            Scale = defaultScale;
            BinCount = defaultBinCount;
            KeypointDescriptorScaleFactor = defaultKeypointDescriptorScaleFactor;
            GradientsMagnitudeThreshold = defaultGradientMagnitudeThreshold;
        }

        protected Tuple<Image<double>, Image<double>, Image<double>> GenerateImageAndGaussianData()
        {
            //Console.WriteLine(InputImage.ToString() + " " + InputImage.GetHashCode());
            GaussianBlurDouble gauss = new GaussianBlurDouble(InputImage.ToDoubleImage(), Scale, FilterSize);
            var gaussianMap = gauss.Execute();
            var magnAndDir = ComputeMagnitudeAndDirection(gaussianMap);
            return Tuple.Create(gaussianMap, magnAndDir.Item1, magnAndDir.Item2);
        }

        protected override void OnInputImageChanged()
        {
            cachedData.Invalidate();
            base.OnInputImageChanged();
        }

        protected virtual double GetMagnitude(int x, int y)
        {
            return Magnitude[y, x];
        }

        protected virtual double GetDirection(int x, int y)
        {
            return Direction[y, x];
        }

        public override void Run()
        {
            var keyPoints = CalculateSamplingPoints();

            // Calcolo orientazione dei keypoint
            foreach (var kp in keyPoints)
            {
                ComputeOrientation(kp);
                //kp.Orientation = Math.PI/3;
            }

            // Calcolo del descrittore
            keyPoints = CreateDescriptors(keyPoints, KeypointDescriptorScaleFactor, DescriptorDirectionsCount);
            Result = keyPoints[0];
        }

        private IList<VR_SiftKeyPoint> CalculateSamplingPoints()
        {
            return new List<VR_SiftKeyPoint> { new VR_SiftKeyPoint(point.X, point.Y, Scale) };
        }

        private Tuple<Image<double>, Image<double>> ComputeMagnitudeAndDirection(Image<double> gaussianMap)
        {
            var magnitude = new Image<double>(gaussianMap.Width, gaussianMap.Height);
            var direction = new Image<double>(gaussianMap.Width, gaussianMap.Height);

            for (int y = 1; y < gaussianMap.Height - 1; y++)
            {
                for (int x = 1; x < gaussianMap.Width - 1; x++)
                {
                    var diffx = gaussianMap[y, x + 1] - gaussianMap[y, x - 1];
                    var diffy = gaussianMap[y + 1, x] - gaussianMap[y - 1, x];
                    magnitude[y, x] = System.Math.Sqrt(diffx * diffx + diffy * diffy);
                    direction[y, x] = System.Math.Atan2(-diffy, diffx);
                }
            }

            return Tuple.Create(magnitude, direction);
        }

        private IList<VR_SiftKeyPoint> CreateDescriptors(IList<VR_SiftKeyPoint> keypoints, double scaleFactor, int directionCount)
        {
            if (keypoints.Count <= 0)
                return (keypoints);

            scaleFactor *= Scale;
            double dDim05 = ((double)DescriptorDimension) / 2.0;

            // Now calculate the radius: We consider pixels in a square with
            // dimension 'descDim' plus 0.5 in each direction. As the feature
            // vector elements at the diagonal borders are most distant from the
            // center pixel we have scale up with sqrt(2).
            Radius = (int)(((DescriptorDimension + 1.0) / 2) *
                System.Math.Sqrt(2.0) * scaleFactor + 0.5);

            // Instead of modifying the original list, we just copy the keypoints
            // that received a descriptor.
            List<VR_SiftKeyPoint> survivors = new List<VR_SiftKeyPoint>();

            // Precompute the sigma for the "center-most, border-less" gaussian
            // weighting.
            //
            // In Lowe03, page 15 it says "A Gaussian weighting function with
            // \sigma equal to one half the width of the descriptor window is
            // used", so we just use his advice.
            double sigma2Sq = 2.0 * dDim05 * dDim05;

            foreach (VR_SiftKeyPoint kp in keypoints)
            {
                // The angle to rotate with: negate the orientation.
                double angle = -kp.Orientation;

                kp.Descriptor = new Vector(DescriptorDimension * DescriptorDimension * directionCount);

                for (int y = -Radius; y < Radius; ++y)
                {
                    for (int x = -Radius; x < Radius; ++x)
                    {
                        // Rotate and scale
                        double yR = System.Math.Sin(angle) * x + System.Math.Cos(angle) * y;
                        double xR = System.Math.Cos(angle) * x - System.Math.Sin(angle) * y;

                        yR /= scaleFactor;
                        xR /= scaleFactor;

                        // Now consider all (xR, yR) that are anchored within
                        // (- descDim/2 - 0.5 ; -descDim/2 - 0.5) to
                        //    (descDim/2 + 0.5 ; descDim/2 + 0.5),
                        // as only those can influence the FV.
                        if (yR >= (dDim05 + 0.5) || xR >= (dDim05 + 0.5) ||
                            xR <= -(dDim05 + 0.5) || yR <= -(dDim05 + 0.5))
                            continue;

                        int currentX = (int)(x + kp.Position.X + 0.5);
                        int currentY = (int)(y + kp.Position.Y + 0.5);
                        if (currentX < 1 || currentX >= (Magnitude.Width - 1) || currentY < 1 || currentY >= (Magnitude.Height - 1))
                            continue;


                        // Weight the magnitude relative to the center of the
                        // whole FV. We do not need a normalizing factor now, as
                        // we normalize the whole FV later anyway (see below).
                        // xR, yR are each in -(dDim05 + 0.5) to (dDim05 + 0.5)
                        // range
                        double magW = System.Math.Exp(-(xR * xR + yR * yR) / sigma2Sq) *
                            GetMagnitude(currentX, currentY);

                        // Anchor to (-1.0, -1.0)-(dDim + 1.0, dDim + 1.0), where
                        // the FV points are located at (x, y)
                        yR += dDim05 - 0.5;
                        xR += dDim05 - 0.5;

                        // Build linear interpolation weights:
                        // A B
                        // C D
                        //
                        // The keypoint is located between A, B, C and D.
                        int[] xIdx = new int[2];
                        int[] yIdx = new int[2];
                        int[] dirIdx = new int[2];
                        double[] xWeight = new double[2];
                        double[] yWeight = new double[2];
                        double[] dirWeight = new double[2];

                        if (xR >= 0)
                        {
                            xIdx[0] = (int)xR;
                            xWeight[0] = (1.0 - (xR - xIdx[0]));
                        }
                        if (yR >= 0)
                        {
                            yIdx[0] = (int)yR;
                            yWeight[0] = (1.0 - (yR - yIdx[0]));
                        }

                        if (xR < (DescriptorDimension - 1))
                        {
                            xIdx[1] = (int)(xR + 1.0);
                            xWeight[1] = xR - xIdx[1] + 1.0;
                        }
                        if (yR < (DescriptorDimension - 1))
                        {
                            yIdx[1] = (int)(yR + 1.0);
                            yWeight[1] = yR - yIdx[1] + 1.0;
                        }

                        // Rotate the gradient direction by the keypoint
                        // orientation, then normalize to [-pi ; pi] range.
                        double dir = GetDirection(currentX, currentY) - kp.Orientation;

                        if (dir <= -System.Math.PI)
                            dir += 2.0 * System.Math.PI;
                        if (dir > System.Math.PI)
                            dir -= 2.0 * System.Math.PI;

                        double idxDir = (dir * directionCount) / (2.0 * System.Math.PI);
                        if (idxDir < 0.0)
                            idxDir += directionCount;

                        dirIdx[0] = (int)idxDir;
                        dirIdx[1] = (dirIdx[0] + 1) % directionCount;
                        dirWeight[0] = 1.0 - (idxDir - dirIdx[0]);
                        dirWeight[1] = idxDir - dirIdx[0];


                        for (int iy = 0; iy < 2; ++iy)
                        {
                            for (int ix = 0; ix < 2; ++ix)
                            {
                                for (int id = 0; id < 2; ++id)
                                {
                                    //int pos = xIdx[ix] * DescriptorDimension * DescriptorDirectionsCount + yIdx[iy] * DescriptorDirectionsCount + dirIdx[id];
                                    int pos = yIdx[iy] * DescriptorDimension * DescriptorDirectionsCount + xIdx[ix] * DescriptorDirectionsCount + dirIdx[id];
                                    if (pos < kp.Descriptor.Count)
                                        kp.Descriptor[pos] += xWeight[ix] * yWeight[iy] * dirWeight[id] * magW;
                                }
                            }
                        }
                    }
                }

                // Normalize and hicap the feature vector
                CapAndNormalizeFV(kp);

                survivors.Add(kp);
            }

            return (survivors);
        }

        /// <summary>
        /// Sogliatura e normalizzazione del feature vector (descrittore) associato a un keypoint.
        /// </summary>
        /// <param name="kp">kp corrente</param>
        private void CapAndNormalizeFV(VR_SiftKeyPoint kp)
        {
            // Straight normalization (calcolo la norma del vettore)
            double norm = 0.0;
            int dim = kp.Descriptor.Count;

            for (int n = 0; n < dim; ++n)
                norm += kp.Descriptor[n] * kp.Descriptor[n];

            norm = System.Math.Sqrt(norm);
            if (norm > 0.0)
            {
                //throw (new InvalidOperationException("CapAndNormalizeFV cannot normalize with norm = 0.0"));
                double inverseNorm = 1 / norm;

                for (int i = 0; i < dim; ++i)
                    kp.Descriptor[i] *= inverseNorm;

                // Hicap after normalization (diminuisco a "fvGradHicap" tutti gli elementi maggiori di esso)
                for (int n = 0; n < dim; ++n)
                    if (kp.Descriptor[n] > GradientsMagnitudeThreshold)
                        kp.Descriptor[n] = GradientsMagnitudeThreshold;

                // Renormalize again
                norm = 0.0;
                for (int n = 0; n < dim; ++n)
                    norm += kp.Descriptor[n] * kp.Descriptor[n];

                norm = System.Math.Sqrt(norm);
                if (norm > 0.0)
                {
                    inverseNorm = 1 / norm;

                    for (int n = 0; n < dim; ++n)
                        kp.Descriptor[n] *= inverseNorm;
                }
            }
        }

        /// <summary>
        /// Questo metodo costruisce i keypoint relativi alla posizione di "point". 
        /// Per ogni punto nell'intorno ha un peso corrispondente alla sua magnitudine per un fattore gaussiano (relativo all'area scelta). Il valore trovato sarà aggiunto alla colonna di orientazione piu simile a quella del punto.
        /// La colonna fa parte di un istogramma di "binCount" colonne(con 36 colonne, ogni colonna rappresenta un range di 10 gradi). 
        /// </summary>
        /// <param name="point">picco localizzato finemente</param>
        private void ComputeOrientation(VR_SiftKeyPoint point)
        {
            // Lowe03, "A gaussian-weighted circular window with a \sigma three times that of the scale of the keypoint".
            //
            // With \sigma = 3.0 * kpScale, the square dimension we have to
            // consider is (3 * sigma) (until the weight becomes very small).
            double sigma = GaussianMagnitudeWeightFactor * Scale;
            int radius = (int)(3.0 * sigma / 2.0 + 0.5); //3 * sigma esclude i punti dopo 3 sigma dal picco della gaussiana. il /2 è per il raggio. Lo 0.5 è per approx intera
            int radiusSq = radius * radius;

            // As the point may lie near the border, build the rectangle
            // coordinates we can still reach, minus the border pixels, for which
            // we do not have gradient information available.
            int xMin = System.Math.Max(point.Position.X - radius, 1);
            int xMax = System.Math.Min(point.Position.X + radius, InputImage.Width - 1);
            int yMin = System.Math.Max(point.Position.Y - radius, 1);
            int yMax = System.Math.Min(point.Position.Y + radius, InputImage.Height - 1);

            // Precompute 1D gaussian divisor (2 \sigma^2) in:
            double gaussianSigmaFactor = 2.0 * sigma * sigma;

            double[] bins = new double[BinCount];

            // Build the direction histogram
            for (int y = yMin; y < yMax; ++y)
            {
                for (int x = xMin; x < xMax; ++x)
                {
                    // Only consider pixels in the circle, else we might skew the
                    // orientation histogram by considering more pixels into the
                    // corner directions
                    int relX = x - point.Position.X;
                    int relY = y - point.Position.Y;
                    if (IsInCircle(relX, relY, radiusSq) == false) //controllo se il punto corrente del box è nel cerchio
                        continue;

                    // The gaussian weight factor.
                    double gaussianWeight = System.Math.Exp(-((relX * relX + relY * relY) / gaussianSigmaFactor));

                    // find the closest bin and add the direction
                    int binIdx = FindClosestRotationColumn(BinCount, GetDirection(x, y));  //cerco la colonna corrispondente all'angolo corrente
                    bins[binIdx] += GetMagnitude(x, y) * gaussianWeight;   //peso la magnitudine del punto per la campana gaussiana in quel punto e assegno alla colonna dell
                    //istogramma trovata
                }
            }

            // cerco il picco massimo dell'istogramma
            double maxGrad = 0.0;
            int maxBin = 0;
            for (int b = 0; b < BinCount; ++b)
            {
                if (bins[b] > maxGrad)
                {
                    maxGrad = bins[b];
                    maxBin = b;
                }
            }

            // All the valid keypoint bins are now marked in binIsKeypoint, now
            // build them.
            //List<VR_SiftKeyPoint> keypoints = new List<VR_SiftKeyPoint>();

            // find other possible locations
            double oneBinRad = (2.0 * System.Math.PI) / BinCount;

            // [-1.0 ; 1.0] -> [0 ; binrange], and add the fixed absolute bin position.
            // We subtract PI because bin 0 refers to 0, binCount-1 bin refers
            // to a bin just below 2PI, so -> [-PI ; PI]. 
            double degree = maxBin * oneBinRad - System.Math.PI;

            //riporto nel range
            if (degree < -System.Math.PI)
                degree += 2.0 * System.Math.PI;
            else if (degree > System.Math.PI)
                degree -= 2.0 * System.Math.PI;

            point.Orientation = degree;
        }

        /// <summary>
        /// Simple helper predicate to tell if (rX, rY) is within a circle of sqrt{radiusSq} radius, assuming the circle center is (0, 0).
        /// </summary>
        /// <param name="rX">coordinata x</param>
        /// <param name="rY">coordinata y</param>
        /// <param name="radiusSq">radius * radius</param>
        /// <returns>true se è nel cerchio passato</returns>
        private bool IsInCircle(int rX, int rY, int radiusSq)
        {
            rX *= rX;
            rY *= rY;
            if ((rX + rY) <= radiusSq)
                return (true);

            return (false);
        }

        /// <summary>
        /// Calcola l'indice della colonna dell'istogramma formato da "columnCount" colonne relativa all'angolo "angle".
        /// "angle" deve essere in -PI <= angle <= PI. Bin 0 è relativo a -PI e columnCount - 1 è relativo ad appena sotto PI
        /// </summary>
        /// <param name="columnCount">numero colonne dell'istogramma</param>
        /// <param name="angle">angolo attuale</param>
        /// <returns>indice colonna più vicina all'angolo</returns>
        private int FindClosestRotationColumn(int columnCount, double angle)
        {
            //normalizzo l'angolo rispetto un range 0, 2PI
            angle += System.Math.PI;
            angle /= 2.0 * System.Math.PI;

            // calculate the aligned bin
            angle *= columnCount;

            int idx = (int)angle;
            //if (idx == columnCount)
            //    idx = 0;
            //return (idx);

            return idx % columnCount;
        }
    }
}
