﻿using BioLab.GCiatto.Math.Geometry;
using BioLab.GCiatto.Math.LinearAlgebra;
using BioLab.ImageProcessing;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.ImageProcessing
{
    public static class MarkedRegionListExtension
    {
        public static Vector ToHighDimensionalVector(this MarkedRegionList mrl)
        {
            return LinearAlgebra.ToHighDimensionalVector(mrl.ToVectorList());
        }

        public static IList<Vector> ToVectorList(this MarkedRegionList mrl)
        {
            var list = new List<Vector>();
            foreach (var region in mrl)
            {
                foreach (var point in region.Points)
                {
                    list.Add(point.ToVector());
                }
            }
            return list;
        }

        public static MarkedRegionList ToMarkedRegionList(this IEnumerable<Vector> vectors, MarkedRegionListPattern patterns)
        {
            var e = vectors.GetEnumerator();
            var newRegions = new MarkedRegionList();
            e.MoveNext();

            foreach (var pattern in patterns.ListOfPatterns)
            {
                var newRegion = new MarkedRegion();
                newRegions.Add(newRegion);
                newRegion.Closed = pattern.Closed;

                for (var i = 0; i < pattern.NumberOfPoints; i++)
                {
                    newRegion.Points.Add(e.Current.ToIntPoint2D());
                    e.MoveNext();
                }
            }

            return newRegions;
        }
    }
}
