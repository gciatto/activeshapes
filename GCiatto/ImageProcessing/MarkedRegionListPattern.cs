﻿using BioLab.ImageProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.ImageProcessing
{
    public class MarkedRegionListPattern : ICloneable
    {
        private readonly IList<MarkedRegionPattern> listOfPatterns;
        private readonly IDictionary<MarkedRegionPattern, int> multisetOfPatterns;

        protected MarkedRegionListPattern(IEnumerable<MarkedRegionPattern> patterns)
        {
            listOfPatterns = patterns.ToList();
            multisetOfPatterns = MultisetOfPatterns(patterns);
        }

        protected static IDictionary<MarkedRegionPattern, int> MultisetOfPatterns(IEnumerable<MarkedRegionPattern> patterns)
        {
            var multiset = new Dictionary<MarkedRegionPattern, int>();
            foreach (var p in patterns)
            {
                if (multiset.ContainsKey(p))
                {
                    multiset[p] += 1;
                }
                else
                {
                    multiset[p] = 1;
                }
            }
            return multiset;
        }

        public static MarkedRegionListPattern FromMarkedRegionList(MarkedRegionList region)
        {
            return new MarkedRegionListPattern(region.Select(mr => MarkedRegionPattern.FromMarkedRegion(mr)));
        }

        public static MarkedRegionListPattern New(IEnumerable<MarkedRegionPattern> patterns)
        {
            return new MarkedRegionListPattern(patterns);
        }

        public static MarkedRegionListPattern New(params MarkedRegionPattern[] patterns)
        {
            return new MarkedRegionListPattern(patterns);
        }

        public bool Matches(MarkedRegionList regions)
        {
            if (regions.Count != NumberOfRegions)
                return false;

            var multiset = MultisetOfPatterns(listOfPatterns);
            var regionPatterns = regions.Select(mr => MarkedRegionPattern.FromMarkedRegion(mr));

            foreach (var rp in regionPatterns)
            {
                if (multiset.ContainsKey(rp))
                {
                    multiset[rp] -= 1;
                }
                else return false;
            }

            return multiset.All(kv => kv.Value == 0);
        }

        public object Clone()
        {
            return new MarkedRegionListPattern(listOfPatterns);
        }

        public IList<MarkedRegionPattern> ListOfPatterns
        {
            get { return listOfPatterns; }
        }

        public int NumberOfRegions
        {
            get { return listOfPatterns.Count; }
        }

        public override string ToString()
        {
            return String.Format(
                    "MarkedRegionListPattern[{0}]",
                    listOfPatterns
                );
        }

        public int TotalNumberOfPoints
        {
            get
            {
                return listOfPatterns
                    .Select(it => it.NumberOfPoints)
                    .Aggregate(0, (a, b) => a + b);
            }
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;
            else if (obj == null)
                return false;
            else if (obj.GetType().IsAssignableFrom(typeof(MarkedRegionListPattern)))
            {
                var other = obj as MarkedRegionListPattern;
                return multisetOfPatterns.Equals(other.multisetOfPatterns);
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            var result = 17;
            result += 31 * listOfPatterns.GetHashCode();
            return result;
        }
    }
}
