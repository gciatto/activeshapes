﻿using BioLab.Math.Geometry;
using BioLab.GUI.Forms;
using BioLab.Common;
using System.ComponentModel;
using System;
using BioLab.ImageProcessing;
using BioLab.Math.LinearAlgebra;
using System.Collections.Generic;

namespace BioLab.GCiatto.ImageProcessing
{
    public class VR_SiftKeyPoint
    {
        public IntPoint2D Position;
        public double Orientation;
        public Vector Descriptor;
        public double Scale;

        public VR_SiftKeyPoint(int x, int y, double scale)
        {
            Position = new IntPoint2D(x, y);
            Scale = scale;
        }

        public VR_SiftKeyPoint(int x, int y, double scale, double orientation)
        {
            Position = new IntPoint2D(x, y);
            Scale = scale;
            Orientation = orientation;
        }
    }
}
