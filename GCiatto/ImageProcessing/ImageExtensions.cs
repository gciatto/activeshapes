﻿using BioLab.ImageProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.ImageProcessing
{
    public static class ImageExtensions
    {

        //public static double LaplaceInterpolation(this Image<byte> image, double x, double y)
        //{
        //    return image.LaplaceInterpolation(x, y, b => b);
        //}
        //public static double LaplaceInterpolation(this Image<int> image, double x, double y)
        //{
        //    return image.LaplaceInterpolation(x, y, b => b);
        //}

        public static double LaplaceInterpolation<T>(this Image<T> image, double x, double y) where T : struct, IEquatable<T>
        {
            var x0 = System.Math.Floor(x);
            var y0 = System.Math.Floor(y);
            var x1 = x0 + 1; //System.Math.Ceiling(x);
            var y1 = y0 + 1; //System.Math.Ceiling(y);

            var wA = (x1 - x) * (y1 - y);
            var wB = (x - x0) * (y1 - y);
            var wC = (x1 - x) * (y - y0);
            var wD = (x - x0) * (y - y0);

            return image.Intensity(x0, y0) * wA +
                image.Intensity(x1, y0) * wB +
                image.Intensity(x0, y1) * wC +
                image.Intensity(x1, y1) * wD;
        }

        public static double Intensity<T>(this Image<T> image, int x, int y) where T : struct, IEquatable<T>
        {
            if (x < 0 || x >= image.Width || y < 0 || y >= image.Height)
                return 0d;
            else
                return Convert.ToDouble(image[y, x]);
        }

        public static double Intensity<T>(this Image<T> image, double x, double y) where T : struct, IEquatable<T>
        {
            return image.Intensity((int)x, (int)y);
        }
    }
}
