﻿using BioLab.ImageProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.ImageProcessing
{
    public class MarkedRegionPattern : ICloneable
    {
        protected MarkedRegionPattern(bool c, int n)
        {
            Closed = c;
            NumberOfPoints = n;
        }

        public static MarkedRegionPattern FromMarkedRegion(MarkedRegion region)
        {
            return new MarkedRegionPattern(region.Closed, region.Points.Count);
        }

        public static MarkedRegionPattern New(bool c, int n)
        {
            return new MarkedRegionPattern(c, n);
        }

        public bool Matches(MarkedRegion region)
        {
            return Closed == region.Closed && NumberOfPoints == region.Points.Count;
        }

        public bool Closed { get; }

        public int NumberOfPoints { get; }

        public object Clone()
        {
            return new MarkedRegionPattern(Closed, NumberOfPoints);
        }

        public override string ToString()
        {
            return String.Format(
                    "MarkedRegionPattern[Closed={0}, NumberOfPoits={1}]",
                    Closed,
                    NumberOfPoints
                );
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;
            else if (obj == null)
                return false;
            else if (obj.GetType().IsAssignableFrom(typeof(MarkedRegionPattern)))
            {
                var other = obj as MarkedRegionPattern;
                return Closed == other.Closed &&
                    NumberOfPoints == other.NumberOfPoints;
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            var result = 17;
            result += 31 * Closed.GetHashCode();
            result += 31 * NumberOfPoints.GetHashCode();
            return result;
        }
    }
}
