﻿using BioLab.GCiatto.Utils;
using System;
using System.Collections.Generic;

namespace Validator
{
    class EnumerableSplitter
    {
        private double percent;

        public EnumerableSplitter(double percentage)
        {
            if (percentage < 0d || percentage > 1d)
                throw new ArgumentOutOfRangeException();
            percent = percentage;
        }

        public double Percent
        {
            get
            {
                return percent;
            }
        }

        protected virtual Tuple<LinkedList<T>, LinkedList<T>> Split<T>(IEnumerable<T> e, out Tuple<int, int> sizes)
        {
            int sizeA = 0, sizeB = 0;
            LinkedList<T> a = new LinkedList<T>();
            LinkedList<T> b = new LinkedList<T>();

            foreach (T t in e)
            {
                if (Randoms.Probability(Percent))
                {
                    a.AddLast(t);
                    sizeA++;
                }
                else
                {
                    b.AddLast(t);
                    sizeB++;
                }
            }

            sizes = Tuple.Create(sizeA, sizeB);

            return Tuple.Create(a, b);
        }

        public virtual Tuple<IEnumerable<T>, IEnumerable<T>> Split<T>(IEnumerable<T> e)
        {
            Tuple<int, int> sizes;
            var t = Split(e, out sizes);
            return Tuple.Create(t.Item1 as IEnumerable<T>, t.Item2 as IEnumerable<T>);
        }
    }
}
