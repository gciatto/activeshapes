﻿using BioLab.ImageProcessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validator
{
    class Program
    {
        const string EXT = ".mkrtxt";
        static readonly string[] IMG_EXTS = new string[] { ".jpg", ".png" };

        const string ARG_PATH = "-path";
        const string ARG_PERCENT = "-percent";
        const string ARG_NUMBER_OF_TESTS = "-count";
        const string ARG_NUMBER_OF_MODES = "-modes";

        static void Main(string[] args)
        {
            var indexed = Indexed(args);

            var pStr = FindByKey(indexed, ARG_PERCENT, "8e-1");
            var p = Double.Parse(pStr);

            var splitter = new ExactEnumerableSplitter(p);

            var nStr = FindByKey(indexed, ARG_NUMBER_OF_TESTS, "10");
            var n = Int32.Parse(nStr);

            var mStr = FindByKey(indexed, ARG_NUMBER_OF_MODES, "5");
            var m = Int32.Parse(nStr);

            var path = FindByKey(indexed, ARG_PATH, ".");

            if (Directory.Exists(path))
            {
                var images = Directory.EnumerateFiles(path)
                    .Where(f => Path.GetExtension(f) == EXT)
                    .Select(f =>
                    {
                        var fi = new { File = f, Image = ImageBase.LoadFromFile(ImageFile(f)) };
                        Console.Error.WriteLine("# Loaded '{0}'", f);
                        return fi;
                    })
                    .Select(fi =>
                    {
                        var img = new ImageWithMarkedRegions(fi.Image);
                        img.LoadRegionsFromTextFile(fi.File);
                        Console.Error.WriteLine("# Loaded '{0}'", fi.File);
                        return img;
                    }).ToList();

                Console.Error.WriteLine("");

                for (int i = 1; i <= n; i++)
                {
                    Console.Error.WriteLine("# Step {0}", i);
                    var ts_vs = splitter.Split(images);

                    Validate(ts_vs, m);
                }
                
            }
            else Console.Error.WriteLine("'{0}' is not a valid directory", path);

            Console.Error.WriteLine("# Done, press enter to quit");
            Console.ReadLine();
        }

        static IEnumerable<Tuple<int, T>> Indexed<T>(IEnumerable<T> e)
        {
            return e.Select((it, i) => Tuple.Create(i, it)); 
        }

        static T FindByKey<T>(IEnumerable<Tuple<int, T>> indexed, T key)
        {
            return FindByKey(indexed, key, default(T));
        }

        static T FindByKey<T>(IEnumerable<Tuple<int, T>> indexed, T key, T def)
        {
            try
            {
                int index = indexed
                    .Where(it => it.Item2.Equals(key))
                    .First()
                    .Item1;

                var res = indexed
                    .Where(it => it.Item1 == index + 1)
                    .First()
                    .Item2;

                return res;
            }
            catch (Exception)
            {
                return def;
            }
        }

        static String ImageFile(string mkrtxt)
        {
            foreach (var ext in IMG_EXTS)
            {
                var f = Path.ChangeExtension(mkrtxt, ext);

                if (File.Exists(f))
                    return f;
            }

            throw new FileNotFoundException();
        }

        static void Validate(Tuple<IEnumerable<ImageWithMarkedRegions>, IEnumerable<ImageWithMarkedRegions>> split, int modes)
        {
            Validate(split.Item1, split.Item2, modes);
        }

        static void Validate(IEnumerable<ImageWithMarkedRegions> ts, IEnumerable<ImageWithMarkedRegions> vs, int modes) 
        {
            var v = new Validator(ts, vs, modes);

            var vres = v.Test();

            Console.WriteLine(String.Join(" | ", vres));
        }
    }
}
