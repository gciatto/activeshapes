﻿using BioLab.GCiatto.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Validator
{
    class ExactEnumerableSplitter : EnumerableSplitter
    {

        public ExactEnumerableSplitter(double percentage)
            : base(percentage)
        {

        }

        public override Tuple<IEnumerable<T>, IEnumerable<T>> Split<T>(IEnumerable<T> e)
        {
            Tuple<int, int> sizes;
            var res = base.Split(e, out sizes);
            int size = sizes.Item1 + sizes.Item2;
            int expectedSize1 = Size1(size);
            int expectedSize2 = size - expectedSize1;

            if (expectedSize1 != sizes.Item1)
            {
                int diff = expectedSize1 - sizes.Item1;
                var maxSize = Math.Max(sizes.Item1, sizes.Item2);
                var list1 = new List<T>(maxSize);
                list1.AddRange(res.Item1);
                var list2 = new List<T>(maxSize);
                list2.AddRange(res.Item2);

                for (int i = 0; diff > 0; i++, diff--)
                {
                    var r = Randoms.RandomInt(sizes.Item2 - i);
                    var t = list2.ElementAt(r);
                    list2.RemoveAt(r);
                    list1.Add(t);
                }
                for (int i = 0; diff < 0; i++, diff++)
                {
                    var r = Randoms.RandomInt(sizes.Item1 - i);
                    var t = list1.ElementAt(r);
                    list1.RemoveAt(r);
                    list2.Add(t);
                }

                return Tuple.Create(list1 as IEnumerable<T>, list2 as IEnumerable<T>);
            }
            else
            {
                return Tuple.Create(res.Item1 as IEnumerable<T>, res.Item2 as IEnumerable<T>);
            }
        }

        public int Size1(int n)
        {
            return Convert.ToInt32(
                    Math.Ceiling(n * Percent)
                );
        }

        public int Size2(int n)
        {
            return n - Size1(n);
        }
    }
}
