﻿using System;
using System.Collections.Generic;
using System.Linq;
using BioLab.ImageProcessing;
using BioLab.ActiveShapes.Model;
using BioLab.GCiatto.Utils;
using BioLab.ActiveShapes.LocalFeature;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes.Normalization;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes.Alignment;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes;
using BioLab.GCiatto.Math.LinearAlgebra;
using BioLab.Math.LinearAlgebra;
using BioLab.ActiveShapes.Search;
using BioLab.Common;
using BioLab.GCiatto.ImageProcessing;

namespace Validator
{
    class Validator
    {
        private const double DELTA_SCALE_MIN = 0.1, DELTA_SCALE_MAX = 0.2;
        private readonly Func<IRelativeFeatureExtractor<byte>> defautltExtractorProvider = () => Extractors.RadialSIFT(2);
        private readonly ShapeNormalizer defaultNormalizer = ShapeNormalizers.MeanNormalize;
        private readonly ShapesAligner defaultAligner = Aligners.ZeroCenteredAlign;
        private readonly Func<ILocalFeatureAggregator<ILocalFeature>> defaultAggregatorProvider = Aggregators.ParzenWindow;
        private readonly int defaultStepsLimit = 5;
        private readonly int defaultMovementsThreshold = 0;
        private readonly int defaultNotifyFreq = 1;


        private IEnumerable<ImageWithMarkedRegions> ts, vs;
        private int numberOfModes = 5;
        private ICacheGroup caches;
        private ICache<IActiveShapeModel> modelCache;
        private ICache<IActiveShapeInstance<byte>> instanceCache;

        public Validator(IEnumerable<ImageWithMarkedRegions> ts, IEnumerable<ImageWithMarkedRegions> vs)
        {
            this.ts = ts;
            this.vs = vs;
            modelCache = Caches.Lazy(CreateModel);
            instanceCache = Caches.Lazy(CreateInstance);
            caches = Caches.Group(modelCache, instanceCache);
        }

        public Validator(IEnumerable<ImageWithMarkedRegions> ts, IEnumerable<ImageWithMarkedRegions> vs, int nom)
            : this(ts, vs)
        {
            numberOfModes = nom;
        }

        private IActiveShapeModel CreateModel()
        {
            return ASM.Builder(ts)
                .Normalize(defaultNormalizer)
                .Align(defaultAligner)
                .SetNumberOfModes(NumberOfModes)
                .SetFeatureExtractor(defautltExtractorProvider())
                .SetAggregatorProvider(defaultAggregatorProvider)
                .Build();
        }

        private IActiveShapeInstance<byte> CreateInstance()
        {
            return Model.Instance(defautltExtractorProvider());
        }

        private IOptimalModesSearch<byte> CreateOMS(Image<byte> img) 
        {
            return Instance.OptimalModesSearch(img, defaultStepsLimit, defaultMovementsThreshold, defaultNotifyFreq);
        }

        public int NumberOfModes
        {
            get
            {
                return numberOfModes;
            }
            set
            {
                if (numberOfModes != value)
                    caches.InvalidateAll();
                numberOfModes = value;
            }
        }

        public IActiveShapeModel Model
        {
            get
            {
                return modelCache.Value;
            }
        }

        public IActiveShapeInstance<byte> Instance
        {
            get
            {
                return instanceCache.Value;
            }
        }

        private void ResetInstance()
        {
            Instance.Pose = Affinity2D.Identity;
            Instance.Modes = LinearAlgebra.Zero(NumberOfModes);
        }
        private void DeformInstance()
        {
            var pm = DeformInstance(Instance.Pose, Instance.Modes);
            if (pm.Item1 != null)
                Instance.Pose = pm.Item1;
            if (pm.Item2 != null)
                Instance.Modes = pm.Item2;
        }

        protected virtual Tuple<Affinity2D, Vector> DeformInstance(Affinity2D currPose, Vector currModes)
        {
            var ds = Randoms.RandomDoubleSign * Randoms.RandomDouble(DELTA_SCALE_MIN, DELTA_SCALE_MAX);
            var t = Affinity2D.NewScale(1 + ds);
            return Tuple.Create<Affinity2D, Vector>(currPose.Combine(t), null);
        }

        private void OnProgress(object sender, AlgorithmProgressChangedEventArgs args)
        {
            OnIntermediate(args.ProgressPercentage, Instance);
        }

        protected virtual void OnIntermediate(double percent, IActiveShapeInstance<byte> instance)
        {
            Console.Error.WriteLine("# {0:0.##} %", percent * 100);
        }

        public IList<double> Test()
        {
            var res = new List<double>(vs.Count());
            int i = 1;
            foreach (var testImage in vs)
            {
                Console.Error.WriteLine("# Image {0}", i++);
                var testShape = testImage.Regions.ToVectorList().ToShape();
                ResetInstance();

                var alignAffinity = CoupleAligners.Aligner(testShape).Affinity(Instance.Shape);
                Instance.Pose = Instance.Pose.Combine(alignAffinity);
                DeformInstance();

                var oms = CreateOMS(testImage.AsByteImage());
                oms.ProgressChanged += OnProgress;
                oms.Exectute();

                var dist = Instance.Difference(testShape);

                Console.Error.WriteLine("# Distance: {0:0.####}", dist);

                res.Add(dist);
            }

            return res;
        }
    }
}
