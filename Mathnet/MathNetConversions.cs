﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.Mathnet
{
    public static class MathnetConversions
    {
        public static MathNet.Numerics.LinearAlgebra.Matrix<double> ToMathNet(this Math.LinearAlgebra.Matrix matrix)
        {
            return MathNet.Numerics.LinearAlgebra.Double.Matrix.Build.Dense(
                    matrix.RowCount,
                    matrix.ColumnCount,
                    (i, j) => matrix[i, j]
                );
        }

        public static MathNet.Numerics.LinearAlgebra.Vector<double> ToMathNet(this Math.LinearAlgebra.Vector vector)
        {
            return MathNet.Numerics.LinearAlgebra.Double.Vector.Build.Dense(
                    vector.Count,
                    i => vector[i]
                );
        }

        public static Math.LinearAlgebra.Matrix ToBioLab<T>(this MathNet.Numerics.LinearAlgebra.Matrix<T> matrix) where T : struct, IEquatable<T>, IFormattable
        {
            var newMatrix = new Math.LinearAlgebra.Matrix(matrix.RowCount, matrix.ColumnCount);
            for (int i = 0; i < newMatrix.RowCount; i++)
            {
                for (int j = 0; j < newMatrix.ColumnCount; j++)
                {
                    newMatrix[i, j] = Convert.ToDouble(matrix.At(i, j));
                }
            }

            return newMatrix;
        }


        public static Math.LinearAlgebra.Vector ToBioLab<T>(this MathNet.Numerics.LinearAlgebra.Vector<T> vector) where T : struct, IEquatable<T>, IFormattable
        {
            var newVector = new Math.LinearAlgebra.Vector(vector.Count);
            for (int i = 0; i < newVector.Count; i++)
            {
                newVector[i] = Convert.ToDouble(vector[i]);
            }

            return newVector;
        }

        public static MathNet.Numerics.LinearAlgebra.Matrix<T> CovarianceMatrixMathNet<T>(this IEnumerable<MathNet.Numerics.LinearAlgebra.Vector<T>> vectors, int count, MathNet.Numerics.LinearAlgebra.Vector<T> mean, Converter<double, T> f) where T : struct, IEquatable<T>, IFormattable
        {
            return vectors.Select(x => {
                var p = x - mean;
                return MathNet.Numerics.LinearAlgebra.Vector<T>.OuterProduct(p, p);
            }).Aggregate((a, b) => a + b)
            .Divide(f(count));
        } 

        public static T SquaredMahalanobisDistanceInverseCovarianceMathNet<T>(this MathNet.Numerics.LinearAlgebra.Vector<T> x, MathNet.Numerics.LinearAlgebra.Matrix<T> inverseCovariance, MathNet.Numerics.LinearAlgebra.Vector<T> mean) where T : struct, IEquatable<T>, IFormattable
        {
            var p = x - mean;
            var dist = p * inverseCovariance * p;
            return dist;
        }
    }
}
