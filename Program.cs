﻿using System;
using System.Windows.Forms;
using BioLab.GUI.Forms;
using BioLab.Math.LinearAlgebra;

namespace BioLab.ActiveShapes
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new TryActiveShapeModels());
        }
    }
}
