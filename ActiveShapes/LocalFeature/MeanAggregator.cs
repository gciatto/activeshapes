﻿using BioLab.GCiatto.Math.LinearAlgebra.Distance;
using BioLab.GCiatto.Math.LinearAlgebra.Similarity;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.ActiveShapes.LocalFeature
{
    class MeanAggregator : ILocalFeatureAggregator<ILocalFeature>
    {
        private Vector accumulator;
        private int count;

        public Vector Mean
        {
            get
            {
                return accumulator / count;
            }
        }

        public int Count
        {
            get
            {
                return count;
            }
        }

        public virtual ILocalFeatureMatcher<ILocalFeature> Matcher
        {
            get
            {
                if (count == 0)
                    return null;
                else
                    return new CosineSimilarityWithMeanMatchable(accumulator, count);
            }
        }

        public virtual ILocalFeatureAggregator<ILocalFeature> Aggregate(ILocalFeature feature)
        {
            if (accumulator == null)
                accumulator = feature.Vector;
            else
                accumulator += feature.Vector;

            count++;

            return this;
        }
    }

    class CosineSimilarityWithMeanMatchable : ILocalFeatureMatcher<ILocalFeature>
    {
        public CosineSimilarityWithMeanMatchable(Vector mean)
        {
            Mean = mean;
        }
        public CosineSimilarityWithMeanMatchable(Vector accumulator, int count)
        {
            Mean = accumulator / count;
        }
        public Vector Mean { get; protected set; }

        public double ConfidenceMatches(ILocalFeature other)
        {
            return SimilarityFunctions.CosineSimilarity(Mean, other.Vector);
        }

        public virtual double FuzzyMatches(ILocalFeature other)
        {
            var cosSim = SimilarityFunctions.CosineSimilarity(Mean, other.Vector);
            return cosSim.SimilarityFromLimitedDistanceLBUB(-1, 1);
            //var cosSim = DistanceFunctions.EuclideanDistance(other.Vector, Mean);
            //return cosSim.SimilarityFromNonNegativeDistance();
        }


    }
}
