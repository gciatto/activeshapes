﻿using System;
using BioLab.Math.LinearAlgebra;
using BioLab.GCiatto.ImageProcessing;
using BioLab.ImageProcessing;
using BioLab.Math.Geometry;
using BioLab.GCiatto.Math.Geometry;

namespace BioLab.ActiveShapes.LocalFeature
{
    class SiftRadialFeatureExtractor<T> : AbstractRadialFeatureExtractor<T> where T : struct, IEquatable<T>
    {
        private RelativeSiftDescriptorExtractor siftDescriptorExtractor;

        public SiftRadialFeatureExtractor(Image<T> image, Point2D? center, int? nos, Func<Image<T>, Image<byte>> f)
            : base(image, center, nos, f)
        {
            siftDescriptorExtractor = new RelativeSiftDescriptorExtractor();
            if (Image != null && ReferenceEquals(Image, siftDescriptorExtractor.InputImage))
                siftDescriptorExtractor.InputImage = ImageConverter(Image);
        }

        public override Image<T> Image
        {
            get
            {
                return base.Image;
            }

            set
            {
                if (Image != value)
                {
                    if (siftDescriptorExtractor != null)
                        siftDescriptorExtractor.InputImage = ImageConverter(value);
                    base.Image = value;
                }
            }
        }

        public override object Clone()
        {
            return new SiftRadialFeatureExtractor<T>(Image, Center, NumberOfSamples, ImageConverter);
        }

        protected override ILocalFeature ExtractImpl(Vector point)
        {
            siftDescriptorExtractor.Scale = NumberOfSamples;
            siftDescriptorExtractor.FilterSize = NumberOfSamples + 1;
            siftDescriptorExtractor.Point = point.ToPoint2D();
            var desc = siftDescriptorExtractor.Execute().Descriptor;
            return new LocalFeature(desc);
        }
    }

    class RelativeSiftDescriptorExtractor : VR_DenseSift
    {
        public Point2D Center
        {
            get;
            set;
        }

        protected double Angle
        {
            get
            {
                var x = Point.X - Center.X;
                var y = Point.Y - Center.Y;
                return System.Math.Atan2(y, x);
            }
        }

        protected override double GetDirection(int x, int y)
        {
            var angle = base.GetDirection(x, y) - Angle;
            if (angle > System.Math.PI)
                return angle - 2 * System.Math.PI;
            else
                return angle + 2 * System.Math.PI;
        }

    }
}
