﻿using BioLab.GCiatto.Math.LinearAlgebra;
using BioLab.GCiatto.Math.LinearAlgebra.Distance;
using BioLab.GCiatto.Math.LinearAlgebra.Similarity;
using BioLab.GCiatto.Math.Statistic;
using BioLab.GCiatto.Utils;
using BioLab.Math.LinearAlgebra;
using BioLab.Mathnet;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BioLab.ActiveShapes.LocalFeature
{
    class ParzenWindowAggregator : MeanAggregator
    {
        private LinkedList<Vector> vectors;

        public ParzenWindowAggregator()
        {
            vectors = new LinkedList<Vector>();
        }

        public IEnumerable<Vector> Vectors
        {
            get
            {
                return vectors;
            }
        }

        public override ILocalFeatureMatcher<ILocalFeature> Matcher
        {
            get
            {
                return new ParzenWindowMatcher(Vectors, Count);
            }
        }

        public override ILocalFeatureAggregator<ILocalFeature> Aggregate(ILocalFeature feature)
        {
            base.Aggregate(feature);
            vectors.AddLast(feature.Vector);
            return this;
        }
    }

    class ParzenWindowMatcher : ILocalFeatureMatcher<ILocalFeature>
    {
        private static double h1 = 1;
        private readonly double h;
        private readonly Lazy<double> max;

        public ParzenWindowMatcher(IEnumerable<Vector> vectors, int count)
        {
            h = System.Math.Pow(h1 / System.Math.Sqrt(count), 1.0 / vectors.First().Count);
            Vectors = vectors;
            max = new Lazy<double>(() =>
                    vectors.Select(x => x.EstimateProbability(vectors, h))
                        .Aggregate((a, b) => a + b)
                );
            //Console.WriteLine(max);
        }

        public IEnumerable<Vector> Vectors { get; protected set; }

        public double ConfidenceMatches(ILocalFeature other)
        {
            var similarity = other.Vector.EstimateProbability(Vectors, h);
            return similarity;
        }

        public double FuzzyMatches(ILocalFeature other)
        {
            var similarity = other.Vector.EstimateProbability(Vectors, h);
            return similarity / max.Value;
        }
    }
}
