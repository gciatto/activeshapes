﻿using BioLab.Common;
using BioLab.GCiatto.Utils;
using BioLab.Math.LinearAlgebra;

namespace BioLab.ActiveShapes.LocalFeature
{
    public interface ILocalFeature : IFuzzyMatchable<ILocalFeature>
    {
        FeatureVector FeatureVector
        {
            get;
        }

        Vector Vector
        {
            get;
        }

        ILocalFeature GetCopy();
    }
}
