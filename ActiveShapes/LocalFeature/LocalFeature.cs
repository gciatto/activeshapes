﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BioLab.Common;
using BioLab.Math.LinearAlgebra;
using BioLab.GCiatto.Math.LinearAlgebra.Similarity;
using BioLab.GCiatto.Math.LinearAlgebra;

namespace BioLab.ActiveShapes.LocalFeature
{
    public class LocalFeature : ILocalFeature
    {
        private static SimilarityFunction DefaultSimilarityFunction = SimilarityFunctions.CosineSimilarity;

        private FeatureVector featureVector;

        public LocalFeature(Vector v)
        {
            Vector = v;
        }

        public LocalFeature(ILocalFeature lf)
        {
            Vector = lf.Vector.Clone();
        }

        public FeatureVector FeatureVector
        {
            get
            {
                if (featureVector == null)
                    featureVector = new FeatureVector(Vector);

                return featureVector;
            }
        }

        public Vector Vector
        {
            get;
        }

        public double FuzzyMatches(ILocalFeature other)
        {
            return DefaultSimilarityFunction(Vector, other.Vector);
        }

        public override string ToString()
        {
            return String.Format(
                    "LocalFeature[Vector={0}]",
                    LinearAlgebra.ToString(Vector)
                );
        }

        public override bool Equals(object obj)
        {
            if (this == obj)
                return true;
            else if (obj == null)
                return false;
            else if (obj.GetType().IsAssignableFrom(typeof(LocalFeature)))
            {
                var other = obj as LocalFeature;
                return Vector.Equals(other.Vector);
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            var result = 17;
            result += 31 * Vector.GetHashCode();
            return result;
        }

        public ILocalFeature GetCopy()
        {
            return new LocalFeature(this);
        }
    }
}
