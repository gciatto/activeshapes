﻿using BioLab.Math.Geometry;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.ActiveShapes.LocalFeature
{
    public interface IRelativeFeatureExtractor<T> : ILocalFeatureExtractor<T> where T : struct, IEquatable<T>
    {
        IntPoint2D CenterAsIntPoint2D
        {
            get;
            set;
        }
        Point2D Center
        {
            get;
            set;
        }

        Vector CenterAsVector
        {
            get;
            set;
        }
    }
}
