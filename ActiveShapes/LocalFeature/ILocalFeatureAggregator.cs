﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.ActiveShapes.LocalFeature
{
    public interface ILocalFeatureAggregator<T> where T : ILocalFeature
    {
        ILocalFeatureMatcher<T> Matcher
        {
            get;
        }

        ILocalFeatureAggregator<T> Aggregate(T feature);
    }
}
