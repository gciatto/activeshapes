﻿using BioLab.GCiatto.Math.Geometry;
using BioLab.ImageProcessing;
using BioLab.Math.Geometry;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.ActiveShapes.LocalFeature
{
    public static class Extractors
    {
        public static IRelativeFeatureExtractor<byte> RadialIntensityDifferences()
        {
            return RadialIntensityDifferences(null);
        }

        public static IRelativeFeatureExtractor<byte> RadialIntensityDifferences(int numberOfSamples)
        {
            return RadialIntensityDifferences(null, numberOfSamples);
        }

        public static IRelativeFeatureExtractor<byte> RadialIntensityDifferences(this Image<byte> img)
        {
            return new RadialIntensityDifferencesFeatureExtractor<byte>(img, null, null, i => i);
        }

        public static IRelativeFeatureExtractor<byte> RadialIntensityDifferences(this Image<byte> img, int numberOfSamples)
        {
            return new RadialIntensityDifferencesFeatureExtractor<byte>(img, null, numberOfSamples, i => i);
        }


        /************************************************************************************/

        public static IRelativeFeatureExtractor<byte> RadialGradientSamples()
        {
            return RadialGradientSamples(null);
        }

        public static IRelativeFeatureExtractor<byte> RadialGradientSamples(this Image<byte> img)
        {
            return new RadialGradientSamplesFeatureExtractor<byte>(img, null, null, i => i.ToByteImage());
        }

        public static IRelativeFeatureExtractor<byte> RadialGradientSamples(this Image<byte> img, int numberOfSamples)
        {
            return new RadialGradientSamplesFeatureExtractor<byte>(img, null, numberOfSamples, i => i.ToByteImage());
        }

        public static IRelativeFeatureExtractor<byte> RadialGradientSamples(byte numberOfSamples)
        {
            return RadialGradientSamples(null, numberOfSamples);
        }

        public static IRelativeFeatureExtractor<byte> RadialSIFT()
        {
            return RadialSIFT(null);
        }

        public static IRelativeFeatureExtractor<byte> RadialSIFT(int nos)
        {
            return new SiftRadialFeatureExtractor<byte>(null, null, nos, i => i.ToByteImage());
        }

        public static IRelativeFeatureExtractor<byte> RadialSIFT(this Image<byte> img)
        {
            return new SiftRadialFeatureExtractor<byte>(img, null, null, i => i.ToByteImage());
        }

        public static IRelativeFeatureExtractor<byte> RadialSIFT(this Image<byte> img, int nos)
        {
            return new SiftRadialFeatureExtractor<byte>(img, null, nos, i => i.ToByteImage());
        }

    }
}
