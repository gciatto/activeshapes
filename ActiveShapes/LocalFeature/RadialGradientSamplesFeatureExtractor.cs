﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BioLab.Math.LinearAlgebra;
using BioLab.ImageProcessing;
using BioLab.Math.Geometry;
using BioLab.GCiatto.Math.LinearAlgebra;

namespace BioLab.ActiveShapes.LocalFeature
{
    class RadialGradientSamplesFeatureExtractor<T> : AbstractRadialFeatureExtractor<T> where T : struct, IEquatable<T>
    {
        public RadialGradientSamplesFeatureExtractor(Image<T> image, Point2D? center, int? nos, Func<Image<T>, Image<byte>> f)
            : base(image, center, nos, f)
        {
            
        }

        public override object Clone()
        {
            return new RadialGradientSamplesFeatureExtractor<T>(Image, Center, NumberOfSamples, ImageConverter);
        }

        protected override ILocalFeature ExtractImpl(Vector point)
        {
            var totNumOfSamples = 2 * NumberOfSamples + 1;
            var center = CenterAsVector;
            var dir = LinearAlgebra.Versor(point, center);
            double[] samples = new double[totNumOfSamples];

            for (int i = -NumberOfSamples, j = 0; j < totNumOfSamples; i++, j++)
            {
                var samplePoint = point + dir * ((double)i);
                samples[j] = GetGradient(samplePoint[0], samplePoint[1]).ComputeDotProduct(dir);
            }

            return new LocalFeature(Normalized(new Vector(samples)));
        }
    }
}
