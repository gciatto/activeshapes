﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BioLab.ImageProcessing;
using BioLab.Math.Geometry;
using BioLab.Math.LinearAlgebra;
using BioLab.GCiatto.Math.LinearAlgebra;
using BioLab.GCiatto.Math.Geometry;
using BioLab.GCiatto.ImageProcessing;
using BioLab.GCiatto.Utils;

namespace BioLab.ActiveShapes.LocalFeature
{
    abstract class AbstractRadialFeatureExtractor<T> : IRelativeFeatureExtractor<T> where T : struct, IEquatable<T>
    {
        private static GradientCalculationMethod DefaultGradientCalculationMethod = GradientCalculationMethod.Sobel;

        private Point2D center = new Point2D(0, 0);
        private int numberOfSamples = 4;
        
        private Image<T> image;
        private ICache<GradientCalculation> gradientCalc;
        private bool normalize = true;

        private Func<Image<T>, Image<byte>> imageConverter;

        protected Func<Image<T>, Image<byte>> ImageConverter
        {
            get
            {
                return imageConverter;
            }
        }

        public AbstractRadialFeatureExtractor(Image<T> image, Point2D? center, int? nos, Func<Image<T>, Image<byte>> f)
        {
            gradientCalc = Caches.Lazy(CalculateGradient);
            if (image != null) Image = image;
            if (center.HasValue) Center = center.Value;
            if (nos.HasValue)
            {
                if (nos.Value > 0)
                    NumberOfSamples = nos.Value;
                else
                    throw new ArgumentException();
            }
            if (f != null)
                imageConverter = f;
            else
                throw new ArgumentException();
        }

        public IntPoint2D CenterAsIntPoint2D
        {
            get { return center.ToIntPoint2D(); }
            set { center = value; }
        }

        public int NumberOfSamples
        {
            get
            {
                return numberOfSamples;
            }
            set
            {
                numberOfSamples = value;
            }
        }

        //private void InvalidateImageCache()
        //{
        //    imageEdited = true;
        //}

        //private void UpdateImageCache()
        //{
        //    if (imageEdited)
        //    {
        //        gradientCalc = new GradientCalculation(imageConverter(Image), DefaultGradientCalculationMethod);
        //        gradientCalc.Run();
        //        imageEdited = false;
        //    }
        //}

        private GradientCalculation CalculateGradient()
        {
            var gradientCalc = new GradientCalculation(imageConverter(Image), DefaultGradientCalculationMethod);
            gradientCalc.Run();
            return gradientCalc;
        }

        public Point2D Center
        {
            get { return center; }
            set
            {
                center = value;
            }
        }

        public Vector CenterAsVector
        {
            get { return center.ToVector(); }
            set
            {
                center = value.ToPoint2D();
            }
        }

        public virtual Image<T> Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
                gradientCalc.Invalidate();
            }
        }

        protected Tuple<Image<int>, Image<int>> Gradient
        {
            get
            {
                var gradientCalc = this.gradientCalc.Value;
                return new Tuple<Image<int>, Image<int>>(gradientCalc.GradientX, gradientCalc.GradientY);
            }
        }

        protected Vector GetGradient(double x, double y)
        {
            var g = Gradient;
            return LinearAlgebra.Vector(
                    g.Item1.LaplaceInterpolation(x, y),
                    g.Item2.LaplaceInterpolation(x, y)
                );
        }

        public ILocalFeature Extract(Point2D point)
        {
            return ExtractImpl(LinearAlgebra.Vector(point.X, point.Y));
        }

        public ILocalFeature Extract(Vector point)
        {
            if (point.Count == 2)
                return ExtractImpl(point);
            else
                throw new ArgumentException();
        }

        protected abstract ILocalFeature ExtractImpl(Vector point);

        public bool Normalize
        {
            get
            {
                return normalize;
            }
            set
            {
                normalize = value;
            }
        }

        private double adjustFactor = 1000;

        protected Vector Normalized(Vector v)
        {
            if (Normalize)
            {
                var norm = v.ComputeL1Norm();
                v.ApplyToEachComponent(it => it * (adjustFactor / norm));
            }
            return v;
        }

        public ILocalFeature Extract(IntPoint2D point)
        { 
            return ExtractImpl(LinearAlgebra.Vector(point.X, point.Y));
        }

        public ILocalFeature Extract(double x, double y)
        {
            return ExtractImpl(LinearAlgebra.Vector(x, y));
        }

        public ILocalFeature Extract(int x, int y)
        {
            return ExtractImpl(LinearAlgebra.Vector(x, y));
        }

        public abstract object Clone();
    }
}
