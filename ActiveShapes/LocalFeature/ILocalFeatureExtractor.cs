﻿using BioLab.ImageProcessing;
using BioLab.Math.Geometry;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.ActiveShapes.LocalFeature
{
    public interface ILocalFeatureExtractor<T> : ICloneable where T : struct, IEquatable<T>
    {
        Image<T> Image
        {
            get;
            set;
        }

        ILocalFeature Extract(int x, int y);
        ILocalFeature Extract(double x, double y);
        ILocalFeature Extract(IntPoint2D point);
        ILocalFeature Extract(Point2D point);
        ILocalFeature Extract(Vector point);
    }
}
