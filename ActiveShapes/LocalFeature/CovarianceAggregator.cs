﻿using BioLab.GCiatto.Math.LinearAlgebra.Distance;
using BioLab.GCiatto.Math.LinearAlgebra.Similarity;
using BioLab.GCiatto.Math.Statistic;
using BioLab.GCiatto.Utils;
using BioLab.Math.LinearAlgebra;
using BioLab.Mathnet;
using System;
using System.Collections.Generic;

namespace BioLab.ActiveShapes.LocalFeature
{
    class CovarianceAggregator : MeanAggregator
    {
        private LinkedList<Vector> vectors;

        private ICache<Matrix> covarianceMatrix;

        public CovarianceAggregator()
        {
            vectors = new LinkedList<Vector>();
            covarianceMatrix = Caches.Lazy(CalculateMatrix);
        }

        public Matrix CovarianceMatrix
        {
            get
            {
                return covarianceMatrix.Value;
            }
        }

        public IEnumerable<Vector> Vectors
        {
            get
            {
                return vectors;
            }
        }

        protected Matrix CalculateMatrix()
        {
            return Statistic.CovarianceMatrix(vectors, Mean);
        }

        public override ILocalFeatureMatcher<ILocalFeature> Matcher
        {
            get
            {
                return new MahalanobisMatcher(covarianceMatrix.Value, Mean);
            }
        }

        public override ILocalFeatureAggregator<ILocalFeature> Aggregate(ILocalFeature feature)
        {
            base.Aggregate(feature);
            vectors.AddLast(feature.Vector);
            covarianceMatrix.Invalidate();
            return this;
        }
    }

    class MahalanobisMatcher : CosineSimilarityWithMeanMatchable
    {
        private Lazy<Matrix> inverseCovarianceMatrix;

        public MahalanobisMatcher(Matrix covariance, Vector mean)
            : base(mean)
        {
            inverseCovarianceMatrix = new Lazy<Matrix>(() => {

                //if (covariance.ComputeDeterminant() < 1e-11)
                //    Console.Error.WriteLine("singular matrix");
                //return covariance.ComputePseudoInverse();
                var inverse = covariance.ToMathNet().Inverse();
                var inverseBiolab = inverse.ToBioLab();
                return inverseBiolab;
            });
            //Console.Out.WriteLine(InverseOfCovarianceMatrix);
        }

        public Matrix InverseOfCovarianceMatrix
        {
            get
            {
                return inverseCovarianceMatrix.Value;
            }
        }

        public override double FuzzyMatches(ILocalFeature other)
        {
            var mDistance = DistanceFunctions.MahalanobisDistanceInverseCovariance(other.Vector, Mean, InverseOfCovarianceMatrix);
            return mDistance.SimilarityFromNonNegativeDistance();
        }
    }
}
