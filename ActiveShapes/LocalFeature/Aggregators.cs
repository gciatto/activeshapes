﻿using BioLab.GCiatto.Math.LinearAlgebra;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.ActiveShapes.LocalFeature
{
    public static class Aggregators
    {
        public static ILocalFeatureAggregator<ILocalFeature> MeanAggregator()
        {
            return new MeanAggregator();
        }

        public static ILocalFeatureAggregator<ILocalFeature> CovarianceAggregator()
        {
            return new CovarianceAggregator();
        }

        public static ILocalFeatureAggregator<ILocalFeature> CovarianceAggregatorMathNet()
        {
            return new CovarianceAggregatorMathNet();
        }

        public static ILocalFeatureAggregator<ILocalFeature> ParzenWindow()
        {
            return new ParzenWindowAggregator();
        }
    }
}
