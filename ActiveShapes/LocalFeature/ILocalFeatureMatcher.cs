﻿using BioLab.GCiatto.Utils;

namespace BioLab.ActiveShapes.LocalFeature
{
    public interface ILocalFeatureMatcher<T> : IFuzzyMatchable<T>, IConfidenceMatchable<T> where T : ILocalFeature
    {

    }
}
