﻿using BioLab.GCiatto.Math.LinearAlgebra.Similarity;
using BioLab.GCiatto.Utils;
using BioLab.Mathnet;
using System;
using System.Collections.Generic;

namespace BioLab.ActiveShapes.LocalFeature
{
    class CovarianceAggregatorMathNet : ILocalFeatureAggregator<ILocalFeature>
    {
        private LinkedList<MathNet.Numerics.LinearAlgebra.Vector<double>> vectors = 
            new LinkedList<MathNet.Numerics.LinearAlgebra.Vector<double>>();

        private MathNet.Numerics.LinearAlgebra.Vector<double> accumulator;

        private int count = 0;

        private ICache<MathNet.Numerics.LinearAlgebra.Matrix<double>> covarianceMatrix;

        public CovarianceAggregatorMathNet()
        {
            covarianceMatrix = Caches.Lazy(GenerateCovarianceMatrix);
        }

        public MathNet.Numerics.LinearAlgebra.Vector<double> Mean
        {
            get
            {
                return accumulator.Divide(count);
            }
        }

        public ILocalFeatureMatcher<ILocalFeature> Matcher
        {
            get
            {
                return new MahalanobisMatcherMathNet(covarianceMatrix.Value, Mean);
            }
        }

        protected MathNet.Numerics.LinearAlgebra.Matrix<double> GenerateCovarianceMatrix()
        {
            return vectors.CovarianceMatrixMathNet(count, Mean, x => x);
        }

        public ILocalFeatureAggregator<ILocalFeature> Aggregate(ILocalFeature feature)
        {
            var fVector = feature.Vector.ToMathNet();
            if (accumulator == null)
                accumulator = fVector;
            else
                accumulator += fVector;
            vectors.AddLast(fVector);
            count++;
            covarianceMatrix.Invalidate();
            return this;
        }
    }

    class MahalanobisMatcherMathNet : ILocalFeatureMatcher<ILocalFeature>
    {
        private Lazy<MathNet.Numerics.LinearAlgebra.Matrix<double>> inverseCovarianceMatrix;
        private MathNet.Numerics.LinearAlgebra.Vector<double> mean;

        public MahalanobisMatcherMathNet(MathNet.Numerics.LinearAlgebra.Matrix<double> covariance, MathNet.Numerics.LinearAlgebra.Vector<double> mean)
        {
            this.mean = mean;
            inverseCovarianceMatrix = new Lazy<MathNet.Numerics.LinearAlgebra.Matrix<double>>(() => covariance.Inverse());
        }

        public MathNet.Numerics.LinearAlgebra.Matrix<double> InverseOfCovarianceMatrix
        {
            get
            {
                return inverseCovarianceMatrix.Value;
            }
        }

        public double ConfidenceMatches(ILocalFeature other)
        {
            var mDistance = other.Vector.ToMathNet().SquaredMahalanobisDistanceInverseCovarianceMathNet(InverseOfCovarianceMatrix, mean);
            return -mDistance;
        }

        public double FuzzyMatches(ILocalFeature other)
        {
            var mDistance = other.Vector.ToMathNet().SquaredMahalanobisDistanceInverseCovarianceMathNet(InverseOfCovarianceMatrix, mean);
            return System.Math.Sqrt(mDistance).SimilarityFromNonNegativeDistance();
        }
    }
}
