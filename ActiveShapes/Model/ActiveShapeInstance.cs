﻿using System;
using System.Collections.Generic;
using System.Linq;
using BioLab.ActiveShapes.LocalFeature;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes;
using BioLab.ImageProcessing;
using BioLab.Math.Geometry;
using BioLab.Math.LinearAlgebra;
using BioLab.GCiatto.Math.LinearAlgebra;
using BioLab.GCiatto.Utils;
using BioLab.GCiatto.Math.Geometry;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes.Alignment;

namespace BioLab.ActiveShapes.Model
{
    class ActiveShapeInstance<T> : IActiveShapeInstance<T> where T : struct, IEquatable<T>
    {
        private ILocalFeatureExtractor<T> extractor;
        //private readonly WeakReference<IActiveShapeModel> model;
        private IActiveShapeModel model;
        private Vector modes;
        //private Point2D position = new Point2D(0, 0);
        //private double angle = 0d, scale = 1d;
        private readonly ICache<Affinity2D> /*affineTransformCache,*/ inverseTransformCache;
        private readonly ICache<Shape> shapeCache;
        private readonly ICacheGroup allCaches;
        private Affinity2D pose = Affinity2D.Identity;

        public ActiveShapeInstance(IActiveShapeModel model, ILocalFeatureExtractor<T> extractor)
        {
            //this.model = new WeakReference<IActiveShapeModel>(model);
            //affineTransformCache = Caches.Lazy(GenerateTransformation);
            inverseTransformCache = Caches.Lazy(GenerateInverseTransformation);
            shapeCache = Caches.Lazy(GenerateShape);
            allCaches = Caches.Group(shapeCache, /*affineTransformCache,*/ inverseTransformCache);
            Model = model;
            Extractor = extractor;
            Modes = LinearAlgebra.Zero(Model.NumberOfModes);
        }

        public ILocalFeatureExtractor<T> Extractor
        {
            get
            {
                return extractor;
            }
            protected set
            {
                if (value == null) throw new ArgumentNullException("null extractor");
                extractor = value;
            }
        }

        public IActiveShapeModel Model
        {
            get
            {
                //IActiveShapeModel m;
                //if (!model.TryGetTarget(out m))
                //{
                //    throw new InvalidOperationException("lost reference to model");
                //}
                //return m;
                return model;
            }
            protected set
            {
                if (value == null) throw new ArgumentNullException("null model");
                model = value;
            }
        }

        public Vector Modes
        {
            get
            {
                return modes;
            }

            set
            {
                if (value == null) throw new ArgumentNullException("null modes vector");
                if (value.Count != Model.NumberOfModes) throw new ArgumentException("wrong dimension of nodes vector");
                if (value == modes) return;
                modes = value;
                for (int i = 0; i < Model.NumberOfModes; i++)
                {
                    var modeRange = Model.GetModeRange(i);
                    if (modes[i] > modeRange)
                        modes[i] = modeRange;
                    else if (modes[i] < -modeRange)
                        modes[i] = -modeRange;
                }
                allCaches.InvalidateAll();
            }
        }

        /*public Point2D Position
        {
            get
            {
                return position;
            }

            set
            {
                position.X = value.X;
                position.Y = value.Y;
                allCaches.InvalidateAll();
            }
        }*/

        /*public double Rotation
        {
            get
            {
                return angle;
            }

            set
            {
                angle = value;
                allCaches.InvalidateAll();
            }
        }*/

        /*public double Scale
        {
            get
            {
                return scale;
            }

            set
            {
                scale = value;
                allCaches.InvalidateAll();
            }
        }*/

        protected Affinity2D Transformation
        {
            get
            {
                return Pose;
            }
        }

        protected Affinity2D InverseTransformation
        {
            get
            {
                return inverseTransformCache.Value;
            }
        }

        /*protected Affinity2D GenerateTransformation()
        {
            return Affinity2D
                .NewRotation(Rotation)
                .Scale(Scale)
                .Translate(Position.ToVector())
                .Combine(furtherTransform);
        }*/

        protected Affinity2D GenerateInverseTransformation()
        {
            /*return furtherTransform.Inverse
                .Combine(Affinity2D
                .NewRotation(-Rotation)
                .Scale(1 / Scale)
                .Translate(-Position.ToVector()));*/

            return Pose.Inverse;
        }

        protected Shape GenerateShape()
        {
            var hdVector = Model.BackProjection(Modes);
            var ldVectors = hdVector.ToLowDimensionalVectors(2);

            return ldVectors
                .Select(it => Transformation.Apply(it))
                .ToShape();
        }

        public Shape Shape
        {
            get
            {
                return shapeCache.Value;
            }
        }

        public Affinity2D Pose
        {
            get
            {
                return pose;
            }

            set
            {
                if (value == null) throw new ArgumentNullException();
                pose = value;
                allCaches.InvalidateAll();
            }
        }

        public void Adapt(ImageWithMarkedRegions image)
        {
            image.Regions = Shape.ToMarkedRegionList(Model.RegionsPattern);
        }

        public double FuzzyMatches(Image<T> other)
        {
            return FuzzyMatchings(other)
                .Aggregate(0d, (a, b) => a + b)
                / Model.NumberOfPoints; 
        }

        public IList<double> FuzzyMatchings(Image<T> other)
        {
            ConfigureExtractor(other);
            return Shape
                .Points
                .Select((p, i) => FuzzyMatching(other, i, p))
                .ToList();
        }

        protected double FuzzyMatching(Image<T> other, int i, Vector p)
        {
            var matcher = Model.FeatureMatchers[i];
            ILocalFeature f = Extractor.Extract(p);
            var match = matcher.FuzzyMatches(f);
            if (match < 0 || match > 1)
                throw new Exception("implementation error");
            return match;
        }

        protected double ConfidenceMatchingImpl(Image<T> other, int i, Vector p)
        {
            var matcher = Model.FeatureMatchers[i];
            ILocalFeature f = Extractor.Extract(p);
            var match = matcher.ConfidenceMatches(f);
            return match;
        }

        protected void ConfigureExtractor(Image<T> other)
        {
            Extractor.Image = other;
            if (typeof(IRelativeFeatureExtractor<T>).IsAssignableFrom(Extractor.GetType()))
            {
                var relativeExtractor = Extractor as IRelativeFeatureExtractor<T>;
                relativeExtractor.Center = Pose.Translation.ToPoint2D();
            }
            else
            {
                Console.Error.WriteLine("warning 2");
            }
        }

        public double FuzzyMatching(Image<T> other, int index)
        {
            ConfigureExtractor(other);
            return FuzzyMatching(other, index, Shape.Points.ElementAt(index));
        }

        public Vector ModesVariation(Shape after)
        {
            var hdAfter = after.Points
                .Select(it => InverseTransformation.Apply(it))
                .ToHighDimensionalVector();
            //var afterModes = Model.Projection(hdAfter);
            //return afterModes - Modes;
            var hdBefore = Shape.Points
                .Select(it => InverseTransformation.Apply(it))
                .ToHighDimensionalVector();

            return model.Projection(hdAfter - hdBefore);
        }

        public IList<double> ConfidenceMatchings(Image<T> other)
        {
            ConfigureExtractor(other);
            return Shape
                .Points
                .Select((p, i) => ConfidenceMatchingImpl(other, i, p))
                .ToList();
        }

        public double ConfidenceMatching(Image<T> other, int index)
        {
            return ConfidenceMatchingInPoint(other, index, Shape.Points.ElementAt(index));
        }

        public double ConfidenceMatches(Image<T> other)
        {
            return ConfidenceMatchings(other)
                .Aggregate(0d, (a, b) => a + b);
        }

        public double ConfidenceMatchingInPoint(Image<T> other, int index, Vector point)
        {
            ConfigureExtractor(other);
            return ConfidenceMatchingImpl(other, index, point);
        }

        public void AlignTo(Shape shape)
        {
            var t = CoupleAligners.Aligner(shape).Affinity(Shape);
            Pose = Pose.Combine(t);
        }

        public double Difference(Shape shape)
        {
            var diff = shape.HighDimensionalVector - Shape.HighDimensionalVector;
            return diff.ComputeL2Norm();
        }

        /*public void Transform(Affinity2D t)
        {

            Position = t.OnlyTranslation().Apply(Position);
            furtherTransform = furtherTransform.Combine(t.OnlyTransformation());
        }*/

        /*public void Reset()
        {
            furtherTransform = Affinity2D.Identity;
            Modes = LinearAlgebra.Zero(Model.NumberOfModes);
            Rotation = 0;
            Position = new Point2D();
            Scale = 1;
        }*/


    }
}
