﻿using BioLab.ActiveShapes.LocalFeature;
using BioLab.DimensionalityReduction;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using BioLab.GCiatto.ImageProcessing;

namespace BioLab.ActiveShapes.Model
{
    class ActiveShapeModel : IActiveShapeModel
    {
        private readonly IList<ILocalFeatureMatcher<ILocalFeature>> featureMatchers;
        private readonly KLTransform klTransform;
        private readonly MarkedRegionListPattern regionsPattern;
        private readonly int numOfModes;
        private readonly int numOfPoints;

        public ActiveShapeModel(KLTransform klTransform, IList<ILocalFeatureMatcher<ILocalFeature>> featureMatchers, MarkedRegionListPattern regionsPattern, int numOfModes, int numOfPoints)
        {
            this.klTransform = klTransform;
            this.featureMatchers = featureMatchers;
            this.regionsPattern = regionsPattern;
            this.numOfModes = numOfModes;
            this.numOfPoints = numOfPoints;
        }

        public ActiveShapeModel(KLTransform transform, IList<ILocalFeatureMatcher<ILocalFeature>> featureMatchers)
        {
            if (transform != null)
                this.klTransform = transform;
            else
                throw new ArgumentNullException();

            if (featureMatchers != null)
                this.featureMatchers = featureMatchers;
            else
                throw new ArgumentNullException();

        }

        public IList<ILocalFeatureMatcher<ILocalFeature>> FeatureMatchers
        {
            get
            {
                return featureMatchers;
            }
        }

        public int NumberOfModes
        {
            get
            {
                return numOfModes;
            }
        }

        public int NumberOfPoints
        {
            get
            {
                return numOfPoints;
            }
        }

        public MarkedRegionListPattern RegionsPattern
        {
            get
            {
                return regionsPattern;
            }
        }

        public Vector BackProjection(Vector modes)
        {
            return klTransform.BackProjection(modes);
        }

        public Vector Projection(Vector shape)
        {
            return klTransform.Reduce(shape);
        }

        public double GetModeRange(int index)
        {
            //return 3 * Math.Sqrt(KL.GetEigenvalue(index)); // 3 sigma
            return 3 * System.Math.Sqrt(klTransform.Eigenvalues[index]); // 3 sigma
        }

        public Vector ModesVariation(Vector before, Vector after)
        {
            return Projection(after) - Projection(before);
        }
    }
}
