﻿using BioLab.ActiveShapes.LocalFeature;
using BioLab.GCiatto.ImageProcessing;
using BioLab.GCiatto.Math.LinearAlgebra;
using BioLab.ImageProcessing;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.ActiveShapes.Model
{
    public static class ASM
    {
        public static IActiveShapeModelBuilder Builder()
        {
            return new ActiveShapeModelBuilder();
        }

        public static IActiveShapeModelBuilder Builder(this IEnumerable<ImageWithMarkedRegions> ts)
        {
            return new ActiveShapeModelBuilder().SetTrainingSet(ts);
        }
        
        public static void Adapt(this IActiveShapeModel model, ImageWithMarkedRegions image, Vector modes)
        {
            image.Regions = model.BackProjection(modes)
                .ToLowDimensionalVectors(2)
                .ToMarkedRegionList(model.RegionsPattern);
        }

        public static IActiveShapeInstance<byte> Instance(this IActiveShapeModel model, ILocalFeatureExtractor<byte> extractor)
        {
            return new ActiveShapeInstance<byte>(model, extractor);
        }

        public static IActiveShapeInstance<byte> Instance(this IActiveShapeModel model)
        {
            return model.Instance(Extractors.RadialIntensityDifferences());
        }
    }
}
