﻿using BioLab.ActiveShapes.LocalFeature;
using BioLab.Common;
using BioLab.DimensionalityReduction;
using BioLab.GCiatto.ImageProcessing;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes.Alignment;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes.Normalization;
using BioLab.GCiatto.Math.Statistic;
using BioLab.GCiatto.Utils;
using BioLab.ImageProcessing;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BioLab.ActiveShapes.Model
{
    class ActiveShapeModelBuilder : IActiveShapeModelBuilder
    {
        private LinkedList<ShapesAligner> aligners = new LinkedList<ShapesAligner>();
        private LinkedList<ShapeNormalizer> normalizers = new LinkedList<ShapeNormalizer>();
        private IEnumerable<ImageWithMarkedRegions> trainingSet;
        private ILocalFeatureExtractor<byte> featureExtractor = Extractors.RadialIntensityDifferences();
        private Func<ILocalFeatureAggregator<ILocalFeature>> aggregatorProvider = LocalFeature.Aggregators.MeanAggregator;
        private int numberOfModes;
        private int numberOfPoints;
        private MarkedRegionListPattern regionsPattern;
        private IList<ILocalFeatureAggregator<ILocalFeature>> aggregators;

        public ActiveShapeModelBuilder()
        {
            normalizers.AddLast(ShapeNormalizers.MeanNormalize);
        }

        public IList<ILocalFeatureAggregator<ILocalFeature>> Aggregators
        {
            get { return aggregators; }
        }

        public Func<ILocalFeatureAggregator<ILocalFeature>> AggregatorProvider
        {
            get { return aggregatorProvider; }
            set
            {
                if (value == null) throw new ArgumentNullException();
                aggregatorProvider = value;
            }
        }

        public IActiveShapeModelBuilder Align(ShapesAligner aligner)
        {
            if (aligner == null) throw new ArgumentNullException();
            aligners.AddLast(aligner);
            return this;
        }

        public IActiveShapeModel Build()
        {
            CheckCanBuild();
            InitializeBuiling();

            var shapes = TrainingSet
                .Peek(CheckImage)
                .Select(ExtractFeatures)
                .Select(ShapeExtensions.ToShape)
                .Select(NormalizeShape)
                .ToList();

            var vs = new FeatureVectorSet();
            AlignAll(shapes)
                .Select(it => it.HighDimensionalVector)
                .Select(it => new FeatureVector(it, false))
                .ForEach(it => vs.Add(it, false));

            var matchers = aggregators.Select(it => it.Matcher).ToList();
            var kl = new KLTransformBuilder(vs, NumberOfModes).Calculate() as KLTransform;

            return new ActiveShapeModel(
                    kl,
                    matchers,
                    regionsPattern,
                    NumberOfModes,
                    NumberOfPoints
                );
        }

        protected IEnumerable<Shape> AlignAll(IEnumerable<Shape> shapes)
        {
            foreach (var a in aligners)
            {
                shapes = a.Invoke(shapes);
            }

            return shapes;
        }

        protected Shape NormalizeShape(Shape s)
        {
            foreach (var f in normalizers)
            {
                s = f(s);
            }

            return s;
        }

        protected void ConfigureFeatureExtractor(ImageWithMarkedRegions image, IList<Vector> points)
        {
            FeatureExtractor.Image = image.AsByteImage();
            if (typeof(IRelativeFeatureExtractor<byte>).IsAssignableFrom(FeatureExtractor.GetType()))
            {
                var relativeFeatureExtractor = FeatureExtractor as IRelativeFeatureExtractor<byte>;
                relativeFeatureExtractor.CenterAsVector = points.Mean();
            }
            else
            {
                Console.Error.WriteLine("warning 1");
            }
        }

        protected IEnumerable<Vector> ExtractFeatures(ImageWithMarkedRegions image)
        {
            var points = image.Regions.ToVectorList();
            ConfigureFeatureExtractor(image, points);
            for (int i = 0; i < points.Count; i++)
            {
                var feature = FeatureExtractor.Extract(points[i]);
                Aggregators[i].Aggregate(feature);
            }
            return points;
        }

        protected void CheckImage(ImageWithMarkedRegions image)
        {
            if (!RegionsPattern.Matches(image.Regions))
                throw new InvalidOperationException("image regions don't match");
        }

        protected void InitializeBuiling()
        {
            RegionsPattern = MarkedRegionListPattern.FromMarkedRegionList(TrainingSet.First().Regions);
            NumberOfPoints = RegionsPattern.TotalNumberOfPoints;
            aggregators = new List<ILocalFeatureAggregator<ILocalFeature>>(NumberOfPoints);
            for (int i = 0; i < NumberOfPoints; i++)
            {
                aggregators.Add(AggregatorProvider());
            }
        }

        protected void CheckCanBuild()
        {
            if (TrainingSet == null) throw new InvalidOperationException("null ts");
            if (FeatureExtractor == null) throw new InvalidOperationException("null feature extractor");
            if (AggregatorProvider == null) throw new InvalidOperationException("null aggregator provider");
        }

        public MarkedRegionListPattern RegionsPattern
        {
            get
            {
                return regionsPattern;
            }
            protected set
            {
                if (value == null) throw new ArgumentNullException();
                regionsPattern = value;
            }
        }

        public IActiveShapeModelBuilder Normalize(ShapeNormalizer normalizer)
        {
            if (normalizer == null) throw new ArgumentNullException();
            normalizers.AddLast(normalizer);
            return this;
        }

        public IActiveShapeModelBuilder SetFeatureExtractor(ILocalFeatureExtractor<byte> fExtractor)
        {
            FeatureExtractor = fExtractor;
            return this;
        }

        public ILocalFeatureExtractor<byte> FeatureExtractor
        {
            get { return featureExtractor; }
            protected set
            {
                if (value == null) throw new ArgumentNullException();
                featureExtractor = value;
            }
        }

        public IActiveShapeModelBuilder SetNumberOfModes(int numOfModes)
        {
            NumberOfModes = numOfModes;
            return this;
        }

        public int NumberOfModes
        {
            get { return numberOfModes; }
            protected set
            {
                if (value <= 0) throw new ArgumentException("invalid number of modes");
                numberOfModes = value;
            }
        }

        public int NumberOfPoints
        {
            get { return numberOfPoints; }
            protected set
            {
                if (value <= 0) throw new ArgumentException("invalid number of points");
                numberOfPoints = value;
            }
        }

        public IActiveShapeModelBuilder SetTrainingSet(IEnumerable<ImageWithMarkedRegions> ts)
        {
            TrainingSet = ts;
            return this;
        }

        public IActiveShapeModelBuilder SetAggregatorProvider(Func<ILocalFeatureAggregator<ILocalFeature>> provider)
        {
            AggregatorProvider = provider;
            return this;
        }

        public IEnumerable<ImageWithMarkedRegions> TrainingSet
        {
            get { return trainingSet; }
            protected set
            {
                if (value == null) throw new ArgumentNullException();
                trainingSet = value;
            }
        }
    }
}
