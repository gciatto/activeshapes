﻿using BioLab.ActiveShapes.LocalFeature;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes.Alignment;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes.Normalization;
using BioLab.ImageProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.ActiveShapes.Model
{
    public interface IActiveShapeModelBuilder
    {
        IActiveShapeModelBuilder Normalize(ShapeNormalizer normalizer);
        IActiveShapeModelBuilder Align(ShapesAligner aligner);
        IActiveShapeModelBuilder SetTrainingSet(IEnumerable<ImageWithMarkedRegions> ts);
        IActiveShapeModelBuilder SetFeatureExtractor(ILocalFeatureExtractor<byte> fExtractor);
        IActiveShapeModelBuilder SetNumberOfModes(int numOfModes);
        IActiveShapeModelBuilder SetAggregatorProvider(Func<ILocalFeatureAggregator<ILocalFeature>> provider);
        IActiveShapeModel Build();
    }
}
