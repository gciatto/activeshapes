﻿using BioLab.ActiveShapes.LocalFeature;
using BioLab.GCiatto.ImageProcessing;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.ActiveShapes.Model
{
    public interface IActiveShapeModel
    {
        int NumberOfModes { get; }
        Vector Projection(Vector shape);
        Vector BackProjection(Vector modes);
        int NumberOfPoints { get; }
        IList<ILocalFeatureMatcher<ILocalFeature>> FeatureMatchers { get; } 
        MarkedRegionListPattern RegionsPattern { get; }
        double GetModeRange(int index);
        Vector ModesVariation(Vector before, Vector after);
    }
}
