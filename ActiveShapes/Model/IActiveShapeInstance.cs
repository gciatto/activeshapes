﻿using BioLab.ActiveShapes.LocalFeature;
using BioLab.GCiatto.Math.LinearAlgebra;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes;
using BioLab.GCiatto.Utils;
using BioLab.ImageProcessing;
using BioLab.Math.Geometry;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;

namespace BioLab.ActiveShapes.Model
{
    public interface IActiveShapeInstance<T> : IFuzzyMatchable<Image<T>>, IConfidenceMatchable<Image<T>> where T : struct, IEquatable<T>
    {
        IActiveShapeModel Model { get; }
        //Point2D Position { get; set; }
        //double Scale { get; set; }
        //double Rotation { get; set; }
        Affinity2D Pose { get; set; }
        Vector Modes { get; set; }
        void Adapt(ImageWithMarkedRegions image);
        Shape Shape { get; }
        ILocalFeatureExtractor<T> Extractor { get; }
        IList<double> FuzzyMatchings(Image<T> other);
        double FuzzyMatching(Image<T> other, int index);
        IList<double> ConfidenceMatchings(Image<T> other);
        double ConfidenceMatching(Image<T> other, int index);
        double ConfidenceMatchingInPoint(Image<T> other, int index, Vector point);
        Vector ModesVariation(Shape after);
        //void Transform(Affinity2D t);
        //void Reset();
        void AlignTo(Shape shape);
        double Difference(Shape shape);
    }
}
