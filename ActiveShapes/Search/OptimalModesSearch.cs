﻿using System;
using System.Collections.Generic;
using System.Linq;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes;
using BioLab.Math.LinearAlgebra;
using BioLab.GCiatto.Math.LinearAlgebra;
using BioLab.GCiatto.Math.Statistic;
using BioLab.ActiveShapes.Model;
using BioLab.ImageProcessing;

namespace BioLab.ActiveShapes.Search
{
    class OptimalModesSearch<T> : AbstractOptimalModesSearch<T> where T : struct, IEquatable<T>
    {
        private int numberOfSamples = 10;
        private Lazy<IList<Affinity2D>> directions;

        public OptimalModesSearch(int limit, int movementsThreshold, int intermediateResultFreq) 
            : base(limit, movementsThreshold, intermediateResultFreq)
        {
            directions = new Lazy<IList<Affinity2D>>(GenerateRotations);
        }

        public OptimalModesSearch() : base()
        {
            directions = new Lazy<IList<Affinity2D>>(GenerateRotations);
        }

        protected override int ImproveShape(Image<T> image, IActiveShapeInstance<T> instance, out Shape outShape)
        {
            int movements = 0, i = 0;
            LinkedList<Vector> newShape = new LinkedList<Vector>();
            var center = instance.Shape.Points.Mean();

            foreach (var point in instance.Shape.Points)
            {
                var samples = SamplingPoints(point, center, MovementMagnitude)
                    .Select(samplePoint => new { Point = samplePoint, Sample = instance.ConfidenceMatchingInPoint(image, i, samplePoint) })
                    .Where(it => {
                        var x = it.Point[0];
                        var y = it.Point[1];

                        return x > 0 && y > 0 && x < image.Width - 1 && y < image.Height - 1; 
                    }).Select((sample, j) => new { Index = j, PointSample = sample });
                var bestSample = samples.Aggregate((a, b) => a.PointSample.Sample > b.PointSample.Sample ? a : b);

                if (bestSample.Index > 0)
                    movements++;

                newShape.AddLast(bestSample.PointSample.Point);
                i++;
            }

            outShape = newShape.ToShape();

            return movements;
        }

        protected static Vector RadialVersor(Vector point, Vector center)
        {
            return point.Versor(center);
        }

        protected IList<Affinity2D> GenerateRotations()
        {
            return new List<Affinity2D>
                {
                    Affinity2D.Nullity,
                    Affinity2D.Identity,
                    Affinity2D.NewRotation(System.Math.PI / 4),
                    Affinity2D.NewRotation(System.Math.PI / 2),
                    Affinity2D.NewRotation(3 * System.Math.PI / 4),
                    Affinity2D.NewRotation(System.Math.PI),
                    Affinity2D.NewRotation(-3 * System.Math.PI / 4),
                    Affinity2D.NewRotation(-System.Math.PI / 2),
                    Affinity2D.NewRotation(-System.Math.PI / 4)
                };
        }

        protected IList<Affinity2D> Rotations
        {
            get
            {
                return directions.Value;
            }
        }

        public int NumberOfSamples
        {
            get
            {
                return numberOfSamples;
            }
        }

        protected IEnumerable<Vector> SamplingPoints(Vector point, Vector center)
        {
            var dir = RadialVersor(point, center);
            return Rotations.Select(r => r.Apply(dir) + point);
        }

        private static Random rand = new Random();

        protected IEnumerable<Vector> SamplingPoints(Vector point, Vector center, double magnitude)
        {
            var dir = RadialVersor(point, center);
            //var lst =  Rotations
            //    .Select(it => it.Scale(magnitude * rand.NextDouble()))
            //    .Select(r => r.Apply(dir) + point).ToList();
            var lst = new LinkedList<Vector>();
            lst.AddFirst(point);
            foreach (var r in Rotations)
            {
                for (int i = 1; i <= numberOfSamples; i++)
                {
                    var sample = r
                        .Scale(i * MovementMagnitude)
                        .Translate(point)
                        .Apply(dir);

                    lst.AddLast(sample);
                }
            }
            return lst;
        }
    }
}
