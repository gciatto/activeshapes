﻿using BioLab.ActiveShapes.Model;
using BioLab.GCiatto.Common;
using BioLab.GCiatto.Math.LinearAlgebra;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes;
using BioLab.GCiatto.Math.Statistic;
using BioLab.ImageProcessing;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.ActiveShapes.Search
{
    class DummyOptimalModesSearch<T> : AbstractAlgorithm<Tuple<Image<T>, IActiveShapeInstance<T>>, IActiveShapeInstance<T>, IActiveShapeInstance<T>>, IOptimalModesSearch<T> where T : struct, IEquatable<T>
    {
        private int step = 0;
        private int limit = 100;
        private int movementsThreshold = 0;
        private int intermediateResultFreq = 1;
        private double movementMagnitude = 1;

        public DummyOptimalModesSearch(int limit, int movementsThreshold, int intermediateResultFreq) : base()
        {
            this.limit = limit;
            this.movementsThreshold = movementsThreshold;
            this.intermediateResultFreq = intermediateResultFreq;
        }

        public DummyOptimalModesSearch() : base()
        {

        }

        protected override IActiveShapeInstance<T> AlgorithmImplementation(Tuple<Image<T>, IActiveShapeInstance<T>> input)
        {
            var image = input.Item1;
            var asInstance = input.Item2;

            Vector center;
            var continueCond = true;

            for (step = 0; step < limit && continueCond; step++)
            {
                int i = 0;
                int movements = 0;
                LinkedList<Vector> newShape = new LinkedList<Vector>();

                center = asInstance.Shape.Points.Mean();

                foreach (var point in asInstance.Shape.Points)
                {
                    var samples = SamplingPoints(point, center, movementMagnitude)
                        .Select(samplePoint => new { Point = samplePoint, Sample = asInstance.ConfidenceMatching(image, i) })
                        .Select((sample, j) => new { Index = j, PointSample = sample });
                    var bestSample = samples.Aggregate((a, b) => a.PointSample.Sample > b.PointSample.Sample ? a : b);

                    if (bestSample.Index > 0)
                        movements++;

                    newShape.AddLast(bestSample.PointSample.Point);

                    i++;
                }

                if (movements <= movementsThreshold)
                    continueCond = false;

                var modesVariation = asInstance.ModesVariation(newShape.ToShape());
                asInstance.Modes += modesVariation;

                if (continueCond)
                {
                    RaiseIntermediateResultEvent(asInstance, step);
                    RaiseProgressChangedEvent((double)(step + 1) / limit);
                }
            }

            return asInstance;
        }

        protected static Vector RadialVersor(Vector point, Vector center)
        {
            return point.Versor(center);
        }

        protected static IList<Affinity2D> rotations = new List<Affinity2D>
        {
            Affinity2D.Nullity,
            Affinity2D.Identity,
            //Affine2DTransformation.NewRotation(System.Math.PI / 4),
            Affinity2D.NewRotation(System.Math.PI / 2),
            //Affine2DTransformation.NewRotation(3 * System.Math.PI / 4),
            Affinity2D.NewRotation(System.Math.PI),
            //Affine2DTransformation.NewRotation(-3 * System.Math.PI / 4),
            Affinity2D.NewRotation(-System.Math.PI / 2),
            //Affine2DTransformation.NewRotation(-System.Math.PI / 4)
        };

        protected static IEnumerable<Vector> SamplingPoints (Vector point, Vector center)
        {
            var dir = RadialVersor(point, center);
            return rotations.Select(r => r.Apply(dir) + point);
        }

        protected static IEnumerable<Vector> SamplingPoints(Vector point, Vector center, double magnitude)
        {
            var dir = RadialVersor(point, center);
            return rotations
                .Select(it => it.Scale(magnitude))
                .Select(r => r.Apply(dir) + point);
        }
    }
}
