﻿using BioLab.ActiveShapes.Model;
using BioLab.GCiatto.Common;
using BioLab.ImageProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.ActiveShapes.Search
{
    public interface IOptimalModesSearch<T> : IAlgorithm<Tuple<Image<T>, IActiveShapeInstance<T>>, IActiveShapeInstance<T>, IActiveShapeInstance<T>> where T : struct, IEquatable<T>
    {
    }
}
