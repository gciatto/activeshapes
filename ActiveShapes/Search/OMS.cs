﻿using BioLab.ActiveShapes.Model;
using BioLab.ImageProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.ActiveShapes.Search
{
    public static class OMS
    {
        public static IOptimalModesSearch<T> DummyOptimalModesSearch<T>(this IActiveShapeInstance<T> asi, Image<T> image) where T : struct, IEquatable<T>
        {
            var osm = new DummyOptimalModesSearch<T>();
            osm.Input = Tuple.Create(image, asi);
            return osm;
        }

        public static IOptimalModesSearch<T> DummyOptimalModesSearch<T>(this IActiveShapeInstance<T> asi, Image<T> image, int stepLimit, int movementsThr, int intermediateFreq) where T : struct, IEquatable<T>
        {
            var osm = new DummyOptimalModesSearch<T>(stepLimit, movementsThr, intermediateFreq);
            osm.Input = Tuple.Create(image, asi);
            return osm;
        }

        public static IOptimalModesSearch<T> OptimalModesSearch<T>(this IActiveShapeInstance<T> asi, Image<T> image) where T : struct, IEquatable<T>
        {
            var osm = new OptimalModesSearch<T>();
            osm.Input = Tuple.Create(image, asi);
            return osm;
        }

        public static IOptimalModesSearch<T> OptimalModesSearch<T>(this IActiveShapeInstance<T> asi, Image<T> image, int stepLimit, int movementsThr, int intermediateFreq) where T : struct, IEquatable<T>
        {
            var osm = new OptimalModesSearch<T>(stepLimit, movementsThr, intermediateFreq);
            osm.Input = Tuple.Create(image, asi);
            return osm;
        }
    }
}
