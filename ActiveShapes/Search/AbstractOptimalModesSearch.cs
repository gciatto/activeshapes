﻿using BioLab.ActiveShapes.Model;
using BioLab.GCiatto.Common;
using BioLab.GCiatto.Math.LinearAlgebra;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes.Alignment;
using BioLab.GCiatto.Math.Statistic;
using BioLab.ImageProcessing;
using BioLab.Math.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.ActiveShapes.Search
{
    abstract class AbstractOptimalModesSearch<T> : AbstractAlgorithm<Tuple<Image<T>, IActiveShapeInstance<T>>, IActiveShapeInstance<T>, IActiveShapeInstance<T>>, IOptimalModesSearch<T> where T : struct, IEquatable<T>
    {
        private int step = 0;
        private int limit = 100;
        private int movementsThreshold = 0;
        private int intermediateResultFreq = 1;
        private double movementMagnitude = 4;
        private bool continueCond = true;
        private int movements = 0;
        private IShapeCoupleAligner aligner;

        public AbstractOptimalModesSearch(int limit, int movementsThreshold, int intermediateResultFreq) : base()
        {
            this.limit = limit;
            this.movementsThreshold = movementsThreshold;
            this.intermediateResultFreq = intermediateResultFreq;
        }

        public AbstractOptimalModesSearch() : base()
        {

        }

        public double MovementMagnitude
        {
            get { return movementMagnitude; }
            set
            {
                movementMagnitude = value;
            }
        }

        protected abstract int ImproveShape(Image<T> image, IActiveShapeInstance<T> instance, out Shape outShape);

        protected override IActiveShapeInstance<T> AlgorithmImplementation(Tuple<Image<T>, IActiveShapeInstance<T>> input)
        {
            var image = input.Item1;
            var asInstance = input.Item2;

            continueCond = true;

            for (step = 0; step < limit && continueCond; step++)
            {
                Shape newShape;
                movements = ImproveShape(image, asInstance, out newShape);

                if (movements <= movementsThreshold)
                    continueCond = false;

                AdjustInstancePose(asInstance, newShape);
                AdjustInstanceModes(asInstance, newShape);

                if (continueCond)
                {
                    if (step % intermediateResultFreq == 0)
                    {
                        RaiseIntermediateResultEvent(asInstance, step);
                        RaiseProgressChangedEvent((double)(step + 1) / limit);
                    }
                }
                else
                {
                    RaiseIntermediateResultEvent(asInstance, step);
                    RaiseProgressChangedEvent(1);
                }
            }

            return asInstance;
        }

        protected virtual void AdjustInstancePose(IActiveShapeInstance<T> instance, Shape newShape)
        {
            aligner = CoupleAligners.Aligner(newShape);
            var affinity = aligner.Affinity(instance.Shape);
            //instance.Scale *= affinity.ScaleX;
            //instance.Rotation += affinity.Angle;
            //instance.Position = new Math.Geometry.Point2D(
            //        instance.Position.X + affinity.DeltaX,
            //        instance.Position.Y + affinity.DeltaY
            //    );
            instance.Pose = instance.Pose.Combine(affinity);
        }

        protected virtual void AdjustInstanceModes(IActiveShapeInstance<T> instance, Shape newShape)
        {
            var modeVariation = instance.ModesVariation(newShape);
            instance.Modes += modeVariation;
        }
    }
}
