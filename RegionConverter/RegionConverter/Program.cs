﻿using BioLab.GCiatto.Mkrtxt;

namespace RegionConverter
{
    static class Program
    {
        static void Main(string[] args)
        {
            MkrtxtPathSplitter.Run(args);
        }
    }
}
