﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BioLab.GCiatto.Regexp
{
    public class RegexBuilder
    {
        private static readonly string U_INT = "(\\d*)";
        private static readonly string U_FLOAT = "(\\d*[.]\\d+)";
        private static readonly string WS = "\\s";

        private StringBuilder sb = new StringBuilder();

        public RegexBuilder UInt
        {
            get
            {
                sb.Append(U_INT);
                return this;
            }
        }

        public RegexBuilder UFloat
        {
            get
            {
                sb.Append(U_FLOAT);
                return this;
            }
        }

        public RegexBuilder Ws
        {
            get
            {
                sb.Append(WS);
                return this;
            }
        }

        public RegexBuilder WsPlus
        {
            get
            {
                return Plus(WS);
            }
        }

        public RegexBuilder WsStar
        {
            get
            {
                return Star(WS);
            }
        }

        public RegexBuilder Star(string what)
        {
            sb.Append(what).Append("*");
            return this;
        }

        public RegexBuilder Plus(string what)
        {
            sb.Append(what).Append("+");
            return this;
        }

        public RegexBuilder Opt(string what)
        {
            sb.Append(what).Append("?");
            return this;
        }

        public Regex Regex
        {
            get
            {
                return new Regex(sb.ToString());
            }
        }

    }
}
