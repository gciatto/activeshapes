﻿using BioLab.GCiatto.Files;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace BioLab.GCiatto.Mkrtxt
{
    class MkrtxtPathSplitter
    {
        static readonly string EXT = ".mkrtxt";
        static readonly string FOLDER = "Z:\\SharedMemory\\imm_face_db\\";

        static readonly ISet<string> IMG_EXTS = new HashSet<string>
        {
            ".jpg",
            ".png"
        };
        public static void Run(string[] args)
        {
            if (Directory.Exists(FOLDER))
            {
                var rFiles = Directory.EnumerateFiles(FOLDER)
                    .Where(it => Path.GetExtension(it) == EXT);

                foreach (var f in rFiles)
                {
                    using (var w = File.OpenText(f))
                    {
                        SplitPaths(f, w);
                    }
                }
            }



        }

        static void SplitPaths(string path, StreamReader file)
        {
            var lines = file.Lines();
            var paths = lines.Skip(1).Select((p, i) => new { Path = p, Index = i });

            foreach (var p in paths) {
                string dir;
                var newPath = PathFileName(path, p.Index, out dir);
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
                GeneratePathFile(newPath, p.Path);
                CopyImages(path, p.Index);
            }
        }

        static string PathFileName(string path, int index)
        {
            string folder = null;
            return PathFileName(path, index, out folder);
        }

        static string PathFileName(string path, int index, out string folder)
        {
            var ext = Path.GetExtension(path);
            var fname = Path.GetFileName(path);
            var newFname = Path.ChangeExtension(fname, ".path_" + index + ext);
            folder = Path.GetDirectoryName(path) + Path.DirectorySeparatorChar + "path" + index + Path.DirectorySeparatorChar;
            var newPath = folder + newFname;
            return newPath;
        }

        static void CopyImages(string path, int index)
        {
            foreach (var e in IMG_EXTS)
            {
                var imgPath = Path.ChangeExtension(path, e);
                if (File.Exists(imgPath))
                {
                    var newImgPath = PathFileName(imgPath, index);
                    File.Copy(imgPath, newImgPath, true);
                }
            }
        }

        static void GeneratePathFile(string path, string secondLine)
        {
            var w = File.CreateText(path);

            using (w)
            {
                Console.Write(path);
                Console.WriteLine(" **************");
                w.WriteLine(1);
                Console.WriteLine(1);
                w.WriteLine(secondLine);
                Console.WriteLine(secondLine);
                Console.WriteLine();
            }
        }
    }
}
