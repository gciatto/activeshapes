﻿using BioLab.GCiatto.Files;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.GCiatto.IMMDb
{
    public class RecordsFile
    {
        private Lazy<int> pathCount;
        private Lazy<IList<Lazy<IList<Record>>>> paths;
        private Lazy<IList<Lazy<int>>> pathSizes;
        private Lazy<IList<Lazy<bool?>>> pathClosed;

        public RecordsFile(int nop, string ifn, IEnumerable<Record> rs)
        {
            if (rs.Count() != nop)
                throw new ArgumentException();
            NumberOfPoints = nop;
            ImageFileName = ifn;
            Records = rs.ToList();
            pathCount = new Lazy<int>(CountPaths);
            paths = new Lazy<IList<Lazy<IList<Record>>>>(GetPaths);
            pathSizes = new Lazy<IList<Lazy<int>>>(GetPathSizes);
            pathClosed = new Lazy<IList<Lazy<bool?>>>(GetPathClosed);
        }

        public static RecordsFile Parse(StreamReader sr)
        {
            int nop;
            string ifn;
            IList<Record> rs;
            var lines = sr.Lines().Select(it => it.Trim())
                .Where(it => !string.IsNullOrEmpty(it))
                .Where(it => it[0] != '#');

            var e = lines.GetEnumerator();
            e.MoveNext();
            nop = int.Parse(e.Current);
            rs = new List<Record>(nop);

            e.MoveNext();
            for (int i = 0; i < nop; i++)
            {
                rs.Add(Record.Parse(e.Current));
                e.MoveNext();
            }

            ifn = e.Current;

            return new RecordsFile(nop, ifn, rs);
        }

        public Tuple<int, bool?, IList<Record>> PathData(int i)
        {
            return Tuple.Create(PathSize(i), IsClosed(i), Path(i));
        }
        public IList<Record> Path(int pathNum)
        {
            return paths.Value[pathNum].Value;
        }

        public int PathSize(int pathNum)
        {
            return pathSizes.Value[pathNum].Value;
        }

        public bool? IsClosed(int pathNum)
        {
            return pathClosed.Value[pathNum].Value;
        }

        public string ImageFileName
        {
            get; set;
        }
        public int NumberOfPoints
        {
            get; set;
        }

        public IList<Record> Records
        {
            get; set;
        }

        public int NumberOfPaths
        {
            get
            {
                return pathCount.Value;
            }
        }

        protected int CountPaths()
        {
            return Records.Select(r => r.PathNumber).Max();
        }

        protected IEnumerable<Record> GetPath(int index)
        {
            var fstOfPath = Records.Where(r => r.PathNumber == index)
                .Aggregate((a, b) => a.PointNumber < b.PointNumber ? a : b);
            return GetPathFromHead(fstOfPath);
        }

        protected Record GetNextRecord(Record rec, Record first, int i)
        {
            if (rec.IsLast || rec.To == first.PointNumber && i > 0)
                return null;
            else
                return Records.Where(r => r.PointNumber == rec.To).First();
        }

        protected IEnumerable<Record> GetPathFromHead(Record r)
        {
            Record curr = r;
            for (int i = 0; curr != null; curr = GetNextRecord(curr, r, i++))
            {
                yield return curr;
            }
        }

        protected IList<Lazy<IList<Record>>> GetPaths()
        {
            var lst = new List<Lazy<IList<Record>>>(NumberOfPaths);
            for (int i = 0; i < NumberOfPaths; i++)
            {
                var j = i;
                lst.Add(new Lazy<IList<Record>>(() => GetPath(j).ToList()));
            }
            return lst;
        }

        protected IList<Lazy<int>> GetPathSizes()
        {
            var lst = new List<Lazy<int>>(NumberOfPaths);
            for (int i = 0; i < NumberOfPaths; i++)
            {
                var j = i;
                lst.Add(new Lazy<int>(
                    () => Records.Where(r => r.PathNumber == j).Count()
                ));
            }
            return lst;
        }

        protected IList<Lazy<bool?>> GetPathClosed()
        {
            var lst = new List<Lazy<bool?>>(NumberOfPaths);
            for (int i = 0; i < NumberOfPaths; i++)
            {
                var j = i;
                lst.Add(new Lazy<bool?>(
                    () =>
                    {
                        var pathj = Path(j);
                        if (pathj.First().IsFirst && pathj.Last().IsLast)
                            return false;
                        else if (pathj.First().PointNumber == pathj.Last().To)
                            return true;
                        else
                            return null;
                    }
                ));
            }
            return lst;
        }

        public override string ToString()
        {
            return string.Join("\n", PathDatas().Select(
                it => Tuple.Create(it.Item1, it.Item2, string.Join(" -> ", it.Item3))
            ));
        }

        public IEnumerable<Tuple<int, bool?, IList<Record>>> PathDatas()
        {
            for (int i = 0; i < NumberOfPaths; i++)
                yield return PathData(i);
        }
    }
}
