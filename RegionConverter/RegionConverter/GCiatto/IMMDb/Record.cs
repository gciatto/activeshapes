﻿using BioLab.GCiatto.Regexp;
using System;
using System.Text.RegularExpressions;
using System.Globalization;

namespace BioLab.GCiatto.IMMDb
{
    public class Record
    {
        private static readonly Regex parser = new RegexBuilder()
            .UInt.WsPlus
            .UInt.WsPlus
            .UFloat.WsPlus
            .UFloat.WsPlus
            .UInt.WsPlus
            .UInt.WsPlus
            .UInt.WsStar
            .Regex;
        public Record(int pathNumber, int type, double x, double y, int pointNumber, int from, int to)
        {
            PathNumber = pathNumber;
            Type = type;
            X = x;
            Y = y;
            PointNumber = pointNumber;
            From = from;
            To = to;
        }

        public static Record Parse(string str)
        {
            var m = parser.Match(str);
            var gc = m.Groups;

            int pathNumber, type, pointNumber, from, to;
            double x, y;

            const int i = 1;

            if (!int.TryParse(gc[i].Value, out pathNumber))
                throw new ArgumentException("invalid path num");

            if (!int.TryParse(gc[i + 1].Value, out type))
                throw new ArgumentException("invalid type");

            x = double.Parse(gc[i + 2].Value, NumberStyles.AllowDecimalPoint, NumberFormatInfo.InvariantInfo);
            y = double.Parse(gc[i + 3].Value, NumberStyles.AllowDecimalPoint, NumberFormatInfo.InvariantInfo);

            if (!int.TryParse(gc[i + 4].Value, out pointNumber))
                throw new ArgumentException("invalid point num");

            if (!int.TryParse(gc[i + 5].Value, out from))
                throw new ArgumentException("invalid from");

            if (!int.TryParse(gc[i + 6].Value, out to))
                throw new ArgumentException("invalid to");

            return new Record(pathNumber, type, x, y, pointNumber, from, to);
        }

        public int PathNumber { get; set; }
        public int Type { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public int PointNumber { get; set; }
        public int From { get; set; }
        public int To { get; set; }
        public bool IsFirst
        {
            get
            {
                return From == PointNumber;
            }
        }

        public bool IsLast
        {
            get
            {
                return To == PointNumber;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}#{1:0.##};{2:0.##}", PointNumber, X, Y);
        }
    }
}
