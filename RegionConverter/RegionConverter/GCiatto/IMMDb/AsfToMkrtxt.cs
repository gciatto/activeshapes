﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BioLab.GCiatto.IMMDb;
using System.IO;
using BioLab.ImageProcessing;

namespace BioLab.GCiatto.IMMDb
{
    static class AsfToMkrtxt
    {
        static readonly string EXT = ".asf";
        static readonly string DST_EXT = ".mkrtxt";
        static readonly string FOLDER = "Z:\\SharedMemory\\imm_face_db\\";
        static void Run(string[] args)
        {
            if (Directory.Exists(FOLDER))
            {
                var rFiles = Directory.EnumerateFiles(FOLDER)
                    .Where(it => Path.GetExtension(it) == EXT)
                    .Select(it => File.OpenText(it))
                    .Select(it =>
                    {
                        using (it)
                            return RecordsFile.Parse(it);
                    });

                foreach (var rf in rFiles)
                {
                    int width, height;
                    var imgPath = FOLDER + rf.ImageFileName;
                    if (File.Exists(imgPath))
                    {
                        var img = ImageBase.LoadFromFile(imgPath);
                        width = img.Width;
                        height = img.Height;
                        img = null;

                        using (var fw = File.CreateText(Path.ChangeExtension(imgPath, DST_EXT))) 
                            rf.Serialize(fw, width, height);

                        Console.WriteLine(rf);
                        Console.WriteLine();
                    }
                    else
                        throw new InvalidOperationException();
                }
            }

            
        }
    }
}
