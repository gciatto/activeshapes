﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BioLab.GCiatto.IMMDb
{
    public static class IMMDbUtils
    {
        public static void Serialize(this Record r, StreamWriter sw, int width, int height)
        {
            double dX = r.X * width;
            double dY = r.Y * height;
            sw.Write((int)dX);
            sw.Write(" ");
            sw.Write((int)dY);
        }

        public static void Serialize(this IEnumerable<Record> rs, StreamWriter sw, int width, int height)
        {
            rs.First().Serialize(sw, width, height);
            foreach (var r in rs.Skip(1))
            {
                sw.Write(" ");
                r.Serialize(sw, width, height);
            }
        }

        public static void Serialize(this Tuple<int, bool?, IList<Record>> rts, StreamWriter sw, int width, int height)
        {
            sw.Write(rts.Item1);
            sw.Write(" ");
            sw.Write(rts.Item2.GetValueOrDefault());
            sw.Write(" ");
            rts.Item3.Serialize(sw, width, height);
            sw.WriteLine();
        }

        public static void Serialize(this RecordsFile rf, StreamWriter sw, int width, int height)
        {
            sw.Write(rf.NumberOfPaths);
            sw.WriteLine();
            foreach (var pd in rf.PathDatas())
            {
                pd.Serialize(sw, width, height);
            }

        }
    }
}
