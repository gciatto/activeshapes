﻿using BioLab.Common;
using BioLab.GCiatto.Utils;
using BioLab.GUI.DataViewers;
using BioLab.Math.Geometry;
using System;
using System.IO;
using System.Text;

namespace BioLab.ImageProcessing
{
    /// <summary>
    /// Un'immagine con una serie di regioni poligonali
    /// </summary>
    [DefaultDataViewer(typeof(ImageWithMarkedRegionsViewer))]
    public class ImageWithMarkedRegions : RgbImage<byte>
    {
        /// <summary>
        /// Regioni nell'immagine
        /// </summary>
        public MarkedRegionList Regions = new MarkedRegionList();
        private ICache<Image<byte>> byteImageCache;

        public ImageWithMarkedRegions(ImageBase img)
          : base(img.ToByteRgbImage())
        {
            byteImageCache = Caches.Lazy(() => img.ToByteImage());
        }

        public Image<byte> AsByteImage()
        {
            return byteImageCache.Value;
        }

        /// <summary>
        /// Calcola il numero totale di punti nelle varie regioni
        /// </summary>
        public int CountTotalPoints()
        {
            int n = 0;
            foreach (var r in Regions)
                n += r.Points.Count;
            return n;
        }

        /// <summary>
        /// Carica le regioni da file
        /// </summary>
        public void LoadRegionsFromTextFile(string pathname)
        {
            var lines = File.ReadAllLines(pathname);
            Regions = new MarkedRegionList();
            var count = int.Parse(lines[0]);
            for (int i = 1; i <= count; i++)
            {
                var region = new MarkedRegion();

                var splittedLine = lines[i].Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                var pointCount = int.Parse(splittedLine[0]);
                region.Closed = bool.Parse(splittedLine[1]);
                var k = 2;
                for (int j = 0; j < pointCount; j++)
                {
                    var x = int.Parse(splittedLine[k++]);
                    var y = int.Parse(splittedLine[k++]);
                    region.Points.Add(new IntPoint2D(x, y));
                }

                Regions.Add(region);
            }
        }

        /// <summary>
        /// Salva le regioni su file
        /// </summary>
        public void SaveRegionsToTextFile(string filePath)
        {
            var strBuilder = new StringBuilder();
            strBuilder.AppendLine(Regions.Count.ToString());
            foreach (var r in Regions)
            {
                strBuilder.AppendFormat("{0} {1}", r.Points.Count, r.Closed);
                foreach (var p in r.Points)
                {
                    strBuilder.AppendFormat(" {0} {1}", p.X, p.Y);
                }
                strBuilder.AppendLine();
            }

            File.WriteAllText(filePath, strBuilder.ToString());
        }

        public override void SaveToStream(Stream stream)
        {
            throw new NotImplementedException();
        }

        protected override Data InternalClone()
        {
            throw new NotImplementedException();
        }
    }
}
