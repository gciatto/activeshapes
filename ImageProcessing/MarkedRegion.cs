﻿using BioLab.Math.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BioLab.ImageProcessing
{
    /// <summary>
    /// Una regione in un'immagine (in coordinate pixel dell'immagine)
    /// </summary>
    public class MarkedRegion : ICloneable
    {
        public List<IntPoint2D> Points = new List<IntPoint2D>();
        public bool Closed = false;

        public bool IsInside(IntPoint2D p)
        {
            return IsInside(p.X, p.Y);
        }

        public bool IsInside(int x, int y)
        {
            if (!Closed)
                return false;
            bool c = false;
            for (int i = 0, j = Points.Count - 1; i < Points.Count; j = i++)
            {
                if ((((Points[i].Y <= y) && (y < Points[j].Y)) ||
                     ((Points[j].Y <= y) && (y < Points[i].Y))) &&
                    (x < (Points[j].X - Points[i].X) * (y - Points[i].Y) / (Points[j].Y - Points[i].Y) + Points[i].X))
                    c = !c;
            }
            return c;
        }

        #region ICloneable Members

        public object Clone()
        {
            MarkedRegion r = new MarkedRegion();
            r.Points = new List<IntPoint2D>(Points);
            r.Closed = Closed;
            return r;
        }

        #endregion
    }
}
