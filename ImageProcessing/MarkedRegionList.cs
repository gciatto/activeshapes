﻿using System.Collections;
using System.Collections.Generic;

namespace BioLab.ImageProcessing
{
    public class MarkedRegionList : IEnumerable<MarkedRegion>
    {
        #region Membri privati
        List<MarkedRegion> regions = new List<MarkedRegion>();
        #endregion

        public void Add(MarkedRegion markedRegion)
        {
            regions.Add(markedRegion);
        }

        public bool Remove(MarkedRegion r)
        {
            return regions.Remove(r);
        }

        public int Count { get { return regions.Count; } }


        public MarkedRegion this[int index]
        {
            get { return regions[index]; }
            set { regions[index] = value; }
        }


        #region IEnumerable<MarkedRegion> Members

        public IEnumerator<MarkedRegion> GetEnumerator()
        {
            return regions.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return regions.GetEnumerator();
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            var res = new MarkedRegionList();
            foreach (var r in regions)
            {
                res.Add((MarkedRegion)r.Clone());
            }

            return res;
        }

        #endregion
    }
}
