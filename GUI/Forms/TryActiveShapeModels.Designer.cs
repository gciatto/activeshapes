using BioLab.GUI.DataViewers;

namespace BioLab.GUI.Forms
{
    partial class TryActiveShapeModels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
            System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TryActiveShapeModels));
            this.buttonLoadTrainingData = new System.Windows.Forms.Button();
            this.groupBoxModes = new System.Windows.Forms.GroupBox();
            this.tableLayoutModes = new System.Windows.Forms.TableLayoutPanel();
            this.buttonResetModes = new System.Windows.Forms.Button();
            this.numericUpDownPreviewTrainImg = new System.Windows.Forms.NumericUpDown();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonMatch = new System.Windows.Forms.Button();
            this.buttonOMS = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPose = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.trackBarX = new System.Windows.Forms.TrackBar();
            this.trackBarY = new System.Windows.Forms.TrackBar();
            this.trackBarScale = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.trackBarRotation = new System.Windows.Forms.TrackBar();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.numericUpDownModeCount = new System.Windows.Forms.NumericUpDown();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.labelMatchingStatistics = new System.Windows.Forms.ToolStripStatusLabel();
            this.labelMatchingDetails = new System.Windows.Forms.ToolStripStatusLabel();
            this.imageWithMarkedRegionsViewerCurrentShape = new BioLab.GUI.DataViewers.ImageWithMarkedRegionsViewer();
            this.imageWithMarkedRegionsViewerTraining = new BioLab.GUI.DataViewers.ImageWithMarkedRegionsViewer();
            this.labelTestDifference = new System.Windows.Forms.ToolStripStatusLabel();
            toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBoxModes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPreviewTrainImg)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarRotation)).BeginInit();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownModeCount)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripStatusLabel1
            // 
            toolStripStatusLabel1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            toolStripStatusLabel1.Size = new System.Drawing.Size(10, 17);
            toolStripStatusLabel1.Text = "|";
            // 
            // buttonLoadTrainingData
            // 
            this.buttonLoadTrainingData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonLoadTrainingData.Location = new System.Drawing.Point(3, 3);
            this.buttonLoadTrainingData.Name = "buttonLoadTrainingData";
            this.buttonLoadTrainingData.Size = new System.Drawing.Size(177, 20);
            this.buttonLoadTrainingData.TabIndex = 2;
            this.buttonLoadTrainingData.Text = "Carica immagini...";
            this.buttonLoadTrainingData.UseVisualStyleBackColor = true;
            this.buttonLoadTrainingData.Click += new System.EventHandler(this.buttonLoadTrainingData_Click);
            // 
            // groupBoxModes
            // 
            this.groupBoxModes.Controls.Add(this.tableLayoutModes);
            this.groupBoxModes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxModes.Location = new System.Drawing.Point(3, 153);
            this.groupBoxModes.Name = "groupBoxModes";
            this.groupBoxModes.Size = new System.Drawing.Size(188, 445);
            this.groupBoxModes.TabIndex = 3;
            this.groupBoxModes.TabStop = false;
            this.groupBoxModes.Text = "Modi di variazione";
            // 
            // tableLayoutModes
            // 
            this.tableLayoutModes.ColumnCount = 1;
            this.tableLayoutModes.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutModes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutModes.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutModes.Name = "tableLayoutModes";
            this.tableLayoutModes.RowCount = 15;
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutModes.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutModes.Size = new System.Drawing.Size(182, 426);
            this.tableLayoutModes.TabIndex = 0;
            // 
            // buttonResetModes
            // 
            this.buttonResetModes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonResetModes.Location = new System.Drawing.Point(3, 3);
            this.buttonResetModes.Name = "buttonResetModes";
            this.buttonResetModes.Size = new System.Drawing.Size(112, 20);
            this.buttonResetModes.TabIndex = 1;
            this.buttonResetModes.Text = "Reset";
            this.buttonResetModes.UseVisualStyleBackColor = true;
            this.buttonResetModes.Click += new System.EventHandler(this.buttonResetModes_Click);
            // 
            // numericUpDownPreviewTrainImg
            // 
            this.numericUpDownPreviewTrainImg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numericUpDownPreviewTrainImg.Location = new System.Drawing.Point(186, 3);
            this.numericUpDownPreviewTrainImg.Name = "numericUpDownPreviewTrainImg";
            this.numericUpDownPreviewTrainImg.Size = new System.Drawing.Size(178, 20);
            this.numericUpDownPreviewTrainImg.TabIndex = 4;
            this.numericUpDownPreviewTrainImg.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownPreviewTrainImg.ValueChanged += new System.EventHandler(this.numericUpDownPreviewTrainImg_ValueChanged);
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonCalculate.Location = new System.Drawing.Point(3, 3);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(112, 20);
            this.buttonCalculate.TabIndex = 2;
            this.buttonCalculate.Text = "Crea modello";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.Controls.Add(this.imageWithMarkedRegionsViewerCurrentShape, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.imageWithMarkedRegionsViewerTraining, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(946, 639);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 610);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(367, 26);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.buttonLoadTrainingData, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.numericUpDownPreviewTrainImg, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(367, 26);
            this.tableLayoutPanel4.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(376, 610);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(367, 26);
            this.panel2.TabIndex = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 3;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.Controls.Add(this.buttonCalculate, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.buttonMatch, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.buttonOMS, 2, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(367, 26);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // buttonMatch
            // 
            this.buttonMatch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonMatch.Location = new System.Drawing.Point(121, 3);
            this.buttonMatch.Name = "buttonMatch";
            this.buttonMatch.Size = new System.Drawing.Size(141, 20);
            this.buttonMatch.TabIndex = 3;
            this.buttonMatch.Text = "Matching Immagine...";
            this.buttonMatch.UseVisualStyleBackColor = true;
            this.buttonMatch.Click += new System.EventHandler(this.buttonMatch_Click);
            // 
            // buttonOMS
            // 
            this.buttonOMS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonOMS.Location = new System.Drawing.Point(268, 3);
            this.buttonOMS.Name = "buttonOMS";
            this.buttonOMS.Size = new System.Drawing.Size(96, 20);
            this.buttonOMS.TabIndex = 4;
            this.buttonOMS.Text = "O.M.S.";
            this.buttonOMS.UseVisualStyleBackColor = true;
            this.buttonOMS.Click += new System.EventHandler(this.buttonOMS_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tableLayoutPanel2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(749, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(194, 601);
            this.panel3.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.groupBoxModes, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(194, 601);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPose);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(188, 144);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Posa";
            // 
            // tableLayoutPose
            // 
            this.tableLayoutPose.ColumnCount = 2;
            this.tableLayoutPose.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPose.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPose.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPose.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPose.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPose.Controls.Add(this.trackBarX, 1, 0);
            this.tableLayoutPose.Controls.Add(this.trackBarY, 1, 1);
            this.tableLayoutPose.Controls.Add(this.trackBarScale, 1, 2);
            this.tableLayoutPose.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPose.Controls.Add(this.trackBarRotation, 1, 3);
            this.tableLayoutPose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPose.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPose.Name = "tableLayoutPose";
            this.tableLayoutPose.RowCount = 4;
            this.tableLayoutPose.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPose.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPose.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPose.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPose.Size = new System.Drawing.Size(182, 125);
            this.tableLayoutPose.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 32);
            this.label3.TabIndex = 7;
            this.label3.Text = "Rotation:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 31);
            this.label4.TabIndex = 6;
            this.label4.Text = "Scale:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 31);
            this.label2.TabIndex = 4;
            this.label2.Text = "Y:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // trackBarX
            // 
            this.trackBarX.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackBarX.Location = new System.Drawing.Point(59, 3);
            this.trackBarX.Name = "trackBarX";
            this.trackBarX.Size = new System.Drawing.Size(120, 25);
            this.trackBarX.TabIndex = 0;
            this.trackBarX.TickFrequency = 0;
            this.trackBarX.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarX.ValueChanged += new System.EventHandler(this.TrackBar_ValueChanged);
            // 
            // trackBarY
            // 
            this.trackBarY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackBarY.Location = new System.Drawing.Point(59, 34);
            this.trackBarY.Name = "trackBarY";
            this.trackBarY.Size = new System.Drawing.Size(120, 25);
            this.trackBarY.TabIndex = 1;
            this.trackBarY.TickFrequency = 0;
            this.trackBarY.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarY.ValueChanged += new System.EventHandler(this.TrackBar_ValueChanged);
            // 
            // trackBarScale
            // 
            this.trackBarScale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackBarScale.Location = new System.Drawing.Point(59, 65);
            this.trackBarScale.Maximum = 1000;
            this.trackBarScale.Minimum = -1000;
            this.trackBarScale.Name = "trackBarScale";
            this.trackBarScale.Size = new System.Drawing.Size(120, 25);
            this.trackBarScale.TabIndex = 2;
            this.trackBarScale.TickFrequency = 0;
            this.trackBarScale.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarScale.Value = 1;
            this.trackBarScale.ValueChanged += new System.EventHandler(this.TrackBar_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 31);
            this.label1.TabIndex = 3;
            this.label1.Text = "X:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // trackBarRotation
            // 
            this.trackBarRotation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackBarRotation.Location = new System.Drawing.Point(59, 96);
            this.trackBarRotation.Maximum = 18000;
            this.trackBarRotation.Minimum = -18000;
            this.trackBarRotation.Name = "trackBarRotation";
            this.trackBarRotation.Size = new System.Drawing.Size(120, 26);
            this.trackBarRotation.SmallChange = 10;
            this.trackBarRotation.TabIndex = 8;
            this.trackBarRotation.TickFrequency = 0;
            this.trackBarRotation.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBarRotation.ValueChanged += new System.EventHandler(this.TrackBar_ValueChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tableLayoutPanel6);
            this.panel4.Location = new System.Drawing.Point(749, 610);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(194, 26);
            this.panel4.TabIndex = 3;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.Controls.Add(this.numericUpDownModeCount, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.buttonResetModes, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(194, 26);
            this.tableLayoutPanel6.TabIndex = 3;
            // 
            // numericUpDownModeCount
            // 
            this.numericUpDownModeCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numericUpDownModeCount.Location = new System.Drawing.Point(121, 3);
            this.numericUpDownModeCount.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDownModeCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownModeCount.Name = "numericUpDownModeCount";
            this.numericUpDownModeCount.Size = new System.Drawing.Size(70, 20);
            this.numericUpDownModeCount.TabIndex = 2;
            this.numericUpDownModeCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownModeCount.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownModeCount.ValueChanged += new System.EventHandler(this.numericUpDownModeCount_ValueChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.progressBar,
            this.labelTestDifference,
            toolStripStatusLabel2,
            this.labelMatchingStatistics,
            toolStripStatusLabel1,
            this.labelMatchingDetails});
            this.statusStrip1.Location = new System.Drawing.Point(0, 639);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(946, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // progressBar
            // 
            this.progressBar.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // labelMatchingStatistics
            // 
            this.labelMatchingStatistics.Name = "labelMatchingStatistics";
            this.labelMatchingStatistics.Size = new System.Drawing.Size(42, 17);
            this.labelMatchingStatistics.Text = "Ready.";
            // 
            // labelMatchingDetails
            // 
            this.labelMatchingDetails.Name = "labelMatchingDetails";
            this.labelMatchingDetails.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.labelMatchingDetails.Size = new System.Drawing.Size(39, 17);
            this.labelMatchingDetails.Text = "None.";
            // 
            // imageWithMarkedRegionsViewerCurrentShape
            // 
            this.imageWithMarkedRegionsViewerCurrentShape.BackColor = System.Drawing.SystemColors.Control;
            this.imageWithMarkedRegionsViewerCurrentShape.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageWithMarkedRegionsViewerCurrentShape.LeftMouseButtonTool = BioLab.GUI.DataViewers.MouseButtonTool.None;
            this.imageWithMarkedRegionsViewerCurrentShape.Location = new System.Drawing.Point(376, 3);
            this.imageWithMarkedRegionsViewerCurrentShape.Modifying = false;
            this.imageWithMarkedRegionsViewerCurrentShape.MouseWheelZoom = false;
            this.imageWithMarkedRegionsViewerCurrentShape.Name = "imageWithMarkedRegionsViewerCurrentShape";
            this.imageWithMarkedRegionsViewerCurrentShape.Size = new System.Drawing.Size(367, 601);
            this.imageWithMarkedRegionsViewerCurrentShape.TabIndex = 0;
            this.imageWithMarkedRegionsViewerCurrentShape.UseDefaultContextMenu = false;
            this.imageWithMarkedRegionsViewerCurrentShape.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imageWithMarkedRegionsViewerCurrentShape_MouseDown);
            this.imageWithMarkedRegionsViewerCurrentShape.MouseLeave += new System.EventHandler(this.imageWithMarkedRegionsViewerCurrentShape_MouseLeave);
            this.imageWithMarkedRegionsViewerCurrentShape.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imageWithMarkedRegionsViewerCurrentShape_MouseMove);
            this.imageWithMarkedRegionsViewerCurrentShape.MouseUp += new System.Windows.Forms.MouseEventHandler(this.imageWithMarkedRegionsViewerCurrentShape_MouseUp);
            // 
            // imageWithMarkedRegionsViewerTraining
            // 
            this.imageWithMarkedRegionsViewerTraining.BackColor = System.Drawing.SystemColors.Control;
            this.imageWithMarkedRegionsViewerTraining.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageWithMarkedRegionsViewerTraining.LeftMouseButtonTool = BioLab.GUI.DataViewers.MouseButtonTool.None;
            this.imageWithMarkedRegionsViewerTraining.Location = new System.Drawing.Point(3, 3);
            this.imageWithMarkedRegionsViewerTraining.Modifying = true;
            this.imageWithMarkedRegionsViewerTraining.Name = "imageWithMarkedRegionsViewerTraining";
            this.imageWithMarkedRegionsViewerTraining.Size = new System.Drawing.Size(367, 601);
            this.imageWithMarkedRegionsViewerTraining.TabIndex = 0;
            this.imageWithMarkedRegionsViewerTraining.UseDefaultContextMenu = false;
            // 
            // toolStripStatusLabel2
            // 
            toolStripStatusLabel2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            toolStripStatusLabel2.Size = new System.Drawing.Size(10, 17);
            toolStripStatusLabel2.Text = "|";
            // 
            // labelTestDifference
            // 
            this.labelTestDifference.Name = "labelTestDifference";
            this.labelTestDifference.Size = new System.Drawing.Size(51, 17);
            this.labelTestDifference.Text = "No Test.";
            // 
            // TryActiveShapeModels
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(946, 661);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(688, 357);
            this.Name = "TryActiveShapeModels";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Active Shape Models";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.TryActiveShapeModels_Load);
            this.groupBoxModes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPreviewTrainImg)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPose.ResumeLayout(false);
            this.tableLayoutPose.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarRotation)).EndInit();
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownModeCount)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonLoadTrainingData;
        private System.Windows.Forms.GroupBox groupBoxModes;
        private System.Windows.Forms.NumericUpDown numericUpDownPreviewTrainImg;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.Button buttonResetModes;
        private ImageWithMarkedRegionsViewer imageWithMarkedRegionsViewerTraining;
        private ImageWithMarkedRegionsViewer imageWithMarkedRegionsViewerCurrentShape;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.NumericUpDown numericUpDownModeCount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutModes;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPose;
        private System.Windows.Forms.TrackBar trackBarX;
        private System.Windows.Forms.TrackBar trackBarY;
        private System.Windows.Forms.TrackBar trackBarScale;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar trackBarRotation;
        private System.Windows.Forms.Button buttonMatch;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button buttonOMS;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel labelMatchingStatistics;
        private System.Windows.Forms.ToolStripProgressBar progressBar;
        private System.Windows.Forms.ToolStripStatusLabel labelMatchingDetails;
        private System.Windows.Forms.ToolStripStatusLabel labelTestDifference;
    }
}