using BioLab.ActiveShapes.LocalFeature;
using BioLab.ActiveShapes.Model;
using BioLab.ActiveShapes.Search;
using BioLab.GCiatto.ImageProcessing;
using BioLab.GCiatto.Math.LinearAlgebra;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes.Alignment;
using BioLab.GCiatto.Math.LinearAlgebra.Shapes.Normalization;
using BioLab.GCiatto.Utils;
using BioLab.GUI.Common;
using BioLab.ImageProcessing;
using BioLab.Math.Geometry;
using BioLab.Math.LinearAlgebra;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace BioLab.GUI.Forms
{
    public partial class TryActiveShapeModels : Form
    {
        #region Campi privati
        ImageWithMarkedRegions[] arImr = null;
        ImageWithMarkedRegions currentShape = null;
        Image<byte> emptyImg = null;
        TrackBar[] trackBarModes;
        IActiveShapeModel asm;
        IActiveShapeInstance<byte> asmInstance;
        ICache<Image<byte>> validationImageCache;
        #endregion


        public TryActiveShapeModels()
        {
            InitializeComponent();

            imageWithMarkedRegionsViewerTraining.Modifying = false;
            imageWithMarkedRegionsViewerCurrentShape.Modifying = false;
        }

        private void buttonLoadTrainingData_Click(object sender, EventArgs e)
        {
            try
            {
                using (var dlg = new OpenFileDialog())
                {
                    dlg.Multiselect = true;
                    dlg.Filter = "Image With Regions Text Files (*.mkrtxt)|*.mkrtxt|All Files (*.*)|*.*";

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        using (var wc = new WaitCursor())
                        {
                            arImr = new ImageWithMarkedRegions[dlg.FileNames.Length];
                            for (int i = 0; i < dlg.FileNames.Length; i++)
                            {
                                var fpath = dlg.FileNames[i];

                                //var folder = Path.GetDirectoryName(fpath);
                                //var ext = Path.GetExtension(fpath);
                                //var fname = Path.GetFileNameWithoutExtension(fpath);

                                //if (fname.Contains("."))
                                //{
                                //    fname = fname.Split(new char[] { '.' })[0];
                                //    folder = Directory.GetParent(folder).FullName;
                                //    fpath = folder + Path.DirectorySeparatorChar + fname + ext;
                                //}

                                ImageWithMarkedRegions mkr;
                                if (File.Exists(Path.ChangeExtension(fpath, ".jpg")))
                                    mkr = new ImageWithMarkedRegions(ImageBase.LoadFromFile(Path.ChangeExtension(fpath, ".jpg")));
                                else if (File.Exists(Path.ChangeExtension(fpath, ".png")))
                                    mkr = new ImageWithMarkedRegions(ImageBase.LoadFromFile(Path.ChangeExtension(fpath, ".png")));
                                else
                                    throw new ArgumentException();

                                mkr.LoadRegionsFromTextFile(fpath);

                                arImr[i] = mkr;
                            }

                            if (arImr.Length > 0)
                            {
                                imageWithMarkedRegionsViewerTraining.SetImageWithMarkedRegions(arImr[0]);
                                emptyImg = new Image<byte>(arImr[0].Width, arImr[0].Height);
                            }

                            numericUpDownPreviewTrainImg.Minimum = 0;
                            numericUpDownPreviewTrainImg.Maximum = arImr.Length - 1;
                            numericUpDownPreviewTrainImg.Value = 0;

                            ResetControl();
                        }
                    }
                }
            }
            catch (InvalidCastException)
            {
                MessageBox.Show("� necessario selezionare immagini con regioni", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void numericUpDownPreviewTrainImg_ValueChanged(object sender, EventArgs e)
        {
            int index = (int)numericUpDownPreviewTrainImg.Value;
            if (arImr[index] != imageWithMarkedRegionsViewerTraining.Image)
            {
                imageWithMarkedRegionsViewerTraining.SetImageWithMarkedRegions(arImr[index]);
            }
        }

        private void TryActiveShapeModels_Load(object sender, EventArgs e)
        {
            UpdateTrackBarModes();
        }

        private void UpdateTrackBarModes()
        {
            tableLayoutModes.Controls.Clear();
            trackBarModes = new TrackBar[(int)numericUpDownModeCount.Value];
            for (int i = 0; i < trackBarModes.Length; i++)
            {
                trackBarModes[i] = new TrackBar();
                trackBarModes[i].Minimum = -100;
                trackBarModes[i].Maximum = +100;
                trackBarModes[i].Value = 0;
                trackBarModes[i].TickFrequency = 10;
                trackBarModes[i].Dock = DockStyle.Fill;
                trackBarModes[i].ValueChanged += new EventHandler(TrackBar_ValueChanged);
                tableLayoutModes.Controls.Add(trackBarModes[i], 0, i);
            }
        }

        private void UpdatePoseTrackBars()
        {
            trackBarX.Maximum = currentShape.Width;
            trackBarY.Maximum = currentShape.Height;
            trackBarX.Value = currentShape.Center.X;
            trackBarY.Value = currentShape.Center.Y;
            trackBarScale.Value = 0;
            trackBarRotation.Value = 0;
        }

        void TrackBar_ValueChanged(object sender, EventArgs e)
        {
            if (currentShape != null)
                UpdateCurrentShape();
        }

        private void UpdateCurrentShape()
        {
            if (currentShape != null)
            {
                var modeValues = new Vector(trackBarModes.Length);
                for (int i = 0; i < trackBarModes.Length; i++)
                {
                    modeValues[i] = ((double)trackBarModes[i].Value / 100) * asm.GetModeRange(i);
                }
                //asm.Adapt(currentShape, modeValues);
                var p = new Point2D(trackBarX.Value, trackBarY.Value);
                var s = System.Math.Pow(2, trackBarScale.Value / 50d);
                var r = trackBarRotation.Value * System.Math.PI / 18000;
                asmInstance.Pose = Affinity2D.NewRotation(r).Scale(s).Translate(p.X, p.Y);
                asmInstance.Modes = modeValues;
                AdaptCurrentShape();
            }
            else
            {
                imageWithMarkedRegionsViewerCurrentShape.SetImageWithMarkedRegions(null);
            }
            imageWithMarkedRegionsViewerCurrentShape.Invalidate();
        }

        private void AdaptCurrentShape()
        {
            asmInstance.Adapt(currentShape);
            imageWithMarkedRegionsViewerCurrentShape.SetImageWithMarkedRegions(currentShape);
            if (validationImageCache != null)
            {
                using (var wc = new WaitCursor())
                {
                    var matchings = asmInstance.ConfidenceMatchings(validationImageCache.Value);
                    var matchingPercent = matchings.Select(it => it * 100).Aggregate((a, b) => a + b);
                    matchingPercent /= matchings.Count;
                    var matchingMin = matchings.Select(it => it * 100).Aggregate((a, b) => System.Math.Min(a, b));
                    var matchingMax = matchings.Select(it => it * 100).Aggregate((a, b) => System.Math.Max(a, b));
                    var matchinsString = matchings
                        .Select(it => String.Format("{0:0.#}", it * 100))
                        .Aggregate((a, b) => a + " " + b);
                    labelMatchingStatistics.Text = String.Format("Avg: {0:0.##}, Min: {1:0.##}, Max: {2:0.##}", matchingPercent, matchingMin, matchingMax);
                    labelMatchingDetails.Text = matchinsString;
                    if (testShape != null)
                    {
                        var diff = asmInstance.Difference(testShape);
                        labelTestDifference.Text = String.Format("Diff: {0:0.##}", diff);
                    }
                }
            }
        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            //try
            //{
            using (var wc = new WaitCursor())
            {
                //var extractor = Extractors.RadialGradientSamples(10);
                //var extractor = Extractors.RadialIntensityDifferences(10);
                var extractor = Extractors.RadialSIFT(2);
                //Asm = new VR_ActiveShapeModel();
                //Asm.Calculate(arImr, checkBoxCenter.Checked, trackBarModes.Length);
                asm = ASM.Builder(arImr)
                    .Normalize(ShapeNormalizers.MeanNormalize)
                    .Align(Aligners.ZeroCenteredAlign)
                    .SetNumberOfModes(trackBarModes.Length)
                    .SetFeatureExtractor(extractor)
                    .SetAggregatorProvider(Aggregators.ParzenWindow)
                    .Build();

                //var instanceExtractor = Extractors.RadialGradientSamples(10);
                //var instanceExtractor = Extractors.RadialIntensityDifferences(10);
                var instanceExtractor = Extractors.RadialSIFT(2);
                asmInstance = asm.Instance(instanceExtractor);
            }
            //}
            //catch
            //{
            //    MessageBox.Show("Impossibile calcolare il modello");
            //    return;
            //}
            currentShape = new ImageWithMarkedRegions(emptyImg);
            UpdatePoseTrackBars();
            UpdateCurrentShape();
        }

        private void buttonResetModes_Click(object sender, EventArgs e)
        {
            ResetModes();
        }

        private void ResetModes()
        {
            for (int i = 0; i < trackBarModes.Length; i++)
            {
                trackBarModes[i].Value = 0;
            }
        }

        private void numericUpDownModeCount_ValueChanged(object sender, EventArgs e)
        {
            UpdateTrackBarModes();

            ResetControl();
        }

        private void ResetControl()
        {
            asm = null;
            asmInstance = null;
            currentShape = null;
            validationImageCache = null;
            ResetModes();
            UpdateCurrentShape();
        }

        private void buttonMatch_Click(object sender, EventArgs e)
        {
            LoadValidationImage();
            UpdatePoseTrackBars();
            //UpdateCurrentShape();
            if (testShape != null)
            {
                asmInstance.AlignTo(testShape);
                AdaptCurrentShape();
            }
            else
            {
                UpdateCurrentShape();
            }
        }

        private Shape testShape;

        private const double DS = 0.2;
        private readonly Random RAND = new Random();
        private void DeformInstance()
        {
            var s = RAND.NextDouble() * (2 * DS) + (1 - DS);
            asmInstance.Pose = asmInstance.Pose.Combine(Affinity2D.NewScale(s));
        }

        private void LoadValidationImage()
        {
            try
            {
                using (var dlg = new OpenFileDialog())
                {
                    dlg.Multiselect = false;
                    //dlg.Filter = "PNG Image (*.png)|*.png|All Files (*.*)|*.*";
                    dlg.Filter = "PNG Image (*.png)|*.png|JPEG Image (*.jpg)|*.jpg|Image With Regions Text Files (*.mkrtxt)|*.mkrtxt";

                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        using (var wc = new WaitCursor())
                        {
                            var fpath = dlg.FileName;
                            if (Path.GetExtension(dlg.FileName) == ".mkrtxt")
                            {
                                ImageWithMarkedRegions testImage;

                                if (File.Exists(Path.ChangeExtension(fpath, ".jpg")))
                                    testImage = new ImageWithMarkedRegions(ImageBase.LoadFromFile(Path.ChangeExtension(fpath, ".jpg")));
                                else if (File.Exists(Path.ChangeExtension(fpath, ".png")))
                                    testImage = new ImageWithMarkedRegions(ImageBase.LoadFromFile(Path.ChangeExtension(fpath, ".png")));
                                else
                                    throw new ArgumentException();

                                testImage.LoadRegionsFromTextFile(fpath);
                                currentShape = new ImageWithMarkedRegions(testImage);
                                testShape = testImage.Regions.ToVectorList().ToShape();
                            }
                            else
                            {
                                var image = ImageBase.LoadFromFile(fpath);
                                currentShape = new ImageWithMarkedRegions(image);
                            }
                            validationImageCache = Caches.Of(() => currentShape.AsByteImage());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private bool dragging = false;
        private IntPoint2D lastMousePos = new IntPoint2D();
        private IntPoint2D lastMouseMov = new IntPoint2D();

        private void UpdateLastMousePosition(MouseEventArgs e)
        {
            lastMouseMov.X = e.X - lastMousePos.X;
            lastMouseMov.Y = e.Y - lastMousePos.Y;
            lastMousePos.X = e.X;
            lastMousePos.Y = e.Y;
        }

        private void MoveCurrentShapeAfterDrag()
        {
            if (lastMouseMov.X > 0)
                trackBarX.Value = System.Math.Min(trackBarX.Value + lastMouseMov.X, trackBarX.Maximum);
            else if (lastMouseMov.X < 0)
                trackBarX.Value = System.Math.Max(trackBarX.Value + lastMouseMov.X, trackBarX.Minimum);

            if (lastMouseMov.Y > 0)
                trackBarY.Value = System.Math.Min(trackBarY.Value + lastMouseMov.Y, trackBarY.Maximum);
            else if (lastMouseMov.Y < 0)
                trackBarY.Value = System.Math.Max(trackBarY.Value + lastMouseMov.Y, trackBarY.Minimum);
        }

        private void imageWithMarkedRegionsViewerCurrentShape_MouseDown(object sender, MouseEventArgs e)
        {
            dragging ^= true;
            imageWithMarkedRegionsViewerCurrentShape.Capture = dragging;
            if (dragging)
                UpdateLastMousePosition(e);
        }

        private void imageWithMarkedRegionsViewerCurrentShape_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                imageWithMarkedRegionsViewerCurrentShape.Capture = true;
                UpdateLastMousePosition(e);
                MoveCurrentShapeAfterDrag();
            }
        }



        private void imageWithMarkedRegionsViewerCurrentShape_MouseUp(object sender, MouseEventArgs e)
        {
            imageWithMarkedRegionsViewerCurrentShape.Capture = false;
            dragging = false;
        }

        private void imageWithMarkedRegionsViewerCurrentShape_MouseLeave(object sender, EventArgs e)
        {
            imageWithMarkedRegionsViewerCurrentShape.Capture = false;
            dragging = false;
        }

        private void buttonOMS_Click(object sender, EventArgs e)
        {
            if (currentShape != null && validationImageCache != null && asmInstance != null)
            {
                var oms = asmInstance.OptimalModesSearch(validationImageCache.Value, 10, 0, 1);
                progressBar.Value = 0;
                oms.ProgressChanged += Oms_ProgressChanged;
                oms.IntermediateResult += Oms_IntermediateResult;
                asmInstance = oms.Exectute();
                AdaptCurrentShape();
            }
        }

        private void Oms_IntermediateResult(object sender, BioLab.Common.AlgorithmIntermediateResultEventArgs e)
        {
            asmInstance = e.IntermediateResult as IActiveShapeInstance<byte>;
            AdaptCurrentShape();
            Refresh();
        }

        private void Oms_ProgressChanged(object sender, BioLab.Common.AlgorithmProgressChangedEventArgs e)
        {
            progressBar.Value = (int) (e.ProgressPercentage * 100);
        }
    }
}