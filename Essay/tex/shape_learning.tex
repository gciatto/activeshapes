\section{Apprendimento della forma}

Questa sezione descrive il processo che porta alla creazione di un modello di forma per una determinata classe di oggetti. Onde evitare fraintendimenti e incomprensioni, fare riferimento al seguente glossario:

\begin{description}

\item[Percorso] è una spezzata aperta o chiusa in uno spazio bidimensionale.
Algebricamente si tratta di una coppia in cui il primo elemento è un \emph{booleano} che indica se la spezzata è chiusa o meno mentre il secondo è una \emph{lista} di punti di $\mathbb{R}^{2}$ in cui ogni coppia contigua di punti va interpretata come segmento. Ogni percorso è quindi strutturato così: $ P = (c,\ [p_1,\ \ldots,\ p_n]) $ dove il valore di $c$ indica la \emph{chiusura} del percorso, ovvero è $true$ se esso è chiuso, $false$ altrimenti; $n$ è il numero di punti che lo compongono ed ogni $p_i$ è a sua volta una coppia di numeri reali $p_i = (x_i,\ y_i)$.

\item[Regione] è sinonimo di percorso.

\item[Forma] è una lista di percorsi e ogni forma è quindi strutturata così: $ F = [P_1,\ \ldots,\ P_m] $, dove $m$ è il numero di percorsi che la compongono e ogni $P_j$ è un percorso. 

\item[\emph{Shape}] è una enumerazione di tutti i punti che compongono tutti i percorsi di una data forma. Dato l'insieme di punti che compongono una forma, tutte le sue permutazioni sono shape valide, anche se, ovviamente, è conveniente fissare la convenzione per cui si impieghi sempre l'enumerazione implicita nella forma stessa: ad esempio alla forma \[
F=[(c_{1},\ [p_{1,1},\ \ldots,\ p_{1,n_{1}}]),\ \ldots,\ (c_{m},[p_{m,1},\ \ldots,\ p_{m,n_{m}}])]
\] dovrebbe corrispondere la shape \[
S_F=[p_{1,1},\ \ldots,\ p_{1,n_{1}},\ \ldots,\ p_{m,1},\ \ldots,\ p_{m,n_{m}}]
\]

\item[Pattern di percorso] è una descrizione sintetica di un percorso. Dato il percorso $P$ con chiusura $c$ e composto da $n$ punti, il pattern ad esso associato è la coppia $T_P = (c,\ n)$. Viceversa si dice $P$ combacia (in gergo \enquote{matcha}) con $T_P$.

\item[Pattern di forma] è una descrizione sintetica di una forma ed è composta da una lista di pattern di percorso. Dire che due forme rispettano lo stesso pattern significa dire che esse hanno: \begin{itemize}
    \item stesso numero e ordine di percorsi
    \item stesso numero e ordine di punti per ogni percorso
    \item stessa chiusura per ogni percorso
  \end{itemize}

\end{description}

L'ipotesi di fondo è quella di essere in possesso di un corposo \ts\ composto da $l$ coppie immagine-forma tali per cui tutte le forme rispettino il medesimo pattern. Data la facilità con la quale è possibile estrarre una shape data una forma, ciò significa disporre di $l$ coppie immagine-shape, si parlerà pertanto, a seconda dei casi, di $k$-esima forma o $k$-esima shape.

\subsection{Gradi di libertà di una forma}

\begin{figure}
  \centering
    \includegraphics[scale=0.3]{img/face_figure.PNG}
  \caption{\label{img.shapes.face} Rappresentazione di una immagine \cite{stegmann02} con \emph{forma} in sovrimpressione}
\end{figure}

L'idea di base è quella di sfruttare il potenziale statistico dato dalla presenza di $l$ esempi per una certa forma così da poter in qualche maniera catturare e controllare i \emph{modi} in cui essa possa essere deformata, stando agli esempi disponibili. Così com'è definita, ogni shape avente $n$ punti possiede $2n$ gradi di libertà: è infatti possibile spostare ogni suo punto in entrambe le direzioni principali in maniera indipendente dagli altri punti, dato che non sussistono vincoli tra di essi. Tuttavia nella quasi totalità delle applicazioni pratiche, esistono dei vincoli, seppur spesso difficili da esprimere, tra i punti che compongono la shape. Si prendano ad esempio i sei percorsi che compongono la forma di una faccia in \figref{img.shapes.face}, non sarebbe corretto affermare che i punti componenti il contorno dell'occhio non siano soggetti a vincoli: un vincolo banale potrebbe infatti essere che essi debbano necessariamente trovarsi \emph{sotto} i punti che compongono il sopracciglio (in un sistema di riferimento solidale con la faccia stessa). Esprimere vincoli di questo genere in maniera analitica potrebbe rivelarsi una soluzione complicata e poco scalabile. Risulta dunque evidente che i gradi di liberà con cui possono muoversi i punti che compongono la forma di un oggetto in un'immagine siano assai inferiori a $2n$. 

Ipotizzando che le forme del \ts\ siano ubicate in un sistema di riferimento centrato nel loro \cdg\, ruotato secondo una direzione di riferimento e scalato in maniera opportuna, è lecito supporre che ogni differenza residua tra le varie forme sia da attribuirsi a lecite variazioni di cui il \ts\ stesso rappresenta un campione.

\subsection[Analisi delle componenti principali]{Analisi delle componenti principali \cite{wiki:pca}}
Gli \acShMo s descritti in \cite{cootes95} impiegano la tecnica nota come PCA (\emph{Principal Component Analysis}) o trasformata KL (di \emph{Karhunen-Loève}) che serve per ridurre la dimensionalità dei dati. Se si considera infatti una shape avente $n$ punti come un vettore di lunghezza $2n$ nella forma \[
S_F = (x_1,\ y_1,\ \ldots,\ x_n,\ y_n)^T
\] è facile immaginare come la dimensione dei dati trattati possa facilmente rappresentare un impedimento per l'elaborazione.
Dato che in questa fase le immagini del \ts\ non servono, è possibile considerare quest'ultimo una matrice di dimensione $l \times 2n$ in cui ogni riga è il vettore \emph{a dimensionalità elevata} associato ad una shape. La PCA assume che ogni vettore sia un campione di una popolazione di shape distribuite normalmente nello spazio $2n$-dimensionale e che, pertanto, esse giacciano su un ellissoide avente $2n$ dimensioni di cui permette di conoscere gli assi e le varianze che i dati stessi hanno rispetto ad essi.

Il primo passo prevede il calcolo della shape media, ovvero la media dei vettori associati alle shape: \begin{equation}
	\mathbf{\bar{x}} = \frac{1}{l} \sum_{i=1}^l \mathbf{x}_i
	\marginnote{
		\tiny{
			$\mathbf{\bar{x}}: 2n \times 1$ \\
			$\mathbf{x}_i: 2n \times 1, \forall i$
		}
	}
	\end{equation} che è indispensabile per poter calcolare la matrice di covarianza: \footnote{
		si prega di ricordare che, dati due vettori $\mathbf{a}$ e $\mathbf{b}$ di dimensione $d$, essi sono colonne per convenzione, ovvero matrici $d \times 1$; c'è pertanto differenza tra scrivere $\mathbf{a}^T \mathbf{b}$ e $\mathbf{a} \mathbf{b}^T$: nel primo caso si parla di \emph{inner product}, che produce uno scalare (ovvero una matrice $1 \times 1$), nel secondo si parla di \emph{outer product}, che produce una matrice $d \times d$
	} \begin{equation} 
		S = \frac{1}{l} \sum_{i=1}^l (\mathbf{x}_i - \mathbf{\bar{x}})(\mathbf{x}_i - \mathbf{\bar{x}})^T 
		\marginnote{\tiny{$S: 2n \times 2n$}}
	\end{equation} 

I $2n$ autovettori di $S$ rappresentano gli assi dell'ellissoide iper-dimensionale su cui abbiamo supposto essere giacenti i vettori delle shape, mentre l'autovalore $k$-esismo rappresenta la varianza di quest'ultimi rispetto all'asse relativo al $k$-esimo autovalore. Se chiamiamo $P$ la matrice che ha per colonne gli autovettori di $S$ e $\Lambda$ la matrice diagonale il cui elemento $\lambda_{k,k}$ è il $k$-esimo autovalore di $S$, è lecito scrivere: $ P^{-1} S P = \Lambda $, \marginnote{
	\tiny{
		$P: 2n \times 2n$ \\
		$\Lambda: 2n \times 2n$
	}
} il che, dato che $S$ è \emph{simmetrica} per definizione, equivale a: $ P^T S P = \Lambda $.


Se si considerano i vettori $\mathbf{x}_i$ relativi alle shape nel sistema di riferimento centrato in $\mathbf{\bar{x}}$ e avente per assi le colonne di $P$, è possibile scrivere ogni vettore $\mathbf{x}_i$ o, più in generale, un qualunque vettore $\mathbf{x}$ come: \begin{equation} \label{eq.pca.projection}
	\mathbf{x} = \mathbf{\bar{x}} + P \mathbf{b}
	\marginnote{
		\tiny{
			$\mathbf{x}: 2n \times 1$ \\
			$\mathbf{\bar{x}}: 2n \times 1$ \\
			$P: 2n \times 2n$ \\
			$\mathbf{b}: 2n \times 1$
		}
	}
\end{equation}

Applicando semplici vincoli alle componenti del vettore $\mathbf{b}$ (che da qui in avanti sarà chiamato \emph{vettore dei modi} e le sue componenti semplicemente \emph{modi di variazione}) è possibile imporre la \emph{coerenza} della forma rappresentata da $\mathbf{x}$. I vincoli imposti sono i seguenti: \begin{itemize}
\item è utile costringere il $k$-esimo modo di variazione all'interno del range dato da $\pm 3$ deviazioni standard relative al $k$-esimo asse: \begin{equation} 
	-3 \sqrt{\lambda_k} \leq b_k \leq 3 \sqrt{\lambda_k}
\end{equation} 

\item dato un numero intero $t$ tale che $t > 0, t < 2n, t < l$ è possibile individuare i $t$ autovalori più significativi (maggiori in valore assoluto) e considerare nulli gli altri, il che equivale, considerando l'enumerazione \begin{equation}
	\lambda_1 \geq \lambda_2 \geq \ldots \geq \lambda_t \geq \ldots \geq \lambda_{2n}
\end{equation} a fissare a zero i $b_k$ con $k = t+1,\ \ldots,\ 2n $ nella \vref{eq.pca.projection}.
\end{itemize}


\subsection{Allineamento del \ts}
La PCA, come accennato, richiede di trovarsi nella comoda e stringente ipotesi che le shape del \ts\ condividano un sistema di riferimento adeguato. Per ottemperare a tale necessità è necessario \emph{allineare} il \ts. Esiste una varietà di tecniche che portano a tale risultato. Nella fattispecie, l'algoritmo di riferimento è descritto in \cite{cootes00,cootes95} e nel dettaglio in \cite{keomany06}.

\paragraph{Normalizzazione}
Si consideri la shape $i$-esima come lista di $n$ punti nel piano: \begin{equation} \label{eq.alignment.shape.i}
	\mathbf{x}_i = [p_{i,1},\ p_{i,2},\ \ldots,\ p_{i,n}]
\end{equation} il \cdg\ della shape è la media dei vari $p_j$: \begin{equation}
	\mu_i = \frac{1}{n} \sum_{j=0}^n p_j
\end{equation} il primo passo consiste nel normalizzare ogni shape rispetto al proprio \cdg , ovvero traslarla in maniera tale esso coincida con l'origine, ottenendo così: \begin{equation}
	{\mathbf{x}'}_i = [{p'}_{i,1},\ \ldots,\ {p'}_{i,n}]\ :\ {p'}_{i,j} = p_{i,j} - \mu_i 
\end{equation} ciò semplificherà il lavoro successivo. Da qui in avanti, per semplicità di notazione, il simbolo $\mathbf{x}_i$ verrà usato in sede di ${\mathbf{x}'}_i$ per riferirsi alla shape centrata nell'origine.

\paragraph{Allineamento di una coppia di shape}
Nelle ipotesi fatte sin qui, allineare due shape $\mathbf{w}$ e $\mathbf{z}$ significa trovare i parametri ottimi della trasformazione lineare $T$ che, se applicata a $\mathbf{w}$, produce $T(\mathbf{z})$. Nel seguito si scriverà, con abuso di notazione, $T(\mathbf{w})$ intendendo la lista di punti ottenuta applicando la trasformazione $T$ a tutti i punti di $\mathbf{w}$: \begin{equation}
T(\mathbf{w}) = [T(w_1),\ \ldots,\ T(w_n)]\ :\ w_j = (x_{w,j},\ y_{w,j})
\end{equation} 

La trasformazione deve \emph{scalare uniformemente} e \emph{ruotare} $\mathbf{w}$ affinché sia minima la differenza: \begin{equation}
	E = | T(\mathbf{w}) - \mathbf{z} |^2
\end{equation} e per tanto la forma della matrice associata a $T$ sarà: \begin{equation}
	T = \left(  \begin{array}{cc}
	s \cos\theta & -s \sin\theta \\ 
	s \sin\theta & s \cos\theta
	\end{array} \right)
	= \left(  \begin{array}{cc}
	a & -b \\ 
	b & a
	\end{array} \right)
\end{equation} dove $s$ è il fattore di scala e $\theta$ è l'angolo di rotazione. È importante notare che, pragmaticamente, non interessano quei due paramentri in sé, quanto magari i soli parametri $a$ e $b$, dato che ciò che importa è, in fin dei conti, l'intera matrice $T$.
Si dimostra in \cite{keomany06} che esiste una soluzione analitica a tale problema ed è: \begin{equation}
a = \frac{
	\sum_{j=1}^{n} x_{w,j} \cdot x_{z,j} + y_{w,j} \cdot y_{z,j}
}{
	\sum_{j=1}^{n} x_{w,j}^2 + y_{w,j}^2
} = \frac {
	\mathbf{w}^T \mathbf{z} 
}{
	|\mathbf{w}|^2
}
\end{equation} \begin{equation}
b = \frac{
	\sum_{j=1}^{n} x_{w,j} \cdot y_{z,j} - y_{w,j} \cdot x_{z,j}
}{
	\sum_{j=1}^{n} x_{w,j}^2 + y_{w,j}^2
} = \frac {
	\sum_{j=1}^{n} x_{w,j} \cdot y_{z,j} - y_{w,j} \cdot x_{z,j}
}{
	|\mathbf{w}|^2
}
\end{equation} in cui $\mathbf{w}$ e $\mathbf{z}$ sono considerati vettori $2n$-dimensionali.

\subparagraph{Allineamento di coppia di shapes con c.d.g. qualsiasi}
Nel caso più generale in cui $\mathbf{w}$ abbia \cdg\ in $\mu_w$ e $\mathbf{z}$ in $\mu_z$, una maniera semplice di ri-usare i ragionamenti descritti sopra è il seguente: \begin{equation} \label{eq:alignment:couple:general}
\begin{array}{c}
\mathbf{z} - \mu_z \simeq T(\mathbf{w} - \mu_w) \\
\Updownarrow \\
\mathbf{z} \simeq \mu_z + T(\mathbf{w} - \mu_w)
\end{array}
\end{equation} che è una maniera molto ricca di abusi di notazione per esprimere l'idea che le immagini vadano \begin{itemize}
\item traslate entrambe in modo che il loro c.d.g. sia l'origine
\item allineate come di consueto
\item riportare $\mathbf{z}$ nella sua posizione originale e traslare $\mathbf{w}$ in maniera che il suo nuovo c.d.g. sia $\mu_z$
\end{itemize}
La sottrazione (risp. addizione) per $\mu$ esprime sinteticamente la traslazione dell'intera shape che si realizza sottraendo (risp. addizionando) $\mu$ ad ogni suo punto.

\paragraph{Allineamento del \ts}
Avendo a disposizione un \ts\ di shape già normalizzate è necessario che tutte siano tra loro allineate affinché l'estrazione delle componenti principali funzioni al meglio. Risolto il problema dell'allineamento di una coppia di shape, resta aperta la questione su come allineare una lista di $l$ shape. Una buona prassi, che sfrutta quanto precedentemente illustrato, è quella descritta in \cite{cootes00,cootes95} e con maggior dettaglio in \cite{keomany06} \begin{algorithm}[ht]
\caption{\label{alg:alignment:ts} Allineamento del \ts\ già normalizzato}
\begin{algorithmic}[1]
\State \Comment{Il \ts\ è composto dalle shape normalizzate}
\State $TS \gets [ {\mathbf{x}'}_1,\ \ldots,\ {\mathbf{x}'}_l ]$
\State \Comment{Stima iniziale della \emph{shape media} pari al primo elemento del \ts}
\State $\mathbf{\bar{x}} \gets {\mathbf{x}'}_1,\ step \gets 0$
\Repeat
	\State \Comment{Accumulatore che tiene traccia della variazione totale}
	\State $e \gets 0$
	\State \Comment{Normalizzazione della shape media}
	\State $\mathbf{\bar{x}} \gets \mathbf{\bar{x}} / |\mathbf{\bar{x}}|$
	\ForAll{$\mathbf{x} \in TS$}
		\State $\mathbf{x}_{old} \gets \mathbf{x}$
		\State \Comment{Calcolo della trasformazione lineare che allinea $\mathbf{x}$ a $\mathbf{\bar{x}}$}
		\State $F_{s,\theta} \gets$ \Call{Align\_Couple}{$\mathbf{x},\ \mathbf{\bar{x}}$}
		\State \Comment{Applicazione della trasformazione lineare a $\mathbf{x}$}
		\State $\mathbf{x} \gets F_{s,\theta}(x)$
		\State \Comment{Proiezione della shape corrente nello \emph{spazio tangente}}
		\State $\mathbf{x} \gets \mathbf{x} / \left( \mathbf{x}^T \mathbf{\bar{x}} \right)$
		\State \Comment{Accumulazione della variazione subita dalla shape corrente}
		\State $e \gets e + |\mathbf{x} - \mathbf{x}_{old}|$
	\EndFor
	\State $step \gets step + 1$
\State \Comment{Terminazione se raggiunto limite di iterazioni o variazione totale inferiore a una certa soglia}
\Until{$step < Limit \wedge e \geq Threshold$}
\end{algorithmic}
\end{algorithm} e qui riportata nell'\algoref{alg:alignment:ts}. Come si può notare, trattasi di un approccio euristico che non garantisce una soluzione ottima. Tuttavia, come evidenziato negli articoli citati, generalmente una buona soluzione è raggiunta in poche iterazioni.
La proiezione nello \emph{spazio tangente} \cite{wiki:tangentspace} serve a ridurre l'effetto delle non-linearità introdotte da questo metodo: data una \emph{varietà} immersa in uno spazio multi-dimensionale (come quella su cui si sono supposte giacenti le shape del \ts), lo spazio tangente in punto della varietà è l'iper-piano tangente alla varietà in quel punto. Proiettare l'intorno di un punto della varietà, in generale non lineare, sul piano tangente permette, tramite un'approssimazione, di rimuovere l'eventuale non-linearità.
