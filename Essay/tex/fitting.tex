\section{Fitting (semi-)automatico}
Come anticipato, per fitting (semi-)automatico di un'istanza di un \asm\ su un'immagine, si intende la procedura che, a partire da un fitting iniziale supposto sufficientemente buono, lo migliora sino a fare aderire al meglio l'istanza all'immagine. Se scorporato dai concetti di extractor e matcher, all'algoritmo di fitting non rimangono molti gradi di libertà: infatti essi sono alla base del suo funzionamento, nonché due dei principali punti di intervento in caso si voglia modificarne il comportamento. La procedura è descritta \begin{algorithm}[!ht]
\caption{\label{alg:search} Algoritmo di fitting. Si noti che la terna $M = \left\langle \mathbf{\bar{x}},\ P,\ \kappa \right\rangle$ è un modello.}
\begin{algorithmic}[1]
\Function{Optimal\_Fitting\_Search}{$\mathbf{\bar{x}},\ P,\ I,\ \epsilon,\ \kappa=[\kappa_1,\ \ldots,\ \kappa_n]$}
\Statex
\State \Comment{Generazione di una stima iniziale di un fitting (modi + posa)}
\State $(\mathbf{b},\ F_{s,\theta,p_0}) \gets$ \Call{Extimate\_Fitting}{$\mathbf{\bar{x}},\ P,\ I,\ \epsilon,\ \kappa$}
\State \Comment{Istanza del modello che non tiene conto della posa}
\State $\mathbf{x} \gets \mathbf{\bar{x}} + P \mathbf{b}$
\State \Comment{Istanza del modello trasformata secondo i parametri di posa}
\State $\mathbf{y} \gets F_{s,\theta,p_0}(\mathbf{x}) = [p_{y,1},\ ...,\ p_{y,n}]$
%\State $\mu_y \gets$ \Call{Mean}{$\mathbf{y}$}
%\State $step \gets 0$
\Statex
\For{$step \gets 1$ \textbf{to} $Limit$} \Comment{Per limitare il numero di iterazioni}
	\State $\mathbf{y}' \gets [{p'}_{y,1},\ \ldots,\ {p'}_{y,n}]$ \Comment{Inizialmente tutti nulli}
	\For{$j \gets 1$ \textbf{to} $n$}
		\State $movements \gets 0$
		%\State $pointsToSample \gets$ \Call{Neighborhood}{$p'_{y,j},\ \mu_y$}
		\Statex
		\State \Comment{Selezione di una quantità di punti nell'intorno di $p_{y,j}$}
		\State $pointsToSample \gets$ \Call{Neighborhood}{$p_{y,j},\ p_0$}
		\Statex
		\State \Comment{Campionamento}
		\State $samples \gets \emptyset$ \Comment{Set di coppie punto-descrittore}
		\ForAll{$p \in pointsToSample$}
			\State $samples \gets samples \cup (p,\ \epsilon(p,\ I))$
		\EndFor
		\Statex
		\State ${p'}_{y,j} \gets$ \Call{Best\_Sample}{$samples,\ \kappa_j$}
		\Statex
		\State \Comment{Eventuale incremento del conteggio dei punti spostatisi}
		\If{$p_{y,j} \neq {p'}_{y,j}$}
			\State $movements \gets movements + 1$ 
		\EndIf
	\EndFor
	\Statex
	\State \Comment{...}
	\algstore{ofsBreak}
\end{algorithmic} 
\end{algorithm} \begin{algorithm}[!ht]
\begin{algorithmic}[1]
	\algrestore{ofsBreak}
	\State \Comment{...}
	\Statex
	\State \Comment{Calcolo della trasformazione affine che meglio allinea $\mathbf{y}$ a $\mathbf{y'}$ }
	\State $F' \gets$ \Call{Align\_Couple}{$\mathbf{y},\ \mathbf{y'}$}
	\State \Comment{Aggiornamento dei parametri di posa}
	\State $F_{s,\theta,p_0} \gets F_{s,\theta,p_0} \cdot F'$
	\State \Comment{Aggiornamento dei punti dell'istanza}
	\State $\mathbf{y} \gets F_{s,\theta,p_0}(\mathbf{x})$
	%\State $\mu_y \gets$ \Call{Mean}{$\mathbf{y}$}
	\State \Comment{Calcolo della variazione dei parametri modali}
	\State $d\mathbf{b} \gets P^T (F_{s,\theta,p_0}^{-1}(\mathbf{y}' - \mathbf{y}) - \mathbf{\bar{x}})$ \Comment{$P^T = P^{-1}$}
	\State \Comment{Aggiornamento del vettore dei modi di variazione}
	\State $\mathbf{b} \gets \mathbf{b} + d\mathbf{b}$
	\State \Comment{Aggiornamento dell'istanza secondo i parametri modali}
	\State $\mathbf{x} \gets \mathbf{\bar{x}} + P \mathbf{b}$
	\State \Comment{Aggiornamento dell'istanza secondo i parametri di posa}
	\State $\mathbf{y} \gets F_{s,\theta,p_0}(\mathbf{x})$
	\Statex
	\State \Comment{Convergenza dichiarata se il numero di punti che si è spostato è sotto una certa soglia}
	\If{$movements < MinMovements$}
		\State \textbf{break}
	\EndIf
\EndFor
\Statex
\State \Return $(\mathbf{b},\ F_{s,\theta,p_0})$
\Statex
\EndFunction
\end{algorithmic} 
\end{algorithm} nell'\algoref{alg:search}.
Una terza via per modificarne il funzionamento lasciando inalterata la struttura è la funzione \textsc{Neighborhood} che dati un punto ed il \cdg\ di una shape, restituisce un insieme di punti da \enquote{campionare}, ovvero da cui estrarre i descrittori usando l'estrattore $\epsilon$. Il punto cui corrisponda il miglior descrittore (ossia quello che abbia matching maggiore secondo la funzione $\kappa_j$) diventerà determinerà lo spostamento del punto $j$-esimo.
Gli step principali sono concettualmente abbastanza semplici: \begin{enumerate}
\item generazione di un'istanza iniziale;
\item spostamento di ogni punto dell'istanza in una posizione migliore vicino all'attuale (se possibile) usando il matcher opportuno per guidare la scelta;
\item allineamento della shape attuale con la precedente (al fine di migliorare i parametri di posa);
\item calcolo della variazione dei parametri modali rispetto alla shape precedente e aggiornamento degli stessi;
\item se i punti dell'istanza spostatisi sono pochi \begin{itemize}
\item dichiarare la convergenza,
\item altrimenti tornare al punto 2.
\end{itemize} 
\end{enumerate}

\subsection{Selezione dei punti per il campionamento}
Uno sguardo allo pseudo-codice rivela come la quantità di punti restituiti dalla funzione $neighborhood_{\lambda, d, n_s}(p,\ \mu)$\footnote{indicata come \textsc{Neighborhood} nel listato}, nonché la loro distribuzione, influisca sensibilmente sia sulla capacità dell'algoritmo di deformare e spostare la shape, sia (negativamente) sulle performance dell'intera ricerca. L'implementazione impiegata seleziona $p$ e gli $n_s$ punti distanti $\lambda$ gli uni dagli altri sulle $d$ direzioni principali nel sistema di riferimento centrato in $p$ e aventi per assi la radiale e la sua perpendicolare. La costruzione di tale funzione è descritta nel seguito.
Si sfrutta la funzione d'appoggio $dirs_d(\hat{v})$ che, dato un versore $\hat{v}$ ritorna l'insieme $\{\hat{v},\ \hat{v}\ ruotato\ di\ \frac{2 \pi}{d},\ \ldots,\ \ \hat{v}\ ruotato\ di\ (d - 1)\frac{2 \pi}{d}\}$: \begin{equation}
dirs_d(\hat{v}) = \bigcup_{i=0}^{d - 1} \left(\begin{array}{cc}
\cos\left( i \frac{2 \pi}{d} \right) & -\sin\left( i \frac{2 \pi}{d} \right)\\
\sin\left( i \frac{2 \pi}{d} \right) & \cos\left( i \frac{2 \pi}{d} \right)
\end{array}\right) \hat{v}
\end{equation} pertanto la \emph{funzione di vicinato} è esprimibile come: \begin{equation}
neighborhood_{\lambda, d, n_s}(p,\ \mu) = \bigcup_{j=0}^{n_s} \left( \bigcup_{\hat{v} \in dirs_d(\hat{r})} p + j \lambda \hat{v} \right)
\end{equation} dove $\hat{r} = (p - \mu) / |p - \mu|$. La quantità totale di punti restituiti è quindi $\left( n_s d + 1 \right)$.

\paragraph{Note implementative} Le impostazioni di default sono $n_s=10$, $\lambda=4$ e $d=8$.

\subsection{Generazione di un fitting iniziale}
Come ripetuto più volte, l'algoritmo di fitting richiede una stima iniziale dei parametri di posa e dei modi di variazione per una data istanza di un certo modello. Nella versione attuale del progetto non è implementato alcun metodo automatico per ottemperare a tale necessità. Tuttavia l'utente ha la possibilità, tramite GUI, di spostare, ruotare, scalare e deformare (coerentemente col modello) agilmente le istanze generate. In questa demo è infatti l'utente ad eseguire "a mano" la procedura che genera una soluzione iniziale. Se c'è un lato positivo è che, in tal modo, l'utente è in grado di sperimentare la bontà dell'algoritmo di ricerca al variare delle condizioni iniziali. Fare riferimento alla \secref{sec.gui} per conoscere i dettagli relativi al funzionamento dell'interfaccia grafica.
