\section{Apprendimento di caratteristiche locali}
Grazie a quanto descritto sin qui è possibile estrarre modelli di forme da un insieme di esempi. Per chiudere il cerchio, sarebbe desiderabile avere un algoritmo in grado di adattare in maniera automatica un'istanza di un modello ad una nuova immagine. Come specificato in tutti gli articoli citati in precedenza, non esiste una sola maniera di raggiungere tale risultato. 

Non è difficile immagine come la sola identificazione di un modello di forma non sia sufficiente quando si voglia ottenere un fitting automatico. Affinché ciò sia possibile è necessario, in fase di creazione del modello, per ogni coppia immagine-forma, estrarre informazioni che descrivano l'intorno di ogni punto in maniera che poi queste siano confrontate con quelle estratte sulle immagini delle quali si voglia ottenere un fitting. Un approccio generale e al tempo stesso facilmente declinabile è quello descritto dai seguenti punti: \begin{enumerate}
  \item In fase di allenamento, prima o dopo l'applicazione della PCA, \emph{estrarre} degli opportuni descrittori locali per ogni punto di ogni forma relativa ad ogni immagine del \ts . Il problema è analogo all'individuazione di key point, risolto in molti modi, alcuni dei quali presentati durante il corso, con la differenza che in questo caso la loro \emph{posizione} è già nota. Formalmente, se consideriamo, il \ts\ come una lista di $l$ coppie: \begin{equation}
      TS = [(I_1,\ F_1),\ \ldots,\ (I_i,\ F_i),\ \ldots,\ (I_l,\ F_l)]
    \end{equation} in cui le $I_i$ sono immagini e le $F_i$ sono forme cui sono associate le shape $\mathbf{x}_i$ descritte nella \formref{eq.alignment.shape.i}, questo passaggio si traduce nell'applicazione di una funzione\footnote{$\mathbb{I} = \{I\,|\,I\,\grave{e}\,un'immagine\}$} $\epsilon:\mathbb{R}^2 \times \mathbb{I} \rightarrow \mathbb{R}^d$, detta \emph{extractor}, ad ogni punto di ogni shape, il che produce, per ognuna, una lista di $n$ descrittori di dimensione $d$: \begin{equation}
      E_i = [\epsilon(\mathbf{x}_0,\ I_i),\ \ldots,\ \epsilon(\mathbf{x}_j,\ I_i),\ \ldots,\ \epsilon(\mathbf{x}_n,\ I_i)]
    \end{equation}
    
  \item A questo punto si avranno a disposizione $l$ esempi di descrittori per ogni punto, uno per ogni immagine nel \ts . Essi andranno \emph{aggregati} impiegando una qualche tecnica statistica.
  
  \item Successivamente, da tale aggregazione dovrà essere sintetizzato un \emph{matcher} ovvero un'entità, concettualmente riducibile ad una funzione, che, preso in ingresso un descrittore e un'immagine lo \emph{confronti} con l'informazione aggregata del punto precedente restituendo un numero reale tanto più grande quanto è maggiore la confidenza con la quale è lecito affermare che il descrittore in ingresso aderisca all'immagine. In formule, al $j$-esimo punto corrisponde il matcher $\kappa_j: \mathbb{R}^d \times \mathbb{I} \rightarrow \mathbb{R}$.
  
  \item Infine, il modello sarà costituito da: \begin{itemize}
      \item i dati necessari a passare dallo spazio delle shape allo spazio ridotto e viceversa ovvero la shape media $\mathbf{\bar{x}}$ e la matrice degli autovettori $P$ che figurano nella \formref{eq.pca.projection};
      \item gli $n$ matcher $\kappa_j$.
    \end{itemize}
\end{enumerate}

Un'\emph{istanza} $\mathbf{y}$ di un modello è invece composta da
\begin{itemize}
	\item un assegnamento dei \emph{parametri di posa}, ovvero posizione $p_0=(x_0,\ y_0)$, rotazione $\theta$ e scala $s$: \begin{equation} 
			\mathbf{y} = \left( \begin{array}{cc}
			s \cos\theta & -s \sin\theta \\ 
			s \sin\theta & s \cos\theta
			\end{array} \right) \mathbf{x} + \left(
			\begin{array}{c}
			x_0 \\ y_0
			\end{array}
			\right)
		\end{equation}
	\item ed uno dei $t$ parametri modali non-nulli che compongono il vettore $\mathbf{b}$: \[ \mathbf{x} = \mathbf{\bar{x}} + P \mathbf{b} \]
\end{itemize}
	
Data un'immagine $I$ e il modello di cui $\mathbf{y}$ è espressione, si chiama grado di aderenza il valore di: \begin{equation}
K(\mathbf{y},\ I) = \sum_{j=1}^{n} \kappa_j(p_{y,j},\ I)
\end{equation} dato dall'aggregazione delle varie confidenze relative all'aderenza di ogni punto di $\mathbf{y}$. Un'adeguata funzione capace di calcolare un grado di aderenza realistico è il punto di partenza necessario per un algoritmo che voglia individuare il fitting migliore.

In quanto segue si ipotizza sempre che un algoritmo siffatto abbia in ingresso una istanza ammissibile con un grado di aderenza abbastanza buono e che il suo compito sia quello di migliorarla.

\subsection{Estrazione di descrittori locali}
\begin{figure}
  \centering
    \includegraphics[scale=0.5]{img/radial_rs.eps}
  \caption{
  	\label{img.radial_rs} Sistema di riferimento usato in fase di estrazione.
  	Il vettore radiale è $r_{i,j} = \mu_{x_i} - p_{i,j}$, il versore corrispondente è  $\hat{r} = r / |r|$.
  }
\end{figure}
Per quanto riguarda la struttura della funzione $\epsilon$, si sono ipotizzate diverse possibilità. Segue una descrizione accurata delle principali. La costante di tutte le varianti sviluppate è il sistema di riferimento impiegato in fase di estrazione: per ogni punto di ogni immagine si considera l'asse radiale e l'asse ad esso normale nel punto in esame, come mostrato in \figref{img.radial_rs}.

\subsubsection{Differenze di intensità luminosa}
L'idea è la seguente: dato che il posizionamento iniziale dell'istanza è \enquote{abbastanza buono}, è lecito supporre che un miglioramento per ogni punto dell'istanza sia da ricercarsi nel suo intorno sulla radiale. Per tanto l'extractor si occupa, dato un punto $p$ e un \cdg\ $\mu$, di campionare le differenze di intensità luminosa di un'immagine $I$ fra $2c + 1$ punti, a distanza $\lambda$ ognuno dal successivo, sulla direzione radiale $\hat{r} = (\mu - p) / |\mu - p|$. Per tanto: \[
\epsilon(p,\ I) = (
\epsilon_1(p,\ I),\ 
\ldots,\ 
\epsilon_{2c}(p,\ I)
)
\] in cui ogni componente è \begin{equation}
\epsilon_h(p,\ I) = I \left( (h - c) \lambda \hat{r} + p \right) - I \left( (h - c - 1) \lambda \hat{r} + p\right)
\end{equation} dove $I(\cdot)$ è l'intensità luminosa dell'immagine calcolata in un certo punto usando l'\emph{interpolazione di Lagrange}.

\subsubsection{Descrittori SIFT}
La \sift\  è una buona tecnica per individuare key-point e per estrarne descrittori (abbastanza) invarianti per scala, rotazione e traslazione, presentata in \cite{lowe99}. Nel seguito non sarà approfondita la trattazione del procedimento, in quanto già effettuata durante il corso.
L'idea è quella di sfruttare il solo descrittore \sift\ per immagazzinare l'informazione sull'intorno di un punto di una certa immagine. Nel seguito si considererà l'algoritmo di estrazione di un descrittore \sift\ come una funzione $sift:\mathbb{R}^2 \times \mathbb{I} \rightarrow \mathbb{R}^d$ parametrica in $\mathbf{\Omega} = (\sigma,\ \phi,\ \beta,\ \delta,\ \gamma)$, quindi: \begin{equation}
\epsilon(p,\ I) = sift_{\mathbf{\Omega}}(p,\ I)
\end{equation} in cui: \begin{itemize}
\item $\sigma$ è la scala  di riferimento
\item $\phi$ è la grandezza (deviazione standard) del filtro gaussiano
\item $\beta$ è il numero di bin
\item $\delta^2$ è il numero di sotto-finestre
\item $\gamma$ è il numero di direzioni 
\end{itemize} la dimensione del descrittore è quindi $d = \delta^2 \times \gamma$.
Per ottenere una maggiore invarianza rispetto alla rotazione si è pensato di orientare la finestra lungo la radiale. Per fare ciò è stato sufficiente sottrarre ad ogni orientazione utilizzata dall'algoritmo \sift\ l'angolo $\vartheta_{\hat{r}}$ che la radiale forma con l'asse $x$ nel sistema di riferimento dell'immagine: la $R_{ij}$ di \cite{lowe99} diventa dunque \begin{equation}
R_{ij} = atan2(A_{ij} - A_{i+1,j},\ A_{i,j+1} - A_{ij}) - \vartheta_{\hat{r}}
\end{equation}

\subsection{Aggregazione e matching di descrittori}
La maniera con cui si aggregano gli $l$ descrittori condiziona la scelta della funziona che computa il matching. Per questo motivo le due procedure vengono presentate insieme.

\subsubsection{Media e distanza euclidea}
Avendo a disposizione $l$ descrittori, ovvero vettori $d$-dimensionali, la tecnica più elementare per aggregarli è la media. Sia $\mathbf{v}_{i,j}$ il descrittore estratto per il $j$-esimo punto della $i$-esima immagine del \ts\ e sia $\mathbf{\bar{v}}_{j}$ la media aritmetica	di tutti gli $l$ descrittori relativi al $j$-esismo punto, allora una buona maniera di valorizzare l'aderenza ad esso di un punto $p$ di un'immagine $I$ è: \begin{equation}
	\kappa_j(p,\ I) = \frac{1}{1 + |\epsilon(p,\ I) - \mathbf{\bar{v}}_{j}|}
\end{equation} che cresce al diminuire della distanza tra $\mathbf{v}'$ e il descrittore medio\footnote{la funzione $f(d)=1/(1 + d)$ è un modo semplice per convertire una \emph{distanza} in una \emph{similarità}}.
Si noti che un approccio analogo è impiegabile ogni qual volta l'operatore di aggregazione dei descrittori produca a sua volta un vettore $d$-dimensionale.

\subsubsection{Similarità coseno in sede della distanza euclidea}
Una variante della tecnica precedente prevede l'impiego della similarità coseno. Essa offre una serie di caratteristiche che la distanza euclidea non ha, tra i più immediati si evidenziano: \begin{itemize}
\item è di per sé una similarità, pertanto non va invertita;
\item è vincolata a variare all'interno dell'intervallo $[-1,\ 1]$, quindi non cresce indefinitamente.
\end{itemize}

In tal caso il matcher avrebbe la forma: \begin{equation}
	\kappa_j(p,\ I) = \frac{\epsilon(p,\ I) \cdot \mathbf{\bar{v}}_j}{|\epsilon(p,\ I)| \cdot |\mathbf{\bar{v}}_{j}|}
\end{equation}

\subsubsection{Matrice di covarianza e distanza di Mahalonobis}
Questa tecnica di aggregazione è più complessa, pur richiedendo anch'essa il calcolo della media: gli $l$ descrittori vengono aggregati calcolando media $\mathbf{\bar{v}}_{j}$ e matrice di covarianza $S_v$. A questo punto il matcher può essere costruito convertendo in similarità la distanza di Mahalonobis: \begin{equation}
	\kappa_j(p,\ I) = \frac{1}{1 + \sqrt{
		(\epsilon(p,\ I) - \mathbf{\bar{v}}_{j})^T
		S_v^{-1}
		(\epsilon(p,\ I) - \mathbf{\bar{v}}_{j})
	}}
\end{equation}

\paragraph{Note implementative}
Questa variante è stata implementata come le altre, tuttavia la continua occorrenza di matrici di covarianza singolari e/o l'accumularsi di errori dovuti al gran numero di operazioni in virgola mobile hanno reso praticamente impossibile verificarne la validità.

\subsubsection{Parzen Window con kernel gaussiano}
Idea: aggregare i descrittori stimandone la densità di probabilità tramite la tecnica nota come \enquote{Parzen Window} e realizzare un matcher che semplicemente campioni tale d.d.p.. Nella pratica, per ogni punto $p_j$ di ogni immagine $I_i$ del \ts\, dopo aver estratto i descrittori $\mathbf{v}_{i,j} = \epsilon(p_j,\ I_i)$, essi andranno aggregati producendo le liste $L_j = [\mathbf{v}_{1,j},\ \ldots,\ \mathbf{v}_{l,j}]$. Sfruttando il kernel gaussiano \begin{equation}
\mathcal{G}(\mathbf{x}) = \frac{1}{\sqrt{2 \pi}} e^{-\frac{1}{2} |\mathbf{x}|^2}
\end{equation} il matcher $j$-esimo diventa: \begin{equation}
	\kappa_j(p,\ I) = \frac{1}{l \cdot h^d} \sum_{\mathbf{v} \in L_j}^{} \mathcal{G} \left( \frac{\epsilon(p,\ I) - \mathbf{v}}{h} \right)
\end{equation} dove $h$ è un parametro libero che regola l'ampiezza finestra gaussiana.

